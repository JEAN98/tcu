<?php

use Illuminate\Database\Seeder;

class AviSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('enterprises')->insert([
            'admin_name' => "Melvin Flores Retana",
            'enterprise_name' => "Granja Don Chilo",
            'phone' => "123456789",
            'email' => "test@gmail.com",
            'location' => "Santa Clara"
        ]);

        DB::table('rol')->insert([
            'rol_name' => "Admin"
        ]);


        DB::table('users')->insert([
            'name' => "Jean Carlo",
            "email" => "jean98vb@hotmail.com",
            "password" => bcrypt('123456789'),
            "enterprise_id" => 1,
            "rol_id" => 1
        ]);

        DB::table('users')->insert([
            'name' => "Melvin Flores Retana",
            "email" => "rukuo.cr@gmail.com",
            "password" => bcrypt('rukuo2019'),
            "enterprise_id" => 1,
            "rol_id" => 1
        ]);
    }
}
