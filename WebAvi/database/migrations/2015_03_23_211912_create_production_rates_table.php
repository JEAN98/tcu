<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductionRatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('production_rates', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('parvada_id')->unsigned();
            $table->integer('total_eggs_collected')->nullable();
            $table->integer('total_good_eggs')->nullable();
            $table->integer('total_broken_eggs')->nullable();
            $table->integer('current_chickens_quantity')->nullable();
            $table->double('percentage')->nullable();
            $table->date('date_collected')->nullable();
            $table->foreign('parvada_id')->references('id')->on('parvadas');
            });

            //TODO:Delete on cascade needs to be added
            //->onDelete('cascade');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('production_rates');
    }
}
