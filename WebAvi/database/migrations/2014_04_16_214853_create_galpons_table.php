<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGalponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('galpons', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('galpon_code')->nullable()->unique();;
            $table->double('meters_large')->nullable();
            $table->double('meters_width')->nullable();
            $table->double('maximum_chickens')->nullable();
            $table->date('construction_date')->nullable();
            $table->integer('enterprise_id')->unsigned();
            $table->foreign('enterprise_id')->references('id')->on('enterprises');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('galpons');
    }
}
