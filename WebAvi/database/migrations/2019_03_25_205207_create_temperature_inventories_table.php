<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTemperatureInventoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temperature_inventories', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('galpon_id')->unsigned();;
            $table->date('date')->nullable();
            $table->string('hour')->nullable();
            $table->double('temperature')->nullable();
            $table->integer('parvada_id')->unsigned();
            $table->foreign('parvada_id')->references('id')->on('parvadas');
            $table->foreign('galpon_id')->references('id')->on('galpons');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('temperature_inventories');
    }
}
