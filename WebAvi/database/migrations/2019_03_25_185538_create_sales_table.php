<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->double('total_kilograms')->nullable();
            $table->double('price_per_kilogram')->nullable();
            $table->double('total_price')->nullable();
            $table->integer('client_id')->unsigned();
            $table->date('date')->nullable();
            $table->string('payment_type');
            $table->string('status');
            $table->string('invoice_id');
            $table->foreign('client_id')->references('id')->on('clients');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sales');
    }
}
