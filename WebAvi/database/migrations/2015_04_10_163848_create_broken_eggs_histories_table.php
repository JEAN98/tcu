<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBrokenEggsHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('broken_eggs_histories', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('causa_id')->unsigned();
            $table->integer('production_id')->unsigned();
            $table->integer('quantity')->nullable();
            $table->foreign('causa_id')->references('id')->on('broken_eggs_causes');
            $table->foreign('production_id')->references('id')->on('production_rates');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('broken_eggs_histories');
    }
}
