<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMortalitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mortalities', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('parvada_id')->unsigned();
            $table->integer('chikens_quantity')->nullable();
            $table->date('death_date')->nullable();
            $table->integer('mortality_type_id')->unsigned();
            $table->string('comment')->nullable();
            $table->foreign('parvada_id')->references('id')->on('parvadas');
            $table->foreign('mortality_type_id')->references('id')->on('mortality_types');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('mortalities');
    }
}
