<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGalponInventoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('galpon_inventories', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('galpon_code')->unsigned();
            $table->integer('feeders_quantity')->nullable();
            $table->integer('reserved_feeders_quantity')->nullable();
            $table->integer('drinking_fountains_quantity')->nullable();
            $table->integer('reserved_drinking_fountains_quantity')->nullable();
            $table->integer('light_bulbs')->nullable();
            $table->integer('reserved_light_bulbs')->nullable();
            $table->integer('chiken_nests')->nullable();
            $table->boolean('is_thermometer')->nullable();
            $table->foreign('galpon_code')->references('id')->on('galpons');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('galpon_inventories');
    }
}
