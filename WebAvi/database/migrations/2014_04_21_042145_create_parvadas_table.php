<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParvadasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parvadas', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->date('admission_date')->nullable();
            $table->string('entry_age')->nullable();
            $table->string('race')->nullable();
            $table->date('beginning_posture')->nullable();
            $table->string('posture_age')->nullable();
            $table->integer('start_quantity_chickens')->nullable();
            $table->double('average_weight')->nullable();
            $table->boolean('parvada_status')->nullable();
            $table->integer('current_chickens_quantity')->nullable();
            $table->integer('galpon_id')->unsigned();
            $table->foreign('galpon_id')->references('id')->on('galpons')->onDelete('cascade')->onUpdate('cascade');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parvadas');
    }
}
