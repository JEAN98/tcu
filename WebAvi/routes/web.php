<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['middleware' => ['auth']], function () {
    Route::resource('admin/parvadas', 'Admin\\ParvadasController');
    Route::resource('admin/galpons', 'Admin\\GalponsController');
    Route::get('admin/productionChart','ProductionRatesController@chart');
    Route::resource('admin/mortality_types', 'Admin\\Mortality_TypesController');
    Route::resource('admin/mortalities', 'Admin\\MortalitiesController');
    Route::resource('admin/production-rates', 'Admin\\ProductionRatesController');
    Route::resource('admin/broken-eggs-causes', 'Admin\\BrokenEggsCausesController');
    Route::resource('admin/broken-eggs-history', 'Admin\\BrokenEggsHistoryController');
    Route::resource('admin/price-history', 'Admin\\PriceHistoryController');
    Route::resource('admin/clients', 'Admin\\ClientsController');
    Route::resource('admin/eggs-return', 'Admin\\EggsReturnController');
    Route::resource('admin/galpon-inventory', 'Admin\\GalponInventoryController');
    Route::resource('admin/sales', 'Admin\\SalesController');
    Route::resource('admin/temperature-inventories', 'Admin\\TemperatureInventoriesController');

    Route::get('admin/filterSalesByDate','Admin\\SalesController@filter_sales_by_dates');
    Route::get('admin/price_by_kilogram','Admin\\SalesController@get_price_by_id');    
    Route::get('admin/filter_avarage_by_dates','Admin\\ProductionRatesController@filter_avarage_by_dates');    

    Route::get('/home', 'HomeController@index')->name('home');
});


Auth::routes();
