@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <ul >
                            <li role="presentation">
                                <a href="{{ url('/admin/galpons') }}">
                                    Galpones
                                </a>
                            </li>
                            <li role="presentation">
                                <a href="{{ url('/admin/galpon-inventory') }}">
                                    Inventario de galpón
                                </a>
                            </li>
                            <li role="presentation">
                                <a href="{{ url('/admin/parvadas') }}">
                                    Parvadas
                                </a>
                            </li>
                            <li role="presentation">
                                <a href="{{ url('/admin/mortality_types') }}">
                                    Tipos de mortalidades
                                </a>
                            </li>
                            <li role="presentation">
                                <a href="{{ url('/admin/mortalities') }}">
                                    Mortalidades
                                </a>
                            </li>
                            <li role="presentation">
                                <a href="{{ url('/admin/production-rates') }}">
                                    Producción de huevos
                                </a>
                            </li>
                            <li role="presentation">
                                <a href="{{ url('/admin/broken-eggs-causes') }}">
                                    Causas de huevos perdidos
                                </a>
                            </li>
                            <li role="presentation">
                                <a href="{{ url('/admin/broken-eggs-history') }}">
                                    Historial de huevos perdidos
                                </a>
                            </li>
                            <li role="presentation">
                                <a href="{{ url('/admin/price-history') }}">
                                    Precios por producto
                                </a>
                            </li>
                            <li role="presentation">
                                <a href="{{ url('/admin/clients') }}">
                                    Clientes
                                </a>
                            </li>
                            <li role="presentation">
                                <a href="{{ url('/admin/sales') }}">
                                    Ventas
                                </a>
                            </li>
                            <li role="presentation">
                                <a href="{{ url('/admin/eggs-return') }}">
                                    Devolución de huevos
                                </a>
                            </li>
                            <li role="presentation">
                                <a href="{{ url('/admin/temperature-inventories') }}">
                                    Inventario de temperatura
                                </a>
                            </li>
                        </ul>
                      
                </div>
            </div>
        </div>
    </div>
</div>



<script>

        var information = [""];
    
        var eggs_quantity = [23];
    
        var percentage = [23];
    
    
        var barChartData = {
    
            labels: information,
    
            datasets: [{
    
                label: 'Porcentaje',
    
                backgroundColor: "rgba(220,220,220,0.5)",
    
                data: percentage
    
            }, {
    
                label: 'Cantidad de huevos buenos',
    
                backgroundColor: "rgba(151,187,205,0.5)",
    
                data: eggs_quantity
    
            }]
    
        };
    
    
        window.onload = function() {
    
            var ctx = document.getElementById("canvas").getContext("2d");
    
            window.myBar = new Chart(ctx, {
    
                type: 'bar',
    
                data: barChartData,
    
                options: {
    
                    elements: {
    
                        rectangle: {
    
                            borderWidth: 2,
    
                            borderColor: 'rgb(0, 255, 0)',
    
                            borderSkipped: 'bottom'
    
                        }
    
                    },
    
                    responsive: true,
    
                    title: {
    
                        display: true,
    
                        text: 'Porcentaje de producción'
    
                    }
    
                }
    
            });
    
    
        };
    
    </script>

@endsection
