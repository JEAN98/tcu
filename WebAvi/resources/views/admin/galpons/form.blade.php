<div class="form-group {{ $errors->has('galpon_code') ? 'has-error' : ''}}">
    {!! Form::label('galpon_code', 'Código de galpón', ['class' => 'control-label']) !!}
    {!! Form::text('galpon_code', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('galpon_code', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('meters_large') ? 'has-error' : ''}}">
    {!! Form::label('meters_large', 'Metros de largo', ['class' => 'control-label']) !!}
    {!! Form::number('meters_large', null, ('required'  == 'required') ? ['class' => 'form-control', 'step'=>'any','required' => 'required','min'=>1] : ['class' => 'form-control','step'=>'any']) !!}
    {!! $errors->first('meters_large', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('meters_width') ? 'has-error' : ''}}">
    {!! Form::label('meters_width', 'Metros de ancho', ['class' => 'control-label']) !!}
    {!! Form::number('meters_width', null, ('required'  == 'required') ? ['class' => 'form-control','step'=>'any','required' => 'required','min'=>1] : ['class' => 'form-control','step'=>'any']) !!}
    {!! $errors->first('meters_width', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('maximum_chickens') ? 'has-error' : ''}}">
    {!! Form::label('maximum_chickens', 'Cantidad máxima de gallinas', ['class' => 'control-label']) !!}
    {!! Form::number('maximum_chickens', null, ('required'  == 'required') ? ['class' => 'form-control', 'required' => 'required','min'=>1] : ['class' => 'form-control']) !!}
    {!! $errors->first('maximum_chickens', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('construction_date') ? 'has-error' : ''}}">
    {!! Form::label('construction_date', 'Fecha de construcción', ['class' => 'control-label']) !!}
    {!! Form::date('construction_date', null, ('required'  == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('construction_date', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Actualizar' : 'Crear', ['class' => 'btn btn-primary']) !!}
</div>
