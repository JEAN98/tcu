@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">Galpones</div>
                    <div class="card-body">
                    @if($errors->any())
                        <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                    @endif
                        <a href="{{ url('/admin/galpons/create') }}" class="btn btn-success btn-sm" title="Add New Galpon">
                            <i class="fa fa-plus" aria-hidden="true"></i> Nueva entrada
                        </a>

                        {!! Form::open(['method' => 'GET', 'url' => '/admin/galpons', 'class' => 'form-inline my-2 my-lg-0 float-right', 'role' => 'search'])  !!}
                        <div class="input-group">
                            <input type="text" class="form-control" name="search" placeholder="Buscar..." value="{{ request('search') }}">
                            <span class="input-group-append">
                                <button class="btn btn-secondary" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                        {!! Form::close() !!}

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <thead>
                                    <tr>
                                        <th>#</th><th>Código</th><th>Cantidad soportada </th><th>Fecha de construcción</th><th>Acción</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($galpons as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $item->galpon_code }}</td>
                                        <td>{{ $item->maximum_chickens }}</td>
                                        <td>{{ $item->construction_date }}</td>
                                        <td>
                                            <a href="{{ url('/admin/galpons/' . $item->id) }}" title="View Galpon"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i>Mostrar</button></a>
                                            <a href="{{ url('/admin/galpons/' . $item->id . '/edit') }}" title="Edit Galpon"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Editar</button></a>
                                            {!! Form::open([
                                                'method'=>'DELETE',
                                                'url' => ['/admin/galpons', $item->id],
                                                'style' => 'display:inline'
                                            ]) !!}
                                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Eliminar', array(
                                                        'type' => 'submit',
                                                        'class' => 'btn btn-danger btn-sm',
                                                        'title' => 'Delete Galpon',
                                                        'onclick'=>'return confirm("¿Está seguro de eliminar?")'
                                                )) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $galpons->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
