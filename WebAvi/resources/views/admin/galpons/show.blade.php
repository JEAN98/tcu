@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                @if($errors->any())
                        <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                    @endif
                    <div class="card-header">Galpón {{ $galpon->galpon_code }}</div>
                    <div class="card-body">

                        <a href="{{ url('/admin/galpons') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Volver</button></a>
                        <a href="{{ url('/admin/galpons/' . $galpon->id . '/edit') }}" title="Edit Galpon"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Editar</button></a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['admin/galpons', $galpon->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Eliminar', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-sm',
                                    'title' => 'Delete Galpon',
                                    'onclick'=>'return confirm("¿Está seguro de eliminar la información?")'
                            ))!!}
                        {!! Form::close() !!}
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <tbody>
                                     <tr><th> Código</th><td> {{ $galpon->galpon_code }} </td>
                                     </tr><tr><th> Largo </th><td> {{ $galpon->meters_large }}m </td></tr>
                                     <tr><th> Ancho </th><td> {{ $galpon->meters_width }}m </td></tr>
                                     <tr><th> Cantidad soportada de gallinas </th><td> {{ $galpon->maximum_chickens}}</td></tr>
                                     <tr><th> Fecha de construcción </th><td> {{ $galpon->construction_date}}</td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
