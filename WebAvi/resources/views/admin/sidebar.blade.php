<div class="col-md-3" >
    <div class="card">
        <div class="card-header" >
            Navegador
        </div>

        <div class="card-body">
            <ul >
                <li role="presentation">
                    <i class="far fa-building">
                        <a href="{{ url('/home') }}">
                            Reportes
                        </a>
                    </i> 
                </li>
                <li role="presentation">
                    <a href="{{ url('/admin/galpons') }}">
                        Galpones
                    </a>
                </li>
                <li role="presentation">
                        <a href="{{ url('/admin/parvadas') }}">
                            Parvadas
                        </a>
                    </li>
                <li role="presentation">
                    <a href="{{ url('/admin/galpon-inventory') }}">
                        Inventario de galpón
                    </a>
                </li>
               
                <li role="presentation">
                    <a href="{{ url('/admin/mortality_types') }}">
                        Tipos de mortalidades
                    </a>
                </li>
                <li role="presentation">
                    <a href="{{ url('/admin/mortalities') }}">
                        Mortalidades
                    </a>
                </li>
                <li role="presentation">
                    <a href="{{ url('/admin/production-rates') }}">
                        Producción de huevos
                    </a>
                </li>
                <li role="presentation">
                    <a href="{{ url('/admin/broken-eggs-causes') }}">
                        Causas de huevos perdidos
                    </a>
                </li>
                <li role="presentation">
                    <a href="{{ url('/admin/broken-eggs-history') }}">
                        Historial de huevos perdidos
                    </a>
                </li>
                <li role="presentation">
                    <a href="{{ url('/admin/price-history') }}">
                        Precios por producto
                    </a>
                </li>
                <li role="presentation">
                    <a href="{{ url('/admin/clients') }}">
                        Clientes
                    </a>
                </li>
                <li role="presentation">
                    <a href="{{ url('/admin/sales') }}">
                        Ventas
                    </a>
                </li>
                <li role="presentation">
                    <a href="{{ url('/admin/eggs-return') }}">
                        Devolución de huevos
                    </a>
                </li>
                <li role="presentation">
                    <a href="{{ url('/admin/temperature-inventories') }}">
                        Inventario de temperatura
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
