@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">Inventario de galpón</div>
                    <div class="card-body">

                        <a href="{{ url('/admin/galpon-inventory') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Volver</button></a>
                        <a href="{{ url('/admin/galpon-inventory/' . $galponinventory->id . '/edit') }}" title="Edit GalponInventory"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Editar</button></a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['admin/galponinventory', $galponinventory->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Eliminar', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-sm',
                                    'title' => 'Delete GalponInventory',
                                    'onclick'=>'return confirm("¿Está seguro de eliminar la información?")'
                            ))!!}
                        {!! Form::close() !!}
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <tbody>
                                    <tr><th>Código de galpón </th><td> {{ $galponinventory->getGalpon->galpon_code }} </td>
                                    </tr><tr><th>Comederos en uso </th><td> {{ $galponinventory->feeders_quantity }} </td>
                                    </tr><tr><th> Comederos en reserva </th><td> {{ $galponinventory->reserved_feeders_quantity }}</td></tr>
                                    </tr><tr><th>Bebederos en uso </th><td> {{ $galponinventory->drinking_fountains_quantity }}</td></tr>
                                    </tr><tr><th>Bebederos en reserva </th><td> {{ $galponinventory->reserved_drinking_fountains_quantity }}</td></tr>
                                    </tr><tr><th>Bombillos en uso </th><td> {{ $galponinventory->light_bulbs }}</td></tr>
                                    </tr><tr><th>Bombillos en reserva </th><td> {{ $galponinventory->reserved_light_bulbs }}</td></tr>
                                    </tr><tr><th>Ponedores </th><td> {{ $galponinventory->chiken_nests }}</td></tr>
                                    @if ($galponinventory->is_thermometer == 1)
                                        </tr><tr><th>Termómetro </th><td> Si tiene</td></tr>    
                                    @else
                                      </tr><tr><th>Termómetro </th><td> No tiene</td></tr>    
                                    @endif
                                    
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
