<div class="form-group {{ $errors->has('galpon_code') ? 'has-error' : ''}}">
    {!! Form::label('galpon_code', 'Código de galpón', ['class' => 'control-label']) !!}
    {!! Form::select('galpon_code', $galpons, null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('galpon_code', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('feeders_quantity') ? 'has-error' : ''}}">
    {!! Form::label('feeders_quantity', 'Comederos', ['class' => 'control-label']) !!}
    {!! Form::number('feeders_quantity', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('feeders_quantity', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('reserved_feeders_quantity') ? 'has-error' : ''}}">
    {!! Form::label('reserved_feeders_quantity', 'Comederos en reserva', ['class' => 'control-label']) !!}
    {!! Form::number('reserved_feeders_quantity', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('reserved_feeders_quantity', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('drinking_fountains_quantity') ? 'has-error' : ''}}">
    {!! Form::label('drinking_fountains_quantity', 'Bebederos', ['class' => 'control-label']) !!}
    {!! Form::number('drinking_fountains_quantity', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('drinking_fountains_quantity', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('reserved_drinking_fountains_quantity') ? 'has-error' : ''}}">
    {!! Form::label('reserved_drinking_fountains_quantity', 'Bebederos en reserva', ['class' => 'control-label']) !!}
    {!! Form::number('reserved_drinking_fountains_quantity', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('reserved_drinking_fountains_quantity', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('light_bulbs') ? 'has-error' : ''}}">
    {!! Form::label('light_bulbs', 'Bombillos', ['class' => 'control-label']) !!}
    {!! Form::number('light_bulbs', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('light_bulbs', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('reserved_light_bulbs') ? 'has-error' : ''}}">
    {!! Form::label('reserved_light_bulbs', 'Bombillos en reserva', ['class' => 'control-label']) !!}
    {!! Form::number('reserved_light_bulbs', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('reserved_light_bulbs', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('chiken_nests') ? 'has-error' : ''}}">
    {!! Form::label('chiken_nests', 'Ponedores', ['class' => 'control-label']) !!}
    {!! Form::number('chiken_nests', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('chiken_nests', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('is_thermometer') ? 'has-error' : ''}}">
    {!! Form::label('is_thermometer', 'Termómetro', ['class' => 'control-label']) !!}
    <div class="checkbox">
    <label>{!! Form::radio('is_thermometer', '1') !!} Sí</label>
</div>
<div class="checkbox">
    <label>{!! Form::radio('is_thermometer', '0', true) !!} No</label>
</div>
    {!! $errors->first('is_thermometer', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Actualizar' : 'Crear', ['class' => 'btn btn-primary']) !!}
</div>
