@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">Inventario de galpón</div>
                    <div class="card-body">
                        <a href="{{ url('/admin/galpon-inventory/create') }}" class="btn btn-success btn-sm" title="Add New GalponInventory">
                            <i class="fa fa-plus" aria-hidden="true"></i> Nueva entrada
                        </a>

                        {!! Form::open(['method' => 'GET', 'url' => '/admin/galpon-inventory', 'class' => 'form-inline my-2 my-lg-0 float-right', 'role' => 'search'])  !!}
                        <div class="input-group">
                            <input type="text" class="form-control" name="search" placeholder="Buscar..." value="{{ request('search') }}">
                            <span class="input-group-append">
                                <button class="btn btn-secondary" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                        {!! Form::close() !!}

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <thead>
                                    <tr>
                                        <th>#</th><th>Código de galpón</th>
                                        <th>Comederos en uso</th>
                                        <th>Bebederos en uso</th>
                                        <th>Bombillos en uso</th>
                                        <th>Acción</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($galponinventory as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $item->getGalpon->galpon_code }}</td>
                                        <td>{{ $item->feeders_quantity }}</td>
                                        <td>{{ $item->drinking_fountains_quantity }}</td>
                                        <td>{{ $item->light_bulbs }}</td>
                                        <td>
                                            <a href="{{ url('/admin/galpon-inventory/' . $item->id) }}" title="View GalponInventory"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> Mostrar</button></a>
                                            <a href="{{ url('/admin/galpon-inventory/' . $item->id . '/edit') }}" title="Edit GalponInventory"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Editar</button></a>
                                            {!! Form::open([
                                                'method'=>'DELETE',
                                                'url' => ['/admin/galpon-inventory', $item->id],
                                                'style' => 'display:inline'
                                            ]) !!}
                                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Eliminar', array(
                                                        'type' => 'submit',
                                                        'class' => 'btn btn-danger btn-sm',
                                                        'title' => 'Delete GalponInventory',
                                                        'onclick'=>'return confirm("¿Está seguro de eliminar la información?")'
                                                )) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $galponinventory->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
