@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header"><i class="fas fa-egg"></i> Producción de huevos</div>
                    <div class="card-body">
                    @if($errors->any())
                        <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                    @endif
                        <a href="{{ url('/admin/production-rates/create') }}" class="btn btn-success btn-sm" title="Add New ProductionRate">
                            <i class="fa fa-plus" aria-hidden="true"></i> Nueva entrada
                        </a>

                        {!! Form::open(['method' => 'GET', 'url' => '/admin/production-rates', 'class' => 'form-inline my-2 my-lg-0 float-right', 'role' => 'search'])  !!}
                        <div class="input-group">
                            <input type="text" class="form-control" name="search" placeholder="Buscar..." value="{{ request('search') }}">
                            <span class="input-group-append">
                                <button class="btn btn-secondary" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                        {!! Form::close() !!}

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Código generado</th>
                                        <th>Código de galpón</th>
                                        <th>Total de huevos recolectados</th>
                                        <th>Producción</th>
                                        <th>Fecha</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($productionrates as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>( {{ $item->id }} )</td>
                                        <td>{{ $item["galpon_code"] }}</td>
                                        <td>{{ $item->total_eggs_collected }}</td>
                                        <td>{{ $item->percentage}}%</td>
                                        <td>{{ $item->date_collected }}</td>
                                        <td>
                                            <a href="{{ url('/admin/production-rates/' . $item->id) }}" title="View ProductionRate"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> Mostrar</button></a>
                                            <a href="{{ url('/admin/production-rates/' . $item->id . '/edit') }}" title="Edit ProductionRate"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Editar</button></a>
                                            {!! Form::open([
                                                'method'=>'DELETE',
                                                'url' => ['/admin/production-rates', $item->id],
                                                'style' => 'display:inline'
                                            ]) !!}
                                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Eliminar', array(
                                                        'type' => 'submit',
                                                        'class' => 'btn btn-danger btn-sm',
                                                        'title' => 'Delete ProductionRate',
                                                        'onclick'=>'return confirm("¿Está seguro de eliminar?")'
                                                )) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $productionrates->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
