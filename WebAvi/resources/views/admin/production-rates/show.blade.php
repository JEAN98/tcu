@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">Producción ( {{ $productionrate->id }} )</div>
                    <div class="card-body">
                    @if($errors->any())
                        <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                    @endif
                        <a href="{{ url('/admin/production-rates') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Volver</button></a>
                        <a href="{{ url('/admin/production-rates/' . $productionrate->id . '/edit') }}" title="Edit ProductionRate"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Editar</button></a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['admin/production-rates', $productionrate->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Eliminar', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-sm',
                                    'title' => 'Delete ProductionRate',
                                    'onclick'=>'return confirm("¿Está seguro de eliminar?")'
                            ))!!}
                        {!! Form::close() !!}
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <tbody>
                                    <tr><th>Código generado </th><td>( {{ $productionrate->id}} )</td></tr>
                                    <tr><th>Código de galpón </th><td> {{ $productionrate["galpon_code"] }} </td></tr>
                                    <tr><th>Total de huevos recogidos </th><td> {{ $productionrate->total_eggs_collected }} </td></tr>
                                    <tr><th>Cantidad de huevos buenos </th><td> {{ $productionrate->total_good_eggs }} </td></tr>
                                    <tr><th>Cantidad de huevos quebrados </th><td> {{ $productionrate->total_broken_eggs }}</td></tr>
                                    <tr><th>Producción </th><td> {{ $productionrate->percentage }}%</td></tr>
                                    <tr><th>Cantidad de gallinas </th><td> {{ $productionrate->current_chickens_quantity }}</td></tr>
                                    <tr><th>Fecha de recolección </th><td> {{ $productionrate->date_collected }}</td></tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="container">
                                <div class="row">
                                    <div class="col-md-10 col-md-offset-1">
                                        <div class="panel panel-default">
                                            <div class="panel-body">
                                                <canvas id="canvas" height="280" width="600"></canvas>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>

  

<script>

    var information = [""];

    var eggs_quantity = [<?php echo  $productionrate->total_good_eggs  ?>];

    var percentage = [<?php echo  $productionrate->percentage; ?>];


    var barChartData = {

        labels: information,

        datasets: [{

            label: 'Porcentaje',

            backgroundColor: "rgba(220,220,220,0.5)",

            data: percentage

        } /*, {

            label: 'Cantidad de huevos buenos',

            backgroundColor: "rgba(151,187,205,0.5)",

            data: eggs_quantity

        }*/
        ]

    };


    window.onload = function() {

        var ctx = document.getElementById("canvas").getContext("2d");

        window.myBar = new Chart(ctx, {

            type: 'bar',

            data: barChartData,

            options: {

                elements: {

                    rectangle: {

                        borderWidth: 2,

                        borderColor: 'rgb(0, 255, 0)',

                        borderSkipped: 'bottom'

                    }

                },

                responsive: true,

                title: {

                    display: true,

                    text: 'Porcentaje de producción'

                }

            }

        });


    };

</script>



@endsection
