<div class="form-group {{ $errors->has('galpon_id') ? 'has-error' : ''}}">
    {!! Form::label('galpon_id', 'Galpón', ['class' => 'control-label']) !!}
    {!! Form::select('galpon_id', $galpon_list, null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('galpon_id', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('total_eggs_collected') ? 'has-error' : ''}}">
    {!! Form::label('total_eggs_collected_label', 'Total de huevos recolectados', ['class' => 'control-label']) !!}
    {!! Form::number('total_eggs_collected', null, ('required' == 'required') ? ['class' => 'form-control','oninput' => 'calculate_rate()','min'=>0] : ['class' => 'form-control','oninput' => 'calculate_rate()']) !!}
    {!! $errors->first('total_eggs_collected', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('total_good_eggs') ? 'has-error' : ''}}">
    {!! Form::label('total_good_eggs_label', 'Total de huevos buenos', ['class' => 'control-label']) !!}
    {!! Form::number('total_good_eggs', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required','oninput' => 'calculate_rate()','min'=>0] : ['class' => 'form-control','oninput' => 'calculate_rate()']) !!}
    {!! $errors->first('total_good_eggs', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('percentage') ? 'has-error' : ''}}">
    {!! Form::label('percentage_label', 'Producción %', ['class' => 'control-label']) !!}
    {!! Form::number('percentage', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required','disabled' => 'disabled'] : ['class' => 'form-control']) !!}
    {!! $errors->first('percentage', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group {{ $errors->has('date_collected') ? 'has-error' : ''}}">
    {!! Form::label('date_collected', 'Fecha de recolección', ['class' => 'control-label']) !!}
    {!! Form::date('date_collected', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('date_collected', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Actulizar' : 'Crear', ['class' => 'btn btn-primary']) !!}
</div>

<script>

  function calculate_rate() {
    console.log(document.getElementsByName("total_good_eggs")[0]);
    var eggs_collected =  parseFloat(document.getElementsByName("total_eggs_collected")[0].value);
    var good_eggs      =  parseFloat(document.getElementsByName("total_good_eggs")[0].value);
    var result         = (good_eggs*100)/eggs_collected;
    document.getElementsByName("percentage")[0].value = result.toString();
    console.log("Percetange: " , result);
  }
</script>
 