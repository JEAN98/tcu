@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">Inventario de temperatura </div>
                    <div class="card-body">

                        <a href="{{ url('/admin/temperature-inventories') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Volver</button></a>
                        <a href="{{ url('/admin/temperature-inventories/' . $temperatureinventory->id . '/edit') }}" title="Edit TemperatureInventory"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Editar</button></a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['admin/temperatureinventories', $temperatureinventory->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Eliminar', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-sm',
                                    'title' => 'Delete TemperatureInventory',
                                    'onclick'=>'return confirm("¿Está seguro de eliminar la información?")'
                            ))!!}
                        {!! Form::close() !!}
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <tbody>
                                    <tr><th> Código de galpón </th><td> {{ $temperatureinventory->getGalpon->galpon_code }} </td></tr>
                                    <tr><th> N° Parvada </th><td> {{ $temperatureinventory->getParvada->id }} </td></tr>
                                    <tr><th> Temperatura</th><td> {{ $temperatureinventory->temperature }}°C </td></tr>
                                    <tr><th> Fecha de la medida</th><td> {{ $temperatureinventory->date }} </td></tr>
                                    <tr><th> Hora de la medida </th><td> {{ $temperatureinventory->hour }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
