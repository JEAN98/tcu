<div class="form-group {{ $errors->has('galpon_id') ? 'has-error' : ''}}">
    {!! Form::label('galpon_id', 'Galpón', ['class' => 'control-label']) !!}
    {!! Form::select('galpon_id', $galpons, null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('galpon_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('date') ? 'has-error' : ''}}">
    {!! Form::label('date', 'Fecha', ['class' => 'control-label']) !!}
    {!! Form::date('date', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('date', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('hour') ? 'has-error' : ''}}">
    {!! Form::label('hour', 'Hora de la medida', ['class' => 'control-label']) !!}
    {!! Form::text('hour', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('hour', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('temperature') ? 'has-error' : ''}}">
    {!! Form::label('temperature', 'Temperatura °C', ['class' => 'control-label']) !!}
    {!! Form::number('temperature', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('temperature', '<p class="help-block">:message</p>') !!}
</div>
.
<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary']) !!}
</div>
