@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">Mortalidad</div>
                    <div class="card-body">

                        <a href="{{ url('/admin/mortalities') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i>Volver</button></a>
                        <a href="{{ url('/admin/mortalities/' . $mortality->id . '/edit') }}" title="Edit Mortality"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Editar</button></a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['admin/mortalities', $mortality->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-sm',
                                    'title' => 'Delete Mortality',
                                    'onclick'=>'return confirm("¿Está seguro de eliminar?")'
                            ))!!}
                        {!! Form::close() !!}
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <tbody>
                                    <tr>
                                        <th>Código de galpón </th><td> {{ $mortality["galpon_code"] }} </td></tr>
                                        <tr><th>Cantidad de gallinas</th><td> {{ $mortality->chikens_quantity }} </td></tr>
                                        <tr><th>Fecha </th><td> {{ $mortality->death_date }} </td></tr>
                                        <tr><th>Tipo de mortalidad </th><td> {{$mortality["type"] }} </td></tr>
                                        <tr><th>Comentario </th><td> {{ $mortality->comment }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
