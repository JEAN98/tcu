@php
 //   $data = 18;
  //  $test = 0;
@endphp

<div class="form-group {{ $errors->has('galpon_id') ? 'has-error' : ''}}">
    {!! Form::label('galpon_id', 'Galpón', ['class' => 'control-label']) !!}
    {!! Form::select('galpon_id', $galpon_list, null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('galpon_id', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('chikens_quantity') ? 'has-error' : ''}}">
    {!! Form::label('chikens_quantity', 'Cantidad de gallinas', ['class' => 'control-label']) !!}
    {!! Form::number('chikens_quantity', null,('required' == 'required') ? ['class' => 'form-control', 'required' => 'required','min'=>1] : ['class' => 'form-control']) !!}
    {!! $errors->first('chikens_quantity', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('death_date') ? 'has-error' : ''}}">
    {!! Form::label('death_date', 'Fecha', ['class' => 'control-label']) !!}
    {!! Form::date('death_date', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('death_date', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('mortality_type_id') ? 'has-error' : ''}}">
    {!! Form::label('mortality_type_id', 'Tipo de mortalidad', ['class' => 'control-label']) !!}
    {!! Form::select('mortality_type_id', $mortality_types, null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('mortality_type_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('comment') ? 'has-error' : ''}}">
    {!! Form::label('comment', 'Comentario', ['class' => 'control-label']) !!}
    {!! Form::textarea('comment', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('comment', '<p class="help-block">:message</p>') !!}
</div>



<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Actualizar' : 'Crear', ['class' => 'btn btn-primary']) !!}
</div>


