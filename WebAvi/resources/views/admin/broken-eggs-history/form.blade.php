<div class="form-group {{ $errors->has('causa_id') ? 'has-error' : ''}}">
    {!! Form::label('causa_id', 'Tipo de causa', ['class' => 'control-label']) !!}
    {!! Form::select('causa_id', $broken_eggs_causes, null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('causa_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('production_id') ? 'has-error' : ''}}">
    {!! Form::label('production_id', 'Código de producción generado', ['class' => 'control-label']) !!}
    {!! Form::select('production_id', $current_production_rates, null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('production_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('quantity') ? 'has-error' : ''}}">
    {!! Form::label('quantity', 'Cantidad', ['class' => 'control-label']) !!}
    {!! Form::number('quantity', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('quantity', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Actualizar' : 'Crear', ['class' => 'btn btn-primary']) !!}
</div>
