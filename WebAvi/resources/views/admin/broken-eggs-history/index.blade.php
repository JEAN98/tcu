@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">Historial de huevos perdidos</div>
                    <div class="card-body">
                        <a href="{{ url('/admin/broken-eggs-history/create') }}" class="btn btn-success btn-sm" title="Add New BrokenEggsHistory">
                            <i class="fa fa-plus" aria-hidden="true"></i> Nueva entrada
                        </a>

                        {!! Form::open(['method' => 'GET', 'url' => '/admin/broken-eggs-history', 'class' => 'form-inline my-2 my-lg-0 float-right', 'role' => 'search'])  !!}
                        <div class="input-group">
                            <input type="text" class="form-control" name="search" placeholder="Buscar..." value="{{ request('search') }}">
                            <span class="input-group-append">
                                <button class="btn btn-secondary" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                        {!! Form::close() !!}

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <thead>
                                    <tr>
                                        <th>#</th><th>Tipo de causa </th><th>Código de la producción</th><th>Cantidad</th><th>Acción</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($brokeneggshistory as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $item["cause_name"] }}</td><td>{{ $item->production_id }}</td><td>{{ $item->quantity }}</td>
                                        <td>
                                            <a href="{{ url('/admin/broken-eggs-history/' . $item->id) }}" title="View BrokenEggsHistory"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> Mostrar</button></a>
                                            <a href="{{ url('/admin/broken-eggs-history/' . $item->id . '/edit') }}" title="Edit BrokenEggsHistory"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Editar</button></a>
                                            {!! Form::open([
                                                'method'=>'DELETE',
                                                'url' => ['/admin/broken-eggs-history', $item->id],
                                                'style' => 'display:inline'
                                            ]) !!}
                                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Eliminar', array(
                                                        'type' => 'submit',
                                                        'class' => 'btn btn-danger btn-sm',
                                                        'title' => 'Delete BrokenEggsHistory',
                                                        'onclick'=>'return confirm("¿Está seguro de eliminar la información?")'
                                                )) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $brokeneggshistory->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
