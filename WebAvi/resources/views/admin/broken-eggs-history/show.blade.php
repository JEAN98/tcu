@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">Historial de huevos perdidos</div>
                    <div class="card-body">

                        <a href="{{ url('/admin/broken-eggs-history') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Volver</button></a>
                        <a href="{{ url('/admin/broken-eggs-history/' . $brokeneggshistory->id . '/edit') }}" title="Edit BrokenEggsHistory"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Editar</button></a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['admin/brokeneggshistory', $brokeneggshistory->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-sm',
                                    'title' => 'Delete BrokenEggsHistory',
                                    'onclick'=>'return confirm("¿Está seguro de eliminar la información?")'
                            ))!!}
                        {!! Form::close() !!}
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <tbody>
                                    <tr>
                                  
                                    </tr>
                                   <tr><th> Código de producción </th><td> {{ $brokeneggshistory->production_id }} </td></tr><tr><th> Cantidad </th><td> {{ $brokeneggshistory->quantity }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
