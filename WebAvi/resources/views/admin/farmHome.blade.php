@extends('layouts.app')

@section('content')

<div class="container">
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">Información de la producción</div>
                    <div class="card-body">
                        <div class="container">
                                <div class="row">
                                    <div class="col-md-10 col-md-offset-1">
                                        <div class="panel panel-default">
                                            <div class="panel-body">
                                                <form action="">
                                                    <div class="form-group {{ $errors->has('date_collected') ? 'has-error' : ''}}">
                                                        {!! Form::label('date_collected', 'Fecha de inicio', ['class' => 'control-label']) !!}
                                                        {!! Form::date('start_date_prod', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
                                                        {!! $errors->first('date_collected', '<p class="help-block">:message</p>') !!}
                                                    </div>
                                                    <div class="form-group {{ $errors->has('date_collected') ? 'has-error' : ''}}">
                                                            {!! Form::label('date_collected', 'Fecha final', ['class' => 'control-label']) !!}
                                                            {!! Form::date('end_date_prod', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
                                                            {!! $errors->first('date_collected', '<p class="help-block">:message</p>') !!}
                                                    </div>
                                                    <button type="button" class="btn btn-success" onclick="filter_production_by_dates()">Filtrar promedio de producción</button>
                                                </form>
                                                <br>
                                                <h4 id="general_avarage">Promedio general = 0</h4>
                                                <h4 id="total_eggs">Total de huevos producidos = 0</h4>
                                                <h4 id="total_good_eggs">Total de huevos buenos = 0</h4>
                                                <h4 id="total_eggs_lost">Total de huevos perdidos = 0</h4>
                                                <canvas id="production_chart"></canvas>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>   
                    <br>
                    <div class="card">
                        <div class="card-header">Información de las ventas </div>
                        <div class="card-body">
                            <div class="container">
                                    <div class="row">
                                        <div class="col-md-10 col-md-offset-1">
                                            <div class="panel panel-default">
                                                <div class="panel-body">
                                                    <form action="">
                                                        <div class="form-group {{ $errors->has('date_collected') ? 'has-error' : ''}}">
                                                            {!! Form::label('date_collected', 'Fecha de inicio', ['class' => 'control-label']) !!}
                                                            {!! Form::date('start_date_sales', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
                                                            {!! $errors->first('date_collected', '<p class="help-block">:message</p>') !!}
                                                        </div>
                                                        <div class="form-group {{ $errors->has('date_collected') ? 'has-error' : ''}}">
                                                                {!! Form::label('date_collected', 'Fecha final', ['class' => 'control-label']) !!}
                                                                {!! Form::date('end_date_sales', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
                                                                {!! $errors->first('date_collected', '<p class="help-block">:message</p>') !!}
                                                        </div>
                                                        <button type="button" class="btn btn-success" onclick="filter_sales_by_dates()">Filtrar total de ventas</button>
                                                    </form>
                                                    <br>
                                                    <h4 id="total">Dinero acumulado = ₡0</h4>
                                                    <h4 id="quantity">Cantidad total de ventas = 0</h4>
                                                    <h4 id="pending_sales" >Ventas pendientes = 0</h4>
                                                    <canvas id="sales_chart"></canvas>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>    
                    </div> 
                </div>
            </div>                
        </div>
    </div>
</div>
<script type="text/javascript" src="{{asset('/js/reports.js')}}"> </script>   
@endsection
