<div class="form-group {{ $errors->has('admission_date') ? 'has-error' : ''}}">
    {!! Form::label('admission_date', 'Fecha de ingreso', ['class' => 'control-label']) !!}
    {!! Form::date('admission_date', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('admission_date', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('entry_age') ? 'has-error' : ''}}">
    {!! Form::label('entry_age', 'Edad de entrada', ['class' => 'control-label']) !!}
    {!! Form::text('entry_age', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('entry_age', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('race') ? 'has-error' : ''}}">
    {!! Form::label('race', 'Raza', ['class' => 'control-label']) !!}
    {!! Form::text('race', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('race', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('beginning_posture') ? 'has-error' : ''}}">
    {!! Form::label('beginning_posture', 'Inicio de postura', ['class' => 'control-label']) !!}
    {!! Form::date('beginning_posture', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('beginning_posture', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('posture_age') ? 'has-error' : ''}}">
    {!! Form::label('posture_age', 'Edad de postura', ['class' => 'control-label']) !!}
    {!! Form::text('posture_age', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('posture_age', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('start_quantity_chickens') ? 'has-error' : ''}}">
    {!! Form::label('start_quantity_chickens', 'Cantidad inicial de gallinas', ['class' => 'control-label']) !!}
    {!! Form::number('start_quantity_chickens', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required','min'=>1] : ['class' => 'form-control']) !!}
    {!! $errors->first('start_quantity_chickens', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('average_weight') ? 'has-error' : ''}}">
    {!! Form::label('average_weight', 'Peso promedio(Kg)', ['class' => 'control-label']) !!}
    {!! Form::number('average_weight', null, ('required' == 'required') ? ['class' => 'form-control', 'step'=>'any','required' => 'required','min'=>1] : ['class' => 'form-control']) !!}
    {!! $errors->first('average_weight', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('parvada_status') ? 'has-error' : ''}}">
    {!! Form::label('parvada_status', 'Parvada activa', ['class' => 'control-label']) !!}
    <div class="checkbox">
    <label>{!! Form::radio('parvada_status', '1') !!} Si</label>
</div>
<div class="checkbox">
    <label>{!! Form::radio('parvada_status', '0', true) !!} No</label>
</div>
    {!! $errors->first('parvada_status', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('galpon_id') ? 'has-error' : ''}}">
    {!! Form::label('galpon_id', 'Código de galpón', ['class' => 'control-label']) !!}
    {!! Form::select('galpon_id', $galpon_list, null, ['class' => 'form-control']) !!}
    {!! $errors->first('galpon_id', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Actualizar' : 'Crear', ['class' => 'btn btn-primary']) !!}
</div>
