@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                <div class="card-header">Parvada {{$parvada->id}}</div>
                    <div class="card-body">
                    @if($errors->any())
                        <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                    @endif
                        <a href="{{ url('/admin/parvadas') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Volver</button></a>
                        <a href="{{ url('/admin/parvadas/' . $parvada->id . '/edit') }}" title="Edit Parvada"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Editar</button></a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['admin/parvadas', $parvada->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Eliminar', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-sm',
                                    'title' => 'Delete Parvada',
                                    'onclick'=>'return confirm("¿Está seguro de eliminar la parvada?")'
                            ))!!}
                        {!! Form::close() !!}
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <tbody>
                                    <tr><th> Número de parvada </th><td> {{ $parvada->id }} </td></tr>
                                    <tr><th> Fecha de ingreso </th><td> {{ $parvada->admission_date }} </td></tr>
                                    <tr><th> Edad de entrada</th><td> {{ $parvada->entry_age }} </td></tr>
                                    <tr><th> Raza </th><td> {{ $parvada->race }} </td></tr>
                                    <tr><th> Inicio de postura </th><td> {{ $parvada->beginning_posture }} </td></tr>
                                    <tr><th> Edad de postura </th><td> {{ $parvada->posture_age }} </td></tr>
                                    <tr><th> Cantidad  inicial</th><td> {{ $parvada->start_quantity_chickens }} </td></tr>
                                    <tr><th> Peso promedio </th><td> {{ $parvada->average_weight }}kg</td></tr>
                                    <tr><th> Estado </th> @if ( $parvada["status"] != "Activa")
                                            <td>
                                                <font color="red"> {{ $parvada["status"]     }} </font>
                                            </td>
                                        @else
                                            <td> 
                                              <font color="green"> {{ $parvada["status"]    }} </font>
                                            </td>
                                        @endif</tr>
                                    <tr><th> Cantidad actual</th><td> {{ $parvada->current_chickens_quantity}} </td></tr>
                                    <tr><th> Código de galpón </th><td> {{ $parvada["galpon_code"] }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
