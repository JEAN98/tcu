@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">Devolución de huevos</div>
                    <div class="card-body">

                        <a href="{{ url('/admin/eggs-return') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Volver</button></a>
                        <a href="{{ url('/admin/eggs-return/' . $eggsreturn->id . '/edit') }}" title="Edit EggsReturn"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Editar</button></a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['admin/eggsreturn', $eggsreturn->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Eliminar', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-sm',
                                    'title' => 'Delete EggsReturn',
                                    'onclick'=>'return confirm("¿Está seguro de eliminar la información?")'
                            ))!!}
                        {!! Form::close() !!}
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <tbody>
                                    <tr><th> Cliente </th>
                                    <td> {{ $client}} </td></tr>
                                    <tr><th> Cantidad(kg) </th><td> {{ $eggsreturn->quantity }}kg </td></tr>
                                    <tr><th> Fecha </th><td> {{ $eggsreturn->date }} </td></tr>
                                    <tr><th>Comentario </th><td> {{ $eggsreturn->comment }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
