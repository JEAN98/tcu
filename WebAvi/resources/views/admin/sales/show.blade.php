@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header"><i class="fas fa-coins"></i> {{ $sale->date }}</div>
                    <div class="card-body">

                        <a href="{{ url('/admin/sales') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Volver</button></a>
                        <a href="{{ url('/admin/sales/' . $sale->id . '/edit') }}" title="Edit Sale"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Editar</button></a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['admin/sales', $sale->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Eliminar', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-sm',
                                    'title' => 'Delete Sale',
                                    'onclick'=>'return confirm("Confirm delete?")'
                            ))!!}
                        {!! Form::close() !!}
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <tbody>
                                    <tr><th> Total de kilogramos </th><td> {{ $sale->total_kilograms }} kg</td></tr>
                                    <tr><th> Precio por kilogramo </th><td>₡ {{ $sale->price_per_kilogram }} </td></tr>
                                    <tr><th> Precio total </th><td> ₡{{ $sale->total_price }} </td></tr>
                                    <tr><th> Cliente </th><td> {{ $sale["client"] }} </td></tr>
                                    <tr><th> Tipo de pago </th><td> {{ $sale["payment_type"] }} </td></tr>
                                    @if ($sale->status == "Pendiente")
                                    <tr><th> Estado </th><td>
                                            <font color="red"> {{ $sale->status     }} </font></td></tr>
                                    @else
                                    <tr><th> Estado </th><td>
                                            <font color="green"> {{ $sale->status     }} </font></td></tr>
                                    @endif
                                   
                                    <tr><th> Número de factura </th><td> {{ $sale->invoice_id }} </td></tr>
                                    <tr><th> Fecha de la venta </th><td> {{ $sale->date }} </td></tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
