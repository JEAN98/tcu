<div class="form-group {{ $errors->has('total_kilograms') ? 'has-error' : ''}}">
    {!! Form::label('total_kilograms', 'Total de kilogramos', ['class' => 'control-label']) !!}
    {!! Form::number('total_kilograms', $total_kilograms, ('required' == 'required') ? ['class' => 'form-control', 'oninput' => 'calculate_total_price()','required' => 'required','min'=>1] : ['class' => 'form-control']) !!}
    {!! $errors->first('total_kilograms', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('price_per_kilogram') ? 'has-error' : ''}}">
    {!! Form::label('price_per_kilogram', '₡ Precio por kilogramo', ['class' => 'control-label']) !!}
    {!! Form::select('prices_per_kilogram', $prices_per_kilogram, null, ('required' == 'required') ? ['class' => 'form-control', 'onchange'=>'show_price_by_id()','required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('price_per_kilogram', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('total_price') ? 'has-error' : ''}}">
    {!! Form::label('total_price', 'Precio total', ['class' => 'control-label']) !!}
    {!! Form::number('total_price', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required','disabled' => 'disabled'] : ['class' => 'form-control']) !!}
    {!! $errors->first('total_price', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('client_id') ? 'has-error' : ''}}">
    {!! Form::label('client_id', 'Cliente', ['class' => 'control-label']) !!}
    {!! Form::select('client_id', $clients, null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('client_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('date') ? 'has-error' : ''}}">
    {!! Form::label('date', 'Fecha de venta', ['class' => 'control-label']) !!}
    {!! Form::date('date', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('date', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('payment_type') ? 'has-error' : ''}}">
    {!! Form::label('payment_type', 'Tipo de pago', ['class' => 'control-label']) !!}
    {!! Form::select('payment_type', json_decode('{"credit":"Efectivo","deposit_in_the_bank":"Dep\u00f3sito en el banco","check":"Cheque"}', true), null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('payment_type', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
    {!! Form::label('status', 'Estado', ['class' => 'control-label']) !!}
    {!! Form::select('status', json_decode('{"Pendiente":"Pendiente","Completado":"Completado"}', true), null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('invoice_id') ? 'has-error' : ''}}">
    {!! Form::label('invoice_id', 'N° Factura', ['class' => 'control-label']) !!}
    {!! Form::text('invoice_id', '', ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('invoice_id', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Actualizar' : 'Crear', ['class' => 'btn btn-primary']) !!}
</div>

<script type="text/javascript" src="{{asset('/js/priceByKilogram.js')}}">
</script>

