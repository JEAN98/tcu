@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header"><i class="fas fa-coins"></i>  Ventas</div>
                    <div class="card-body">
                        <a href="{{ url('/admin/sales/create') }}" class="btn btn-success btn-sm" title="Add New Sale">
                            <i class="fa fa-plus" aria-hidden="true"></i> Nueva entrada
                        </a>

                        {!! Form::open(['method' => 'GET', 'url' => '/admin/sales', 'class' => 'form-inline my-2 my-lg-0 float-right', 'role' => 'search'])  !!}
                        <div class="input-group">
                            <input type="text" class="form-control" name="search" placeholder="Buscar..." value="{{ request('search') }}">
                            <span class="input-group-append">
                                <button class="btn btn-secondary" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                        {!! Form::close() !!}

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <thead>
                                    <tr>
                                        <th>#</th><th>Total de kilogramos</th>
                                        <th>Precio por kilogramo</th>
                                        <th>Precio total</th>
                                        <th>Cliente</th>
                                        <th>Estado</th>
                                        <th>Acción</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($sales as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $item->total_kilograms }} kg</td>
                                        <td>₡{{ $item->price_per_kilogram }}</td>
                                        <td>₡{{ $item->total_price }}</td>
                                        <td>{{ $item["client"] }}</td>
                                        @if ( $item->status == "Pendiente")
                                            <td>
                                                <font color="red"> {{ $item->status     }} </font>
                                            </td>
                                        @else
                                            <td> 
                                              <font color="green"> {{ $item->status     }} </font>
                                            </td>
                                        @endif
                                        <td>
                                            <a href="{{ url('/admin/sales/' . $item->id) }}" title="View Sale"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> Mostrar</button></a>
                                            <a href="{{ url('/admin/sales/' . $item->id . '/edit') }}" title="Edit Sale"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Editar</button></a>
                                            {!! Form::open([
                                                'method'=>'DELETE',
                                                'url' => ['/admin/sales', $item->id],
                                                'style' => 'display:inline'
                                            ]) !!}
                                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Eliminar', array(
                                                        'type' => 'submit',
                                                        'class' => 'btn btn-danger btn-sm',
                                                        'title' => 'Delete Sale',
                                                        'onclick'=>'return confirm("¿Está seguro de eliminar la información?")'
                                                )) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $sales->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
     

@endsection
