let current_price = 0;
$(document).ready(function() {
    console.log("found it!");
    show_price_by_id();
});


function show_price_by_id()
{
    var e = document.getElementsByName("prices_per_kilogram")[0];
    var priceID = e.options[e.selectedIndex].value;
    //console.log(priceID); 
    ajax_request(priceID);
}

function calculate_total_price()
{
    console.log(current_price); 
    var total_kilograms  =  parseFloat(document.getElementsByName("total_kilograms")[0].value);
    var total_price      =  total_kilograms * current_price; 
    document.getElementsByName("total_price")[0].value =  total_price.toString();
}

function verify_URL()
{
    //console.log(window.location.pathname);
    if(window.location.pathname == "/admin/sales/create")
    {
        return "../"+"price_by_kilogram";
    }   
    return  "../"+"../"+"price_by_kilogram"
}

function ajax_request(ID)
{   
      $.ajax({
        url: verify_URL(),
        method: "GET",
        data: {
            priceID: ID            
        },
        success: function(data) {
            console.log(data.price_by_kilogram);
            current_price = data.price_by_kilogram;
            calculate_total_price();
        },
        error: function(event) {
            console.log('error', event);
        }
    })
}
