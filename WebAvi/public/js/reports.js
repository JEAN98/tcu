$(document).ready(function() {
    console.log("Report");
});


function build_chart_data(chartData)
{
    barChartData = {
        labels: chartData.labels,
        datasets: [
            {
                label: chartData.header,
                backgroundColor: chartData.backgroundColor,
                data: chartData.array_data
            }
        ]
    };
  return barChartData;
}

function build_chart(chartData,chartCharacs)
{
    var ctx3 = document.getElementById(chartCharacs.canvas_id).getContext("2d");
    window.myBar = new Chart(ctx3, {
        type: chartCharacs.chart_style,
        width:100,
        data: chartData,
        options: {
            elements: {
                rectangle: {
                    borderWidth: 3,
                    borderColor: 'rgba(0, 0, 0, 0.1)',
                    borderSkipped: 'bottom',
                    pointBackgroundColor:'rgba(0, 0, 0, 0.1)'
                }
            },
            responsive: true,
            title: {
                display: true,
                text: chartCharacs.title
            }
        }
    });
}



function filter_production_by_dates()
{
    var starte_date = document.getElementsByName("start_date_prod")[0].value;
    console.log(starte_date);
    var end_date = document.getElementsByName("end_date_prod")[0].value;
   
    if(starte_date != "" && end_date != "")
    {
        if(starte_date < end_date)
        {
            ajax_production_request(starte_date,end_date);
        }
        else
        {
            show_error_message("Formato incorrecto","¡La fecha de inicio debe ser menor a la fecha final!");
        }
    }
    else 
    {
        show_error_message("Campos vacios","¡Debe selecionar el rango de fechas para poder filtrar la información!");
    }
}

function build_production_chart(data)
{
    var chartData = { labels :data.date_list,
                      header:  "Producción",
                      array_data: data.avarage_list,
                      backgroundColor: 'rgb(219, 242,242)'
                    } 
    var lineChartData = build_chart_data(chartData);

    var chartCharacs = {
                          title:"Reporte de producción",
                          canvas_id:"production_chart",
                          chart_style: "line"
                       }
    build_chart(lineChartData,chartCharacs);
}

function filter_sales_by_dates()
{
    var starte_date = document.getElementsByName("start_date_sales")[0].value;
    console.log(document.getElementsByName("start_date_sales")[0]);
    var end_date = document.getElementsByName("end_date_sales")[0].value;
    
        if(starte_date != "" && end_date != "")
        {
            if(starte_date < end_date)
            {
                ajax_sales_request(starte_date,end_date);
            }
            else{
                show_error_message("Formato incorrecto","¡La fecha de inicio debe ser menor a la fecha final!");
            }
        }
        else
        {
            show_error_message("Campos vacios","¡Debe selecionar el rango de fechas para poder filtrar la información!");
        }
}

function build_sales_chart(data)
{
    var chartData = { 
        labels :data.date_list,
        header:  "Dinero de ventas",
        array_data: data.total_sales_list,
        backgroundColor: 'rgb(219, 242,242)'
    } 
    var lineChartData = build_chart_data(chartData);
    var chartCharacs = {
            title:"Reporte de ventas",
            canvas_id:"sales_chart",
            chart_style: "line"
        }
    build_chart(lineChartData,chartCharacs);
}

function show_error_message(title,message)
{
    swal({
        title: title,
        text: message,
        icon: "warning",
        dangerMode: true
      });
}

function ajax_sales_request(start_date,end_date)
{
    var current_dates = {
        "start_date" : start_date,
        "end_date"  :end_date
    };

    $.ajax({
        url: "../"+"admin/filterSalesByDate",
        method: "GET",
        data: {
            dates: current_dates            
        },
        success: function(data) {
          build_sales_report(data);
        },
        error: function(event) {
            console.log('error', event);
        }
    })
}

function build_sales_report(data)
{
    
    var pending_sales_element = document.getElementById("pending_sales");
    pending_sales_element.classList.remove("good","bad");

    if(data.total == 0)
    {
        show_error_message( "Información no encontrada","¡No hay información de ventas con" + 
                            " respecto al rango de fechas ingresado!"
                           );
    }
    document.getElementById('total').innerText = "Dinero acumulado = ₡" + data.total_sales.toString();
    document.getElementById('quantity').innerText = "Cantidad total de ventas = " + data.total_quantity.toString();

    if(data.pending_sales > 0 )
    {
        pending_sales_element.classList.add("bad")
    }
    else{
        pending_sales_element.classList.add("good")
    }

    document.getElementById('pending_sales').innerText = "Ventas pendientes = " + data.pending_sales.toString();


    build_sales_chart(data);
}

function ajax_production_request(start_date,end_date)
{
    var current_dates = {
        "start_date" : start_date,
        "end_date"  :end_date
    };

    $.ajax({
        url: "../"+"admin/filter_avarage_by_dates",
        method: "GET",
        data: {
            dates: current_dates            
        },
        success: function(data) {
            console.log(data);
            build_production_report(data);
        },
        error: function(event) {
            console.log('error', event);
        }
    })
}

function build_production_report(data)
{
    if(data.avarage == 0)
    {
        show_error_message( "Información no encontrada","¡No hay información de produción con" + 
                            " respecto al rango de fechas ingresado!"
                           );
    }
    document.getElementById('general_avarage').innerText = "Promedio general = " + data.general_avarage.toString() + "%";
    document.getElementById('total_eggs').innerText = "Total de huevos producidos = " + data.total_eggs_collected.toString();
    document.getElementById('total_good_eggs').innerText = "Total de huevos buenos = " + data.total_good_eggs.toString();
    document.getElementById('total_eggs_lost').innerText = "Total de huevos perdidos = " + data.total_eggs_lost.toString();
    build_production_chart(data);
}
