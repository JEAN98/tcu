<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GalponInventory extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'galpon_inventories';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['galpon_code', 'feeders_quantity', 'reserved_feeders_quantity', 'drinking_fountains_quantity', 'reserved_drinking_fountains_quantity', 'light_bulbs', 'reserved_light_bulbs', 'chiken_nests', 'is_thermometer'];


    public function getGalpon() 
    {         
       return $this->belongsTo('App\Galpon', 'galpon_code');     
    }
    
}
