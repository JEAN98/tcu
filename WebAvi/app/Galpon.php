<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Galpon extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'galpons';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['galpon_code', 'meters_large', 'meters_width', 'maximum_chickens', 'construction_date','enterprise_id'];

    
}
