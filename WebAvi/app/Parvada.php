<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Parvada extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'parvadas';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['admission_date', 'entry_age', 'race', 'beginning_posture', 'posture_age', 'start_quantity_chickens', 'average_weight','parvada_status', 'galpon_id','current_chickens_quantity'];

    public function galpon()
    {
        return $this->belongsTo('App\Galpon');
    }
    
}
