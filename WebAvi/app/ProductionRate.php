<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductionRate extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'production_rates';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['parvada_id', 'total_eggs_collected', 'total_good_eggs', 'total_broken_eggs', 'current_chickens_quantity', 'percentage', 'date_collected'];

    
}
