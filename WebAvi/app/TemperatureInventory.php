<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TemperatureInventory extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'temperature_inventories';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['galpon_id', 'date', 'hour', 'temperature', 'parvada_id'];

    public function getGalpon() 
    {         
       return $this->belongsTo('App\Galpon', 'galpon_id');     
    }

    public function getParvada() 
    {         
       return $this->belongsTo('App\Parvada', 'parvada_id');     
    }
}
