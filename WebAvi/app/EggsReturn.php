<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EggsReturn extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'eggs_returns';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['client_id', 'quantity', 'date', 'comment'];

    public function client()
    {
        return $this->belongsTo('App\Client','client_id');
    }
}
