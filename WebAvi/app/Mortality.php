<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mortality extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'mortalities';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['parvada_id', 'chikens_quantity', 'death_date', 'mortality_type_id', 'comment'];

    public function parvada()
    {
        return $this->belongsTo('App\Parvada');
    }
    public function mortality_type()
    {
        return $this->belongsTo('App\Mortality_Type');
    }
    
}
