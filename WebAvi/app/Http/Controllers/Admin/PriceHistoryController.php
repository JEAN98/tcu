<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\PriceHistory;
use Illuminate\Http\Request;

class PriceHistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 10;

        if (!empty($keyword)) {
            $pricehistory = PriceHistory::where('name', 'LIKE', "%$keyword%")
                ->orWhere('price', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $pricehistory = PriceHistory::latest()->paginate($perPage);
        }

        return view('admin.price-history.index', compact('pricehistory'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.price-history.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'price' => 'required'
		]);
        $requestData = $request->all();
        
        PriceHistory::create($requestData);

        return redirect('admin/price-history')->with('flash_message', 'PriceHistory added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $pricehistory = PriceHistory::findOrFail($id);

        return view('admin.price-history.show', compact('pricehistory'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $pricehistory = PriceHistory::findOrFail($id);

        return view('admin.price-history.edit', compact('pricehistory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'name' => 'required',
            'price' => 'required'
		]);
        $requestData = $request->all();
        
        $pricehistory = PriceHistory::findOrFail($id);
        $pricehistory->update($requestData);

        return redirect('admin/price-history')->with('flash_message', 'PriceHistory updated!');
    }

    
    
    
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */

    public function destroy($id)
    {
        try{
            PriceHistory::destroy($id);
            return redirect('admin/price-history')->with('flash_message', 'PriceHistory deleted!');
        }
        catch(\Illuminate\Database\QueryException $exception )
        {
            if($exception->errorInfo[0] == 23000)
            {
                $error["message"] = "¡No puede eleminar la entrada selecionada,".
                                    " ya que tiene información ligada en otras tablas!"
                                    ." Para poder eliminar el elemento debe elminar primero la información que tiene ligada"; 
                
                $errors = $error;  
            }
            else{
                $error["message"] = "Se encontró un problema, por lo que no se puede eliminar la entrada selecionada";
                $errors = $error; 
            }
            return Redirect::back()->withErrors($errors);
        }
    }
}
