<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\ProductionRate;
use App\Galpon;
use App\Parvada;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;

class ProductionRatesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 10;

        if (!empty($keyword)) {
            $productionrates = ProductionRate::where('parvada_id', 'LIKE', "%$keyword%")
                ->orWhere('total_eggs_collected', 'LIKE', "%$keyword%")
                ->orWhere('total_good_eggs', 'LIKE', "%$keyword%")
                ->orWhere('total_broken_eggs', 'LIKE', "%$keyword%")
                ->orWhere('current_chickens_quantity', 'LIKE', "%$keyword%")
                ->orWhere('percentage', 'LIKE', "%$keyword%")
                ->orWhere('date_collected', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $productionrates = ProductionRate::latest()->paginate($perPage);
        }
        $productionrates = $this->set_galpon_code_list($productionrates);

        return view('admin.production-rates.index', compact('productionrates'));
    }

    private function set_galpon_code_list($production_rates)
    {
        foreach ($production_rates as $production_rate) {
            $production_rate = $this->set_galpon_code($production_rate);
        }
        return $production_rates;
    }

    private function set_galpon_code($production_rate)
    {
        $current_parvada = Parvada::findOrFail($production_rate->parvada_id);
        $production_rate["galpon_code"] = (Galpon::findOrFail($current_parvada->galpon_id))->galpon_code;
        $production_rate->percentage = floor(($production_rate->percentage*1000))/1000;
        return $production_rate;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        /*FIXED: Show only the galpons with parvadas active*/
        $galpon_list = DB::table('galpons')
                      ->join('parvadas','parvadas.galpon_id','=','galpons.id')
                      ->where('parvadas.parvada_status','=','1')
                      ->pluck('galpons.galpon_code','galpons.id')
                      ->all();
        return view('admin.production-rates.create',compact('galpon_list'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {

        $this->validate($request, [
			
			'galpon_id' => 'required',
			'total_good_eggs' => 'required',
			'date_collected' => 'required'
        ]);
        $requestData = $request->all();
        $requestData["total_broken_eggs"] = $requestData["total_eggs_collected"] - $requestData["total_good_eggs"]; 

        $percentage =  ($requestData["total_good_eggs"]*100)/$requestData["total_eggs_collected"];
        $requestData["percentage"] = $percentage;
        $current_parvada = Parvada::where('parvada_status','=', '1')
                                ->where('galpon_id','=', $requestData["galpon_id"])
                                ->first();
        if($current_parvada)
        {
            $requestData["current_chickens_quantity"] = $current_parvada->current_chickens_quantity;
            $requestData["parvada_id"] = $current_parvada->id;
            ProductionRate::create($requestData);
            return redirect('admin/production-rates')->with('flash_message', 'ProductionRate added!');
        }
        else
        {
            $errors["message"] = "El galpón selecionado no tiene ninguna parvada activa!";
            return Redirect::back()->withErrors($errors);
        }
       // unset($requestData["galpon_id"]);   
        //echo(var_dump($requestData ));
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $productionrate = ProductionRate::findOrFail($id);
        $productionrate =$this->set_galpon_code($productionrate);
        return view('admin.production-rates.show', compact('productionrate'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $galpon_list = Galpon::pluck('galpon_code','id');
        $productionrate = ProductionRate::findOrFail($id);

        return view('admin.production-rates.edit', compact('productionrate','galpon_list'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'galpon_id' => 'required',
			'total_good_eggs' => 'required',
			'date_collected' => 'required'
		]);
        $requestData = $request->all();
        $productionrate = ProductionRate::findOrFail($id);
        $requestData["total_broken_eggs"] = $productionrate->total_broken_eggs;
        $productionrate->update($requestData);

        return redirect('admin/production-rates')->with('flash_message', 'ProductionRate updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try{
            ProductionRate::destroy($id);
            return redirect('admin/production-rates')->with('flash_message', 'ProductionRate deleted!');
        }
        catch(\Illuminate\Database\QueryException $exception )
        {
            if($exception->errorInfo[0] == 23000)
            {
                $error["message"] = "¡No puede eleminar la entrada selecionada,".
                                    " ya que tiene información ligada en otras tablas!"
                                    ." Para poder eliminar el elemento debe elminar primero la información que tiene ligada"; 
                
                $errors = $error;  
            }
            else{
                $error["message"] = "Se encontró un problema, por lo que no se puede eliminar la entrada selecionada";
                $errors = $error; 
            }
            return Redirect::back()->withErrors($errors);
        }
    }

    public function filter_avarage_by_dates(Request $request)
    {
        if($request->ajax())
        {
            $dates = $_GET['dates'];
            $status = true;
            $avarage_result = (DB::select('select round((SUM(percentage)/COUNT(*)),2) as avarage , date_collected,
                                SUM(total_good_eggs) as total_good_eggs, SUM(total_eggs_collected) as total_eggs_collected,
                                SUM(total_broken_eggs) as total_broken_eggs
                                from `production_rates`
                                where date_collected between ? and ?
                                group by date_collected
                                order by date_collected asc', 
                                [$dates["start_date"],$dates["end_date"]]));
            $date_list = array();
            $avarage_list = array();
            $sum = 0;
            $total_good_eggs = 0;
            $total_eggs_collected = 0;
            $total_eggs_lost = 0;

            for ($i=0; $i < count($avarage_result); $i++) { 
                $date_list[$i] = $avarage_result[$i]->date_collected;
                $avarage_list[$i] = $avarage_result[$i]->avarage;
                $sum += $avarage_result[$i]->avarage;
                $total_good_eggs +=  $avarage_result[$i]->total_good_eggs;
                $total_eggs_collected += $avarage_result[$i]->total_eggs_collected;
                $total_eggs_lost += $avarage_result[$i]->total_broken_eggs;
            }
            if(!$avarage_result)
            {
                $status = false;
            }
            return response()->json([
               // 'avarage'=> number_format($avarage_result,2)
               'date_list'=> $date_list,
               'avarage_list'=> $avarage_list,
               'general_avarage' => number_format($sum / count($avarage_result), 2),
               'total_good_eggs' => $total_good_eggs,
               'total_eggs_collected' => $total_eggs_collected,
               'total_eggs_lost' => $total_eggs_lost,
               'status'=> $status
            ]);
        }
    }

    public function chart()
    {
      $result = ProductionRate::all()->get();
      return response()->json($result);
    }

}
