<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use App\Client;
use Illuminate\Http\Request;

class ClientsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 10;

        if (!empty($keyword)) {
            $clients = Client::where('name', 'LIKE', "%$keyword%")
                ->orWhere('location', 'LIKE', "%$keyword%")
                ->orWhere('email', 'LIKE', "%$keyword%")
                ->orWhere('phone', 'LIKE', "%$keyword%")
                ->orWhere('card_id', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $clients = Client::latest()->paginate($perPage);
        }

        return view('admin.clients.index', compact('clients'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.clients.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'name' => 'required',
			'phone' => 'required',
			'location' => 'required'
		]);
        $requestData = $request->all();
        $is_exist = $this->verify_Name_Card_Exits($requestData["name"],$requestData["card_id"]);
        if(!$is_exist["status"])
        {
            Client::create($requestData);

            return redirect('admin/clients')->with('flash_message', 'Client added!');
        }
        else
        {
            //echo(json_encode($is_exist));
             $errors = $is_exist["message"];
             return Redirect::back()->withErrors($errors);
        }
      
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $client = Client::findOrFail($id);

        return view('admin.clients.show', compact('client'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $client = Client::findOrFail($id);

        return view('admin.clients.edit', compact('client'));
    }

    private function verify_Name_Card_Exits($new_name,$new_card)
    {
        $is_exist["status"] = false;
        $name_Count = count(Client::where('name',$new_name)->get());
        $card_Count = count(Client::where('card_id',$new_card)->get());
        if( $name_Count > 0 & $card_Count > 0)
        {
            $is_exist["status"] = true;
            $is_exist["name_Count"] = $name_Count;
            $is_exist["card_Count"] = $card_Count;
            $is_exist["message"] = "El nombre o el código ya existen dentro de la base de datos".
            "por favor utilizar uno nuevo";
            
        }
        else if( $name_Count > 0)
        {
            $is_exist["status"] = true;
            $is_exist["name_Count"] = $name_Count;
            $is_exist["card_Count"] = $card_Count;
            $is_exist["message"] = "El nombre ".$new_name ." existe dentro de la base de datos".
            "por favor utilizar un nombre nuevo";
        }
        else if($card_Count > 0)
        {
            $is_exist["status"] = true;
            $is_exist["name_Count"] = $name_Count;
            $is_exist["card_Count"] = $card_Count;
            $is_exist["message"] = "La identificación ".$new_card ." existe dentro de la base de datos".
            "por favor utilizar una identificación nueva";
        }

        return $is_exist;
    }


    
    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'name' => 'required',
			'phone' => 'required',
			'location' => 'required'
		]);
        $requestData = $request->all();
        
        $client = Client::findOrFail($id);
        if($client->name == $requestData["name"]  & $client->card_id == $requestData["card_id"])
        {
            $client->update($requestData);
            return redirect('admin/clients')->with('flash_message', 'Client updated!');
        }

        $is_exist = $this->verify_Name_Card_Exits($requestData["name"],$requestData["card_id"]);
        if( $client->name == $requestData["name"] & $client->card_id != $requestData["card_id"])
        {
            if($is_exist["card_Count"] == 0)
            {
                $client->update($requestData);
                return redirect('admin/clients')->with('flash_message', 'Client updated!');
            }
        }
        else if($client->name != $requestData["name"] & $client->card_id == $requestData["card_id"])
        {
            if($is_exist["name_Count"] == 0)
            {
                $client->update($requestData);
                return redirect('admin/clients')->with('flash_message', 'Client updated!');
            }
        }

        $errors = $is_exist["message"];
        return Redirect::back()->withErrors($errors);
        //echo(json_encode($is_exist));
               
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try{
            Client::destroy($id);
            return redirect('admin/clients')->with('flash_message', 'Client deleted!');
        }
        catch(\Illuminate\Database\QueryException $exception )
        {
            if($exception->errorInfo[0] == 23000)
            {
                $error["message"] = "¡No puede eleminar la entrada selecionada,".
                                    " ya que tiene información ligada en otras tablas!"
                                    ." Para poder eliminar el elemento debe elminar primero la información que tiene ligada"; 
                
                $errors = $error;  
            }
            else{
                $error["message"] = "Se encontró un problema, por lo que no se puede eliminar la entrada selecionada";
                $errors = $error; 
            }
            return Redirect::back()->withErrors($errors);
        }
    }
}
