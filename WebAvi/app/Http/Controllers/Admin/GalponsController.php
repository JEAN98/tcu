<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use App\Galpon;
use Illuminate\Http\Request;

class GalponsController extends Controller
{
  
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 10;

        if (!empty($keyword)) {
            $galpons = Galpon::where('galpon_code', 'LIKE', "%$keyword%")
                ->orWhere('meters_large', 'LIKE', "%$keyword%")
                ->orWhere('meters_width', 'LIKE', "%$keyword%")
                ->orWhere('maximum_chickens', 'LIKE', "%$keyword%")
                ->orWhere('construction_date', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $galpons = Galpon::latest()->paginate($perPage);
        }

        return view('admin.galpons.index', compact('galpons'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.galpons.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $this->validate($request, [
			'galpon_code' => 'required'
        ]);

        /*
        TODO: Remember add the entrepise model
         */
        $enterprise_id = 1;
        $requestData = $request->all();
        $requestData["enterprise_id"] =  $enterprise_id;
        if($this->verify_galpon_code_exists($requestData["galpon_code"]))
        {
            $errors = "El código " .$requestData["galpon_code"]
                    ." para la sección  de galpones ya existe en la base de datos! Por favor ingrese un código nuevo";
            return Redirect::back()->withErrors($errors);
        }
       
        // echo(var_dump($requestData));
        Galpon::create($requestData);

        return redirect('admin/galpons')->with('flash_message', 'Galpon added!');
    }

    private function verify_galpon_code_exists($new_galpon_code)
    {
        $galpon_list = Galpon::where('galpon_code',$new_galpon_code)->get();
        if(count($galpon_list) > 0)
        {   
            return true;
        }
        return false;
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $galpon = Galpon::findOrFail($id);

        return view('admin.galpons.show', compact('galpon'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $galpon = Galpon::findOrFail($id);

        return view('admin.galpons.edit', compact('galpon'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'galpon_code' => 'required'
		]);
        $requestData = $request->all();
        
        $galpon = Galpon::findOrFail($id);

        if($galpon->galpon_code != $requestData["galpon_code"])
        {
            if($this->verify_galpon_code_exists($requestData["galpon_code"]))
            {
                $errors = "El código " .$requestData["galpon_code"]
                        ." para la sección  de galpones ya existe en la base de datos! Por favor vuelva a buscar la información del galpón para editarla";
                return Redirect::back()->withErrors($errors);
            }
        }

        $galpon->update($requestData);

        return redirect('admin/galpons')->with('flash_message', 'Galpon updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try{
            Galpon::destroy($id);
            return redirect('admin/galpons')->with('flash_message', 'Galpon deleted!');
        }
        catch(\Illuminate\Database\QueryException $exception )
        {
            if($exception->errorInfo[0] == 23000)
            {
                $error["message"] = "¡No puede eleminar la entrada selecionada,".
                                    " ya que tiene información ligada en otras tablas!"
                                    ." Para poder eliminar el elemento debe elminar primero la información que tiene ligada"; 
                
                $errors = $error;  
            }
            else{
                $error["message"] = "Se encontró un problema, por lo que no se puede eliminar la entrada selecionada";
                $errors = $error; 
            }
          return Redirect::back()->withErrors($errors);
           // echo(json_encode($exception->errorInfo));
        }
    }
}
