<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Parvada;
use App\Galpon;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;

class ParvadasController extends Controller
{
    public function __construct()
    {
       // $this->middleware('auth');
    }
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 10;

        if (!empty($keyword)) {
            $parvadas = Parvada::where('admission_date', 'LIKE', "%$keyword%")
                ->orWhere('entry_age', 'LIKE', "%$keyword%")
                ->orWhere('id', 'LIKE', "%$keyword%")
                ->orWhere('race', 'LIKE', "%$keyword%")
                ->orWhere('beginning_posture', 'LIKE', "%$keyword%")
                ->orWhere('posture_age', 'LIKE', "%$keyword%")
                ->orWhere('start_quantity_chickens', 'LIKE', "%$keyword%")
                ->orWhere('average_weight', 'LIKE', "%$keyword%")
                ->orWhere('parvada_status', 'LIKE', "%$keyword%")
                ->orWhere('galpon_id', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
                /*TODO: Make join here with galpon table    
                 */
                //echo(var_dump(count($parvadas)));
        } else {
            $parvadas = Parvada::latest()->paginate($perPage);
        }
        $parvadas = $this->set_status_galpon_parvada_list($parvadas);
        return view('admin.parvadas.index', compact('parvadas'));
    }

    private function set_status_galpon_parvada_list($parvadas)
    {
        
        foreach ($parvadas as $parvada) {
            $parvada["status"] = $this->set_status($parvada->parvada_status);
            $parvada["galpon_code"] = $this->get_galpon_code($parvada->galpon_id);
        }
        return $parvadas;
    }

    private function get_galpon_list()
    {
       return Galpon::pluck('galpon_code','id');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $galpon_list = $this->get_galpon_list();
        return view('admin.parvadas.create',compact('galpon_list'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
       
        $this->validate($request, [
			'start_quantity_chickens' => 'required'
        ]);
        $requestData = $request->all();
        $result = $this->verify_new_status($requestData["parvada_status"],$requestData["galpon_id"]);
        if( $result["is_handle"] == false)
        {
            $errors = $result["message"];
            return Redirect::back()->withErrors($errors);
        }
       
        $requestData["current_chickens_quantity"] = $request->start_quantity_chickens;
        Parvada::create($requestData);
        return redirect('admin/parvadas');
    }

    private function verify_new_status($status,$galpon_id)
    {
        $result["is_handle"] = true;
        if($status == 1)
        {
            $current_parvada = Parvada::where('parvada_status','=', '1')
                                ->where('galpon_id','=', $galpon_id)
                                ->first();
           //echo json_encode($current_parvada);
            if($current_parvada)
            {
                $galpon_code = $this->get_galpon_code($galpon_id);
                $result["is_handle"] = false;
                $result["message"]  = "Por favor desactivar la parvada con respecto al galpón ".$galpon_code. 
                ", luego puede activar una nueva! ".
                "Puede buscar la parvada activa por medio de este [N° Parvada : ".$current_parvada->id."  ]";
            }                                    
        }
        return $result;
        
    }

    private function get_galpon_code($galpon_id)
    {
        return Galpon::findOrFail($galpon_id)->galpon_code;
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $parvada = Parvada::findOrFail($id);
        $parvada["galpon_code"] =  $this->get_galpon_code($parvada->galpon_id);
        $parvada["status"] = $this->set_status($parvada->parvada_status);
        
        //echo(var_dump($parvada));
        
        return view('admin.parvadas.show', compact('parvada'));
    }

    private function set_status($status)
    {
        if($status === 0)
        {
            return "Desactiva";
        }
        return  "Activa";
    }

  

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $galpon_list = $this->get_galpon_list();
        $parvada = Parvada::findOrFail($id);

        return view('admin.parvadas.edit', compact('parvada','galpon_list'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $parvada = Parvada::findOrFail($id);
        $result = $this->verify_new_status($requestData["parvada_status"],$requestData["galpon_id"]);
        if( $result["is_handle"] == false)
        {
            $errors = $result["message"];
            return Redirect::back()->withErrors($errors);
        }
        $parvada->update($requestData);
        return redirect('admin/parvadas')->with('flash_message', 'Parvada updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try{
            Parvada::destroy($id);
            return redirect('admin/parvadas')->with('flash_message', 'Parvada deleted!');
        }
        catch(\Illuminate\Database\QueryException $exception )
        {
            if($exception->errorInfo[0] == 23000)
            {
                $error["message"] = "¡No puede eleminar la entrada selecionada,".
                                    " ya que tiene información ligada en otras tablas!"
                                    ." Para poder eliminar el elemento debe elminar primero la información que tiene ligada"; 
                
                $errors = $error;  
            }
            else{
                $error["message"] = "Se encontró un problema, por lo que no se puede eliminar la entrada selecionada";
                $errors = $error; 
            }
            
         return Redirect::back()->withErrors($errors);
           // echo(json_encode($exception->errorInfo));
        }
    }
}
