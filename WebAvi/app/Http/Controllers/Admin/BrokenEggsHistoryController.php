<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\BrokenEggsHistory;
use App\ProductionRate;
use App\BrokenEggsCause;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class BrokenEggsHistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 10;

        if (!empty($keyword)) {
            $brokeneggshistory = BrokenEggsHistory::where('causa_id', 'LIKE', "%$keyword%")
                ->orWhere('production_id', 'LIKE', "%$keyword%")
                ->orWhere('quantity', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $brokeneggshistory = BrokenEggsHistory::latest()->paginate($perPage);
        }

        $brokeneggshistory = $this->set_causes_brokeneggshistory_list($brokeneggshistory);
        return view('admin.broken-eggs-history.index', compact('brokeneggshistory'));
    }

    private function set_causes_brokeneggshistory_list($brokeneggshistories)
    {
        foreach ($brokeneggshistories as $brokeneggshistory) {
            $brokeneggshistory = $this->set_cause_by_history($brokeneggshistory);
        }
        return $brokeneggshistories;
    }

    private function set_cause_by_history($brokeneggshistory)
    {
        $brokeneggshistory["cause_name"] = BrokenEggsCause::findOrFail($brokeneggshistory->causa_id)->name;
        return $brokeneggshistory;
    }

    private function set_current_quantity_broken_eggs($production_id,$eggs_quantity)
    {
        $sum =  DB::table('broken_eggs_histories')->where('production_id', '=', $production_id)->sum('quantity');
        $current_production = ProductionRate::findOrFail($production_id);
        $current_production->total_broken_eggs =  (int)$sum - $eggs_quantity;
        //echo json_encode($current_production);
        $current_production->update(((array)$current_production));
    }


    private function verify_new_quantiy_available($production_id,$new_eggs_quantity)
    {
        $current_quantity = DB::table('broken_eggs_histories')->where('production_id', '=', $production_id)->sum('quantity');
        $total_broken_eggs = ProductionRate::findOrFail($production_id)->total_broken_eggs;
        
        if($new_eggs_quantity <= 0)
        {
            $result["message"] = "¡La cantidad debe ser mayor a cero!";
            $result["status"] = false;
        }
        else if($current_quantity == $total_broken_eggs)
        {
             $result["message"] = "¡Ya la cantidad máxima fue alcanzada, por lo que no se puede ingresar más registros con respecto a esta producción!";
             $result["status"] = false;
        }
        else if(($new_eggs_quantity + $current_quantity) <=  $total_broken_eggs)
        {
            echo("Si se puede agregar la nueva cantidad");
            $result["message"] = "Si se puede agregar la nueva cantidad";
            $result["status"] = true;
        }
        else
        {
             $result["message"] = "¡El número que está ingresando supera el límite establecido para los registros de huevos perdidos para el código de producción (".
                                    $production_id."), porfavor ingresar un número menor!  [".
                                    $current_quantity." es la cantidad actual establecida y se deben llenar como máximo ".$total_broken_eggs."]";
             $result["status"] = false;
        }    
        return $result;
    }


    private function get_production_rates_active()
    {
        return DB::table('production_rates')
                    ->join('parvadas','parvadas.id','=','production_rates.parvada_id')
                    ->where('parvadas.parvada_status','=','1')
                    ->pluck('production_rates.id','production_rates.id')
                    ->all();
    }

    private function get_broken_eggs_causes()
    {
        return BrokenEggsCause::pluck('name','id'); 
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {   
        $current_production_rates = $this->get_production_rates_active();
        $broken_eggs_causes = $this->get_broken_eggs_causes();
        return view('admin.broken-eggs-history.create',compact('current_production_rates','broken_eggs_causes'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'quantity' => 'required'
		]);
        $requestData = $request->all();
        
        $result = $this->verify_new_quantiy_available($requestData["production_id"],$requestData["quantity"]);
        if($result["status"] == true)
        {
            BrokenEggsHistory::create($requestData);
            // $this->set_current_quantity_broken_eggs($requestData["production_id"],0);
     
            return redirect('admin/broken-eggs-history')->with('flash_message', 'BrokenEggsHistory added!');
        }
        $errors = $result["message"];
        return Redirect::back()->withErrors($errors);
        //echo($result["message"]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $brokeneggshistory = BrokenEggsHistory::findOrFail($id);

        return view('admin.broken-eggs-history.show', compact('brokeneggshistory'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $brokeneggshistory = BrokenEggsHistory::findOrFail($id);
        $current_production_rates = $this->get_production_rates_active();
        $broken_eggs_causes = $this->get_broken_eggs_causes();
        return view('admin.broken-eggs-history.edit', compact('brokeneggshistory','current_production_rates','broken_eggs_causes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {   
        $this->validate($request, [
			'quantity' => 'required'
		]);
        $requestData = $request->all();
        
        //Sum of broken eggs on production rate needs to be updated as well
        if($result["status"] == true)
        {
            $brokeneggshistory = BrokenEggsHistory::findOrFail($id);
            $brokeneggshistory->update($requestData);
          //  $this->set_current_quantity_broken_eggs($brokeneggshistory->production_id,0);
    
            return redirect('admin/broken-eggs-history')->with('flash_message', 'BrokenEggsHistory updated!');
        }
        echo($result["message"]);
       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $brokeneggshistory = BrokenEggsHistory::findOrFail($id);
       // $this->set_current_quantity_broken_eggs($brokeneggshistory->production_id,$brokeneggshistory->quantity);
        BrokenEggsHistory::destroy($id);
         //TODO:Sum of broken eggs on production_rate needs to be updated as well, according to new quantiy 
        return redirect('admin/broken-eggs-history')->with('flash_message', 'BrokenEggsHistory deleted!');
    }
}
