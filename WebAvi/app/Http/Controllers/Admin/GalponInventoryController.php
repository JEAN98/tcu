<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\GalponInventory;
use App\Galpon;
use Illuminate\Http\Request;

class GalponInventoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 10;

        if (!empty($keyword)) {
            $galponinventory = GalponInventory::where('galpon_code', 'LIKE', "%$keyword%")
                ->orWhere('feeders_quantity', 'LIKE', "%$keyword%")
                ->orWhere('reserved_feeders_quantity', 'LIKE', "%$keyword%")
                ->orWhere('drinking_fountains_quantity', 'LIKE', "%$keyword%")
                ->orWhere('reserved_drinking_fountains_quantity', 'LIKE', "%$keyword%")
                ->orWhere('light_bulbs', 'LIKE', "%$keyword%")
                ->orWhere('reserved_light_bulbs', 'LIKE', "%$keyword%")
                ->orWhere('chiken_nests', 'LIKE', "%$keyword%")
                ->orWhere('is_thermometer', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $galponinventory = GalponInventory::latest()->paginate($perPage);
        }

        return view('admin.galpon-inventory.index', compact('galponinventory'));
    }


    private function get_galpons()
    {
        return Galpon::pluck('galpon_code','id');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $galpons = $this->get_galpons();
        return view('admin.galpon-inventory.create',compact('galpons'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        $this->validate($request, [
			'galpon_code' => 'required',
            'feeders_quantity' => 'required',
            'reserved_feeders_quantity'  => 'required',
            'drinking_fountains_quantity'   => 'required',
            'reserved_drinking_fountains_quantity'   => 'required',
            'light_bulbs'   => 'required',
            'reserved_light_bulbs'   => 'required',
            'chiken_nests'   => 'required',
            'is_thermometer'   => 'required'
        ]);
        GalponInventory::create($requestData);

        return redirect('admin/galpon-inventory')->with('flash_message', 'GalponInventory added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $galponinventory = GalponInventory::findOrFail($id);

        return view('admin.galpon-inventory.show', compact('galponinventory'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $galponinventory = GalponInventory::findOrFail($id);
        $galpons = $this->get_galpons();
        return view('admin.galpon-inventory.edit', compact('galponinventory','galpons'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $galponinventory = GalponInventory::findOrFail($id);
        $galponinventory->update($requestData);

        return redirect('admin/galpon-inventory')->with('flash_message', 'GalponInventory updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        GalponInventory::destroy($id);

        return redirect('admin/galpon-inventory')->with('flash_message', 'GalponInventory deleted!');
    }
}
