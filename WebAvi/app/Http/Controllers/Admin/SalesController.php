<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Sale;
use App\PriceHistory;
use App\Client;
use Illuminate\Http\Request;

class SalesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 10;

        if (!empty($keyword)) {
            $sales = Sale::where('total_kilograms', 'LIKE', "%$keyword%")
                ->orWhere('price_per_kilogram', 'LIKE', "%$keyword%")
                ->orWhere('total_price', 'LIKE', "%$keyword%")
                ->orWhere('client_id', 'LIKE', "%$keyword%")
                ->orWhere('date', 'LIKE', "%$keyword%")
                ->orWhere('payment_type', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->orWhere('invoice_id', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $sales = Sale::latest()->paginate($perPage);
        }
        $sales = $this->set_information_sales_list($sales);

      return view('admin.sales.index', compact('sales'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        //   $price_per_kilogram = $this->get_current_price_per_kilogram();
       $prices_per_kilogram = $this->get_prices();
        $clients = $this->get_clients();
        if(!$clients)
        {
            $clients = [];
        }
        $total_kilograms = 0;
        return view('admin.sales.create',compact('clients','prices_per_kilogram','total_kilograms'));
    }

    private function get_clients()
    {
        return Client::pluck('name','id');
    }


    private function get_prices()
    {
        return PriceHistory::pluck('name','id');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'total_kilograms' => 'required',
			'date' => 'required',
			'status' => 'required',
			'payment_type' => 'required'
		]);
        $requestData = $request->all();
        $requestData = $this->update_entry_to_current_prices($requestData);
        //echo(json_encode($requestData));
        Sale::create($requestData);
        return redirect('admin/sales')->with('flash_message', 'Sale added!');
    }

    private  function update_entry_to_current_prices($requestData)
    {
        $current_price = PriceHistory::findOrFail($requestData["prices_per_kilogram"])->price;
        $requestData["price_per_kilogram"] =  $current_price;
        $requestData["total_price"] = $current_price * floatval($requestData["total_kilograms"]);
        if(!$requestData["invoice_id"])
        {
            $requestData["invoice_id"] = "No se ha agregado la factura";
        }
        return $requestData;
    }

    private function set_information_sales_list($sales)
    {
       // echo(var_dump($sales));
        foreach ($sales as $sale) {
            $sale  = $this->set_information_sale($sale);
        }
        return $sales;
    }

    private function set_information_sale($sale)
    {
        $sale["client"] = (Client::findOrFail($sale->client_id ))->name;
        if($sale->payment_type == "deposit_in_the_bank")
        {
            $sale["payment_type"] = "Déposito en el banco";
        }
        else if($sale->payment_type == "credit")
        {
            $sale["payment_type"] = "Efectivo";
        }
        else{
            $sale["payment_type"] = "Cheque";
        }
        return $sale;
    }

    public function get_price_by_id(Request $request )
    {
        if($request->ajax())
        { 
            $priceID = $_GET['priceID'];
            $price = PriceHistory::findOrFail($priceID)->price;
            if(!$price)
            {
                $price = 0;
            }
            return response()->json([
                'price_by_kilogram'=> $price
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $sale = Sale::findOrFail($id);
        $sale  = $this->set_information_sale($sale);
        return view('admin.sales.show', compact('sale'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $sale = Sale::findOrFail($id);
        $prices_per_kilogram = $this->get_prices();
        $clients = $this->get_clients();
        if(!$clients)
        {
            $clients = [];
        }
        $total_kilograms =  $sale->total_kilograms;
        return view('admin.sales.edit', compact('sale','clients','prices_per_kilogram','total_kilograms'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'total_kilograms' => 'required',
			'date' => 'required',
			'status' => 'required',
			'payment_type' => 'required'
		]);
        $requestData = $request->all();
        $requestData = $this->update_entry_to_current_prices($requestData);
            
        $sale = Sale::findOrFail($id);
        $sale->update($requestData);

        return redirect('admin/sales')->with('flash_message', 'Sale updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Sale::destroy($id);

        return redirect('admin/sales')->with('flash_message', 'Sale deleted!');
    }

    private function sales_result_by_dates($dates)
    {
            
        $sales_result = DB::select('select SUM(total_price) total , date , COUNT(*) as quantity
            from `sales`
            where date between ? and ?
            group by date
            order by date asc',
            [$dates["start_date"],$dates["end_date"]]);
        
        $total_sales_list = array();
        $date_list = array();
        $total_sales = 0;
        $total_quantity = 0;
        for ($i=0; $i < count($sales_result); $i++) 
        {
            $total_sales_list[$i] = $sales_result[$i]->total;
            $date_list[$i] = $sales_result[$i]->date;
            $total_sales +=  $sales_result[$i]->total;
            $total_quantity += $sales_result[$i]->quantity;
        }
        $sales_result_by_dates["total_sales_list"] = $total_sales_list;
        $sales_result_by_dates["date_list"] = $date_list;
        $sales_result_by_dates["total_sales"] = $total_sales;
        $sales_result_by_dates["total_quantity"] = $total_quantity;

        return $sales_result_by_dates;

    }
    private function get_sales_pending($dates)
    {
        $pending_sales = (DB::select('select COUNT(*) as quantity
                                     from sales 
                                     where date between ? and ? 
                                     and status = "Pendiente"',
                        [$dates["start_date"],$dates["end_date"]]))[0]->quantity;
        return $pending_sales;             
    }

    public function filter_sales_by_dates(Request $request)
    {
        if($request->ajax())
        { 
            $dates = $_GET['dates'];
            $sales_result = $this->sales_result_by_dates($dates);
          
            return response()->json([
                'total_sales'=> number_format($sales_result["total_sales"],2),
                'total_sales_list' => $sales_result["total_sales_list"],
                'date_list' => $sales_result["date_list"],
                'total_quantity' => $sales_result["total_quantity"],
                'pending_sales' => $this->get_sales_pending($dates)
            ]);
        }
    }
}
