<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\EggsReturn;
use App\Client;
use Illuminate\Http\Request;

class EggsReturnController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 10;

        if (!empty($keyword)) {
            $eggsreturn = EggsReturn::where('client_id', 'LIKE', "%$keyword%")
                ->orWhere('quantity', 'LIKE', "%$keyword%")
                ->orWhere('date', 'LIKE', "%$keyword%")
                ->orWhere('comment', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $eggsreturn = EggsReturn::latest()->paginate($perPage);
        }
        $eggsreturn =  $this->set_client_information($eggsreturn);
        return view('admin.eggs-return.index', compact('eggsreturn'));
    }

    private function set_client_information($eggsreturn_list)
    {
        foreach ($eggsreturn_list as $eggsreturn) {
            $eggsreturn["client"] = $this->get_client($eggsreturn->client_id);
        }
        return $eggsreturn_list;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $clients =  $this->get_clients();
        return view('admin.eggs-return.create',compact('clients'));
    }

    private function get_clients()
    {
        return Client::pluck('name','id');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'client_id' => 'required',
			'quantity' => 'required',
			'date' => 'required'
		]);
        $requestData = $request->all();
        
        EggsReturn::create($requestData);

        return redirect('admin/eggs-return')->with('flash_message', 'EggsReturn added!');
    }

    private function get_client($client_id)
    {
        return Client::findOrFail($client_id)->name;
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $eggsreturn = EggsReturn::findOrFail($id);
        $client = $this->get_client($eggsreturn->client_id);
        return view('admin.eggs-return.show', compact('eggsreturn','client'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $eggsreturn = EggsReturn::findOrFail($id);
        $clients =  $this->get_clients();
        return view('admin.eggs-return.edit', compact('eggsreturn','clients'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'name' => 'required',
			'quantity' => 'required',
			'date' => 'required'
		]);
        $requestData = $request->all();
        
        $eggsreturn = EggsReturn::findOrFail($id);
        $eggsreturn->update($requestData);

        return redirect('admin/eggs-return')->with('flash_message', 'EggsReturn updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        EggsReturn::destroy($id);

        return redirect('admin/eggs-return')->with('flash_message', 'EggsReturn deleted!');
    }
}
