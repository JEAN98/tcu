<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Galpon;
use App\Parvada;
use App\Mortality;
use App\Mortality_Type;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;

class MortalitiesController extends Controller
{
  
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 10;

        if (!empty($keyword)) {
            $mortalities = Mortality::where('parvada_id', 'LIKE', "%$keyword%")
                ->orWhere('chikens_quantity', 'LIKE', "%$keyword%")
                ->orWhere('death_date', 'LIKE', "%$keyword%")
                ->orWhere('mortality_type_id', 'LIKE', "%$keyword%")
                ->orWhere('comment', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $mortalities = Mortality::latest()->paginate($perPage);
        }
        $mortalities =  $this->set_galpon_type_code_mortalities($mortalities);
        return view('admin.mortalities.index', compact('mortalities'));
    }

    private function set_galpon_type_code_mortalities($mortalities)
    {
        foreach ($mortalities as $mortality) {
            $mortality =  $this->set_galpon_type_code_mortality($mortality);
        }
        return $mortalities;
    }

    private function set_galpon_type_code_mortality($mortality)
    {
        $galpon_id = (Parvada::findOrFail($mortality->parvada_id))->galpon_id;
        $mortality["galpon_code"] = Galpon::findOrFail($galpon_id)->galpon_code;
        $mortality["type"] = (Mortality_Type::findOrFail($mortality->mortality_type_id))->type;
        //echo(json_encode($mortality));
        return  $mortality;
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $galpon_list = DB::table('galpons')
                        ->join('parvadas','parvadas.galpon_id','=','galpons.id')
                        ->where('parvadas.parvada_status','=','1')
                        ->where('parvadas.current_chickens_quantity','>','0')
                        ->pluck('galpons.galpon_code','galpons.id')
                        ->all();
        $mortality_types = Mortality_Type::pluck('type','id');

        return view('admin.mortalities.create',compact('galpon_list','mortality_types'));
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'chikens_quantity' => 'required',
            'galpon_id' => 'required',
            'mortality_type_id'  => 'required',
            'death_date'   => 'required'
        ]);
        
        $requestData = $request->all();
        $current_parvada = Parvada::where('parvada_status','=', '1')
                                ->where('galpon_id','=', $requestData["galpon_id"])
                                ->first();
        $requestData["parvada_id"] = $current_parvada->id;   

        $this->decrease_chikens_quantity_parvada($current_parvada,$requestData["chikens_quantity"]);
        
        Mortality::create($requestData);
        return redirect('admin/mortalities')->with('flash_message', 'Mortality added!');
    }

    private function decrease_chikens_quantity_parvada($parvada,$death_chikens_quantity)
    {
        $new_quantity = 0;
        $chikens_subtraction_result = $parvada->current_chickens_quantity - $death_chikens_quantity;
        if( $chikens_subtraction_result > 0 )
          {
            $new_quantity = $chikens_subtraction_result;
          } 
        $parvada->current_chickens_quantity = $new_quantity;
        $parvada_updated = ((array)$parvada);
        $parvada->update($parvada_updated);
    }


    private function increase_chikens_quantity_parvada($parvada,$death_chikens_quantity)
    {
        $parvada->current_chickens_quantity += $death_chikens_quantity;
        $parvada_updated = ((array)$parvada);
        $parvada->update($parvada_updated);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $mortality = Mortality::findOrFail($id);
        $mortality = $this->set_galpon_type_code_mortality($mortality);
        return view('admin.mortalities.show', compact('mortality'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $mortality = Mortality::findOrFail($id);
        $galpon_list = DB::table('galpons')
                        ->join('parvadas','parvadas.galpon_id','=','galpons.id')
                        ->where('parvadas.parvada_status','=','1')
                        ->where('parvadas.current_chickens_quantity','>','0')
                        ->pluck('galpons.galpon_code','galpons.id')
                        ->all();
        $mortality_types = Mortality_Type::pluck('type','id');

        return view('admin.mortalities.edit', compact('mortality','galpon_list','mortality_types'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'chikens_quantity' => 'required',
            'galpon_id' => 'required',
            'mortality_type_id'  => 'required',
            'death_date'   => 'required'
		]);
        $requestData = $request->all();
        
        $mortality = Mortality::findOrFail($id);

        $parvada = Parvada::findOrFail($mortality->parvada_id);
        $this->increase_chikens_quantity_parvada($parvada,$mortality->chikens_quantity); 
        $this->decrease_chikens_quantity_parvada($parvada,$requestData["chikens_quantity"]);    
        $mortality->update($requestData);

        return redirect('admin/mortalities')->with('flash_message', 'Mortality updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $mortality = Mortality::findOrFail($id);
        $parvada = Parvada::findOrFail($mortality->parvada_id);
        $this->increase_chikens_quantity_parvada($parvada,$mortality->chikens_quantity);

        Mortality::destroy($id);

        return redirect('admin/mortalities')->with('flash_message', 'Mortality deleted!');
    }
}
