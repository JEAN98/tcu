<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use App\BrokenEggsCause;
use Illuminate\Http\Request;

class BrokenEggsCausesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 10;

        if (!empty($keyword)) {
            $brokeneggscauses = BrokenEggsCause::where('name', 'LIKE', "%$keyword%")
                ->orWhere('description', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $brokeneggscauses = BrokenEggsCause::latest()->paginate($perPage);
        }

        return view('admin.broken-eggs-causes.index', compact('brokeneggscauses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.broken-eggs-causes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'name' => 'required'
		]);
        $requestData = $request->all();
        
        BrokenEggsCause::create($requestData);

        return redirect('admin/broken-eggs-causes')->with('flash_message', 'BrokenEggsCause added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $brokeneggscause = BrokenEggsCause::findOrFail($id);

        return view('admin.broken-eggs-causes.show', compact('brokeneggscause'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $brokeneggscause = BrokenEggsCause::findOrFail($id);

        return view('admin.broken-eggs-causes.edit', compact('brokeneggscause'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'name' => 'required'
		]);
        $requestData = $request->all();
        
        $brokeneggscause = BrokenEggsCause::findOrFail($id);
        $brokeneggscause->update($requestData);

        return redirect('admin/broken-eggs-causes')->with('flash_message', 'BrokenEggsCause updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try{
            BrokenEggsCause::destroy($id);
            return redirect('admin/broken-eggs-causes')->with('flash_message', 'BrokenEggsCause deleted!');
        }
        catch(\Illuminate\Database\QueryException $exception )
        {
            if($exception->errorInfo[0] == 23000)
            {
                $error["message"] = "¡No puede eleminar la entrada selecionada,".
                                    " ya que tiene información ligada en otras tablas!"
                                    ." Para poder eliminar el elemento debe elminar primero la información que tiene ligada"; 
                
                $errors = $error;  
            }
            else{
                $error["message"] = "Se encontró un problema, por lo que no se puede eliminar la entrada selecionada";
                $errors = $error; 
            }
            return Redirect::back()->withErrors($errors);
        }
    }
}
