<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\TemperatureInventory;
use App\Galpon;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class TemperatureInventoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 10;

        if (!empty($keyword)) {
            $temperatureinventories = TemperatureInventory::where('galpon_id', 'LIKE', "%$keyword%")
                ->orWhere('date', 'LIKE', "%$keyword%")
                ->orWhere('hour', 'LIKE', "%$keyword%")
                ->orWhere('temperature', 'LIKE', "%$keyword%")
                ->orWhere('parvada_id', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $temperatureinventories = TemperatureInventory::latest()->paginate($perPage);
        }

        return view('admin.temperature-inventories.index', compact('temperatureinventories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $galpons = $this->get_galpon_list_active();
        return view('admin.temperature-inventories.create',compact('galpons'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'date' => 'required',
			'temperature' => 'required',
			'galpon_id' => 'required',
			'hour' => 'required'
		]);
        $requestData = $request->all();
        $requestData["parvada_id"] = $this->get_parvada_active_by_galpon_code( $requestData["galpon_id"])[0];
        TemperatureInventory::create($requestData);
      //  echo(var_dump($requestData));
        return redirect('admin/temperature-inventories')->with('flash_message', 'TemperatureInventory added!');
    }

    private function get_parvada_active_by_galpon_code($galpon_id)
    {
        return DB::table('parvadas')
                ->where('parvadas.galpon_id','=',$galpon_id)
                ->where('parvadas.parvada_status','=','1')
                ->pluck('parvadas.id')
                ->all();
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $temperatureinventory = TemperatureInventory::findOrFail($id);

        return view('admin.temperature-inventories.show', compact('temperatureinventory'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $temperatureinventory = TemperatureInventory::findOrFail($id);
        $galpons = $this->get_galpon_list_active();
        return view('admin.temperature-inventories.edit', compact('temperatureinventory','galpons'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'date' => 'required',
			'temperature' => 'required',
			'galpon_id' => 'required',
			'hour' => 'required'
		]);
        $requestData = $request->all();
        
        $temperatureinventory = TemperatureInventory::findOrFail($id);
        $temperatureinventory->update($requestData);

        return redirect('admin/temperature-inventories')->with('flash_message', 'TemperatureInventory updated!');
    }

    private function get_galpon_list_active()
    {
        //Only show the galpons active by parvadas
        return DB::table('galpons')
                    ->join('parvadas','parvadas.galpon_id','=','galpons.id')
                    ->where('parvadas.parvada_status','=','1')
                    ->pluck('galpons.galpon_code','galpons.id')
                    ->all();
    }
    
  

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        TemperatureInventory::destroy($id);

        return redirect('admin/temperature-inventories')->with('flash_message', 'TemperatureInventory deleted!');
    }
}
