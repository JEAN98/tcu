<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use App\Mortality_Type;
use Illuminate\Http\Request;

class Mortality_TypesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 10;

        if (!empty($keyword)) {
            $mortality_types = Mortality_Type::where('type', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $mortality_types = Mortality_Type::latest()->paginate($perPage);
        }

        return view('admin.mortality_-types.index', compact('mortality_types'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.mortality_-types.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        echo(var_dump($request->all()));
        $this->validate($request, [
			'type' => 'required'
		]);
        $requestData = $request->all();
        
        Mortality_Type::create($requestData);

       return redirect('admin/mortality_types')->with('flash_message', 'Mortality_Type added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $mortality_type = Mortality_Type::findOrFail($id);

        return view('admin.mortality_-types.show', compact('mortality_type'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $mortality_type = Mortality_Type::findOrFail($id);

        return view('admin.mortality_-types.edit', compact('mortality_type'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'type' => 'required'
		]);
        $requestData = $request->all();
        
        $mortality_type = Mortality_Type::findOrFail($id);
        $mortality_type->update($requestData);

        return redirect('admin/mortality_types')->with('flash_message', 'Mortality_Type updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try{
            Mortality_Type::destroy($id);
            return redirect('admin/mortality_types')->with('flash_message', 'Mortality_Type deleted!');
        }
        catch(\Illuminate\Database\QueryException $exception )
        {
            if($exception->errorInfo[0] == 23000)
            {
                $error["message"] = "¡No puede eleminar la entrada selecionada,".
                                    " ya que tiene información ligada en otras tablas!"
                                    ." Para poder eliminar el elemento debe elminar primero la información que tiene ligada"; 
                
                $errors = $error;  
            }
            else{
                $error["message"] = "Se encontró un problema, por lo que no se puede eliminar la entrada selecionada";
                $errors = $error; 
            }
          return Redirect::back()->withErrors($errors);
        }
    }
}
