/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import controllers.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import entities.Sales;
import entities.SalesBySupermarket;
import entities.Supermarket;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Jean Carlo
 */
public class SalesBySupermarketJpaController implements Serializable {

    public SalesBySupermarketJpaController() {
        this.emf = Persistence.createEntityManagerFactory("AviPU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(SalesBySupermarket salesBySupermarket) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Sales salesid = salesBySupermarket.getSalesid();
            if (salesid != null) {
                salesid = em.getReference(salesid.getClass(), salesid.getSalesid());
                salesBySupermarket.setSalesid(salesid);
            }
            Supermarket supermarketid = salesBySupermarket.getSupermarketid();
            if (supermarketid != null) {
                supermarketid = em.getReference(supermarketid.getClass(), supermarketid.getSupermarketid());
                salesBySupermarket.setSupermarketid(supermarketid);
            }
            em.persist(salesBySupermarket);
            if (salesid != null) {
                salesid.getSalesBySupermarketList().add(salesBySupermarket);
                salesid = em.merge(salesid);
            }
            if (supermarketid != null) {
                supermarketid.getSalesBySupermarketList().add(salesBySupermarket);
                supermarketid = em.merge(supermarketid);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(SalesBySupermarket salesBySupermarket) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            SalesBySupermarket persistentSalesBySupermarket = em.find(SalesBySupermarket.class, salesBySupermarket.getSalesbysupermarketid());
            Sales salesidOld = persistentSalesBySupermarket.getSalesid();
            Sales salesidNew = salesBySupermarket.getSalesid();
            Supermarket supermarketidOld = persistentSalesBySupermarket.getSupermarketid();
            Supermarket supermarketidNew = salesBySupermarket.getSupermarketid();
            if (salesidNew != null) {
                salesidNew = em.getReference(salesidNew.getClass(), salesidNew.getSalesid());
                salesBySupermarket.setSalesid(salesidNew);
            }
            if (supermarketidNew != null) {
                supermarketidNew = em.getReference(supermarketidNew.getClass(), supermarketidNew.getSupermarketid());
                salesBySupermarket.setSupermarketid(supermarketidNew);
            }
            salesBySupermarket = em.merge(salesBySupermarket);
            if (salesidOld != null && !salesidOld.equals(salesidNew)) {
                salesidOld.getSalesBySupermarketList().remove(salesBySupermarket);
                salesidOld = em.merge(salesidOld);
            }
            if (salesidNew != null && !salesidNew.equals(salesidOld)) {
                salesidNew.getSalesBySupermarketList().add(salesBySupermarket);
                salesidNew = em.merge(salesidNew);
            }
            if (supermarketidOld != null && !supermarketidOld.equals(supermarketidNew)) {
                supermarketidOld.getSalesBySupermarketList().remove(salesBySupermarket);
                supermarketidOld = em.merge(supermarketidOld);
            }
            if (supermarketidNew != null && !supermarketidNew.equals(supermarketidOld)) {
                supermarketidNew.getSalesBySupermarketList().add(salesBySupermarket);
                supermarketidNew = em.merge(supermarketidNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = salesBySupermarket.getSalesbysupermarketid();
                if (findSalesBySupermarket(id) == null) {
                    throw new NonexistentEntityException("The salesBySupermarket with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            SalesBySupermarket salesBySupermarket;
            try {
                salesBySupermarket = em.getReference(SalesBySupermarket.class, id);
                salesBySupermarket.getSalesbysupermarketid();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The salesBySupermarket with id " + id + " no longer exists.", enfe);
            }
            Sales salesid = salesBySupermarket.getSalesid();
            if (salesid != null) {
                salesid.getSalesBySupermarketList().remove(salesBySupermarket);
                salesid = em.merge(salesid);
            }
            Supermarket supermarketid = salesBySupermarket.getSupermarketid();
            if (supermarketid != null) {
                supermarketid.getSalesBySupermarketList().remove(salesBySupermarket);
                supermarketid = em.merge(supermarketid);
            }
            em.remove(salesBySupermarket);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<SalesBySupermarket> findSalesBySupermarketEntities() {
        return findSalesBySupermarketEntities(true, -1, -1);
    }

    public List<SalesBySupermarket> findSalesBySupermarketEntities(int maxResults, int firstResult) {
        return findSalesBySupermarketEntities(false, maxResults, firstResult);
    }

    private List<SalesBySupermarket> findSalesBySupermarketEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(SalesBySupermarket.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public SalesBySupermarket findSalesBySupermarket(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(SalesBySupermarket.class, id);
        } finally {
            em.close();
        }
    }

    public int getSalesBySupermarketCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<SalesBySupermarket> rt = cq.from(SalesBySupermarket.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
