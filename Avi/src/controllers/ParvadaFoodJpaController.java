/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import controllers.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import entities.Parvada;
import entities.ParvadaFood;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Jean Carlo
 */
public class ParvadaFoodJpaController implements Serializable {

    public ParvadaFoodJpaController() {
        this.emf = Persistence.createEntityManagerFactory("AviPU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(ParvadaFood parvadaFood) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Parvada parvadaid = parvadaFood.getParvadaid();
            if (parvadaid != null) {
                parvadaid = em.getReference(parvadaid.getClass(), parvadaid.getParvadaid());
                parvadaFood.setParvadaid(parvadaid);
            }
            em.persist(parvadaFood);
            if (parvadaid != null) {
                parvadaid.getParvadaFoodList().add(parvadaFood);
                parvadaid = em.merge(parvadaid);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(ParvadaFood parvadaFood) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            ParvadaFood persistentParvadaFood = em.find(ParvadaFood.class, parvadaFood.getParvadasfoodid());
            Parvada parvadaidOld = persistentParvadaFood.getParvadaid();
            Parvada parvadaidNew = parvadaFood.getParvadaid();
            if (parvadaidNew != null) {
                parvadaidNew = em.getReference(parvadaidNew.getClass(), parvadaidNew.getParvadaid());
                parvadaFood.setParvadaid(parvadaidNew);
            }
            parvadaFood = em.merge(parvadaFood);
            if (parvadaidOld != null && !parvadaidOld.equals(parvadaidNew)) {
                parvadaidOld.getParvadaFoodList().remove(parvadaFood);
                parvadaidOld = em.merge(parvadaidOld);
            }
            if (parvadaidNew != null && !parvadaidNew.equals(parvadaidOld)) {
                parvadaidNew.getParvadaFoodList().add(parvadaFood);
                parvadaidNew = em.merge(parvadaidNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = parvadaFood.getParvadasfoodid();
                if (findParvadaFood(id) == null) {
                    throw new NonexistentEntityException("The parvadaFood with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            ParvadaFood parvadaFood;
            try {
                parvadaFood = em.getReference(ParvadaFood.class, id);
                parvadaFood.getParvadasfoodid();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The parvadaFood with id " + id + " no longer exists.", enfe);
            }
            Parvada parvadaid = parvadaFood.getParvadaid();
            if (parvadaid != null) {
                parvadaid.getParvadaFoodList().remove(parvadaFood);
                parvadaid = em.merge(parvadaid);
            }
            em.remove(parvadaFood);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<ParvadaFood> findParvadaFoodEntities() {
        return findParvadaFoodEntities(true, -1, -1);
    }

    public List<ParvadaFood> findParvadaFoodEntities(int maxResults, int firstResult) {
        return findParvadaFoodEntities(false, maxResults, firstResult);
    }

    private List<ParvadaFood> findParvadaFoodEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(ParvadaFood.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public ParvadaFood findParvadaFood(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(ParvadaFood.class, id);
        } finally {
            em.close();
        }
    }

    public int getParvadaFoodCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<ParvadaFood> rt = cq.from(ParvadaFood.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
