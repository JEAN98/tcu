/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import controllers.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import entities.Parvada;
import entities.Temperature;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Jean Carlo
 */
public class TemperatureJpaController implements Serializable {

    public TemperatureJpaController() {
        this.emf = Persistence.createEntityManagerFactory("AviPU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Temperature temperature) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Parvada parvadaid = temperature.getParvadaid();
            if (parvadaid != null) {
                parvadaid = em.getReference(parvadaid.getClass(), parvadaid.getParvadaid());
                temperature.setParvadaid(parvadaid);
            }
            em.persist(temperature);
            if (parvadaid != null) {
                parvadaid.getTemperatureList().add(temperature);
                parvadaid = em.merge(parvadaid);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Temperature temperature) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Temperature persistentTemperature = em.find(Temperature.class, temperature.getTemperatureid());
            Parvada parvadaidOld = persistentTemperature.getParvadaid();
            Parvada parvadaidNew = temperature.getParvadaid();
            if (parvadaidNew != null) {
                parvadaidNew = em.getReference(parvadaidNew.getClass(), parvadaidNew.getParvadaid());
                temperature.setParvadaid(parvadaidNew);
            }
            temperature = em.merge(temperature);
            if (parvadaidOld != null && !parvadaidOld.equals(parvadaidNew)) {
                parvadaidOld.getTemperatureList().remove(temperature);
                parvadaidOld = em.merge(parvadaidOld);
            }
            if (parvadaidNew != null && !parvadaidNew.equals(parvadaidOld)) {
                parvadaidNew.getTemperatureList().add(temperature);
                parvadaidNew = em.merge(parvadaidNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = temperature.getTemperatureid();
                if (findTemperature(id) == null) {
                    throw new NonexistentEntityException("The temperature with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Temperature temperature;
            try {
                temperature = em.getReference(Temperature.class, id);
                temperature.getTemperatureid();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The temperature with id " + id + " no longer exists.", enfe);
            }
            Parvada parvadaid = temperature.getParvadaid();
            if (parvadaid != null) {
                parvadaid.getTemperatureList().remove(temperature);
                parvadaid = em.merge(parvadaid);
            }
            em.remove(temperature);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Temperature> findTemperatureEntities() {
        return findTemperatureEntities(true, -1, -1);
    }

    public List<Temperature> findTemperatureEntities(int maxResults, int firstResult) {
        return findTemperatureEntities(false, maxResults, firstResult);
    }

    private List<Temperature> findTemperatureEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Temperature.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Temperature findTemperature(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Temperature.class, id);
        } finally {
            em.close();
        }
    }

    public int getTemperatureCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Temperature> rt = cq.from(Temperature.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
