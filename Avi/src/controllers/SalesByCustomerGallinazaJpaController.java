/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import controllers.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import entities.Customer;
import entities.SalesByCustomerGallinaza;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Jean Carlo
 */
public class SalesByCustomerGallinazaJpaController implements Serializable {

    public SalesByCustomerGallinazaJpaController() {
        this.emf = Persistence.createEntityManagerFactory("AviPU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(SalesByCustomerGallinaza salesByCustomerGallinaza) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Customer clientid = salesByCustomerGallinaza.getClientid();
            if (clientid != null) {
                clientid = em.getReference(clientid.getClass(), clientid.getClientid());
                salesByCustomerGallinaza.setClientid(clientid);
            }
            em.persist(salesByCustomerGallinaza);
            if (clientid != null) {
                clientid.getSalesByCustomerGallinazaList().add(salesByCustomerGallinaza);
                clientid = em.merge(clientid);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(SalesByCustomerGallinaza salesByCustomerGallinaza) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            SalesByCustomerGallinaza persistentSalesByCustomerGallinaza = em.find(SalesByCustomerGallinaza.class, salesByCustomerGallinaza.getSalesbyclientgallinazaid());
            Customer clientidOld = persistentSalesByCustomerGallinaza.getClientid();
            Customer clientidNew = salesByCustomerGallinaza.getClientid();
            if (clientidNew != null) {
                clientidNew = em.getReference(clientidNew.getClass(), clientidNew.getClientid());
                salesByCustomerGallinaza.setClientid(clientidNew);
            }
            salesByCustomerGallinaza = em.merge(salesByCustomerGallinaza);
            if (clientidOld != null && !clientidOld.equals(clientidNew)) {
                clientidOld.getSalesByCustomerGallinazaList().remove(salesByCustomerGallinaza);
                clientidOld = em.merge(clientidOld);
            }
            if (clientidNew != null && !clientidNew.equals(clientidOld)) {
                clientidNew.getSalesByCustomerGallinazaList().add(salesByCustomerGallinaza);
                clientidNew = em.merge(clientidNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = salesByCustomerGallinaza.getSalesbyclientgallinazaid();
                if (findSalesByCustomerGallinaza(id) == null) {
                    throw new NonexistentEntityException("The salesByCustomerGallinaza with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            SalesByCustomerGallinaza salesByCustomerGallinaza;
            try {
                salesByCustomerGallinaza = em.getReference(SalesByCustomerGallinaza.class, id);
                salesByCustomerGallinaza.getSalesbyclientgallinazaid();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The salesByCustomerGallinaza with id " + id + " no longer exists.", enfe);
            }
            Customer clientid = salesByCustomerGallinaza.getClientid();
            if (clientid != null) {
                clientid.getSalesByCustomerGallinazaList().remove(salesByCustomerGallinaza);
                clientid = em.merge(clientid);
            }
            em.remove(salesByCustomerGallinaza);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<SalesByCustomerGallinaza> findSalesByCustomerGallinazaEntities() {
        return findSalesByCustomerGallinazaEntities(true, -1, -1);
    }

    public List<SalesByCustomerGallinaza> findSalesByCustomerGallinazaEntities(int maxResults, int firstResult) {
        return findSalesByCustomerGallinazaEntities(false, maxResults, firstResult);
    }

    private List<SalesByCustomerGallinaza> findSalesByCustomerGallinazaEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(SalesByCustomerGallinaza.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public SalesByCustomerGallinaza findSalesByCustomerGallinaza(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(SalesByCustomerGallinaza.class, id);
        } finally {
            em.close();
        }
    }

    public int getSalesByCustomerGallinazaCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<SalesByCustomerGallinaza> rt = cq.from(SalesByCustomerGallinaza.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
