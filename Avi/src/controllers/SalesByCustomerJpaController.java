/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import controllers.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import entities.Customer;
import entities.Sales;
import entities.SalesByCustomer;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Jean Carlo
 */
public class SalesByCustomerJpaController implements Serializable {

    public SalesByCustomerJpaController() {
        this.emf = Persistence.createEntityManagerFactory("AviPU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(SalesByCustomer salesByCustomer) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Customer clientid = salesByCustomer.getClientid();
            if (clientid != null) {
                clientid = em.getReference(clientid.getClass(), clientid.getClientid());
                salesByCustomer.setClientid(clientid);
            }
            Sales salesid = salesByCustomer.getSalesid();
            if (salesid != null) {
                salesid = em.getReference(salesid.getClass(), salesid.getSalesid());
                salesByCustomer.setSalesid(salesid);
            }
            em.persist(salesByCustomer);
            if (clientid != null) {
                clientid.getSalesByCustomerList().add(salesByCustomer);
                clientid = em.merge(clientid);
            }
            if (salesid != null) {
                salesid.getSalesByCustomerList().add(salesByCustomer);
                salesid = em.merge(salesid);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(SalesByCustomer salesByCustomer) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            SalesByCustomer persistentSalesByCustomer = em.find(SalesByCustomer.class, salesByCustomer.getSalesbyclientid());
            Customer clientidOld = persistentSalesByCustomer.getClientid();
            Customer clientidNew = salesByCustomer.getClientid();
            Sales salesidOld = persistentSalesByCustomer.getSalesid();
            Sales salesidNew = salesByCustomer.getSalesid();
            if (clientidNew != null) {
                clientidNew = em.getReference(clientidNew.getClass(), clientidNew.getClientid());
                salesByCustomer.setClientid(clientidNew);
            }
            if (salesidNew != null) {
                salesidNew = em.getReference(salesidNew.getClass(), salesidNew.getSalesid());
                salesByCustomer.setSalesid(salesidNew);
            }
            salesByCustomer = em.merge(salesByCustomer);
            if (clientidOld != null && !clientidOld.equals(clientidNew)) {
                clientidOld.getSalesByCustomerList().remove(salesByCustomer);
                clientidOld = em.merge(clientidOld);
            }
            if (clientidNew != null && !clientidNew.equals(clientidOld)) {
                clientidNew.getSalesByCustomerList().add(salesByCustomer);
                clientidNew = em.merge(clientidNew);
            }
            if (salesidOld != null && !salesidOld.equals(salesidNew)) {
                salesidOld.getSalesByCustomerList().remove(salesByCustomer);
                salesidOld = em.merge(salesidOld);
            }
            if (salesidNew != null && !salesidNew.equals(salesidOld)) {
                salesidNew.getSalesByCustomerList().add(salesByCustomer);
                salesidNew = em.merge(salesidNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = salesByCustomer.getSalesbyclientid();
                if (findSalesByCustomer(id) == null) {
                    throw new NonexistentEntityException("The salesByCustomer with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            SalesByCustomer salesByCustomer;
            try {
                salesByCustomer = em.getReference(SalesByCustomer.class, id);
                salesByCustomer.getSalesbyclientid();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The salesByCustomer with id " + id + " no longer exists.", enfe);
            }
            Customer clientid = salesByCustomer.getClientid();
            if (clientid != null) {
                clientid.getSalesByCustomerList().remove(salesByCustomer);
                clientid = em.merge(clientid);
            }
            Sales salesid = salesByCustomer.getSalesid();
            if (salesid != null) {
                salesid.getSalesByCustomerList().remove(salesByCustomer);
                salesid = em.merge(salesid);
            }
            em.remove(salesByCustomer);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<SalesByCustomer> findSalesByCustomerEntities() {
        return findSalesByCustomerEntities(true, -1, -1);
    }

    public List<SalesByCustomer> findSalesByCustomerEntities(int maxResults, int firstResult) {
        return findSalesByCustomerEntities(false, maxResults, firstResult);
    }

    private List<SalesByCustomer> findSalesByCustomerEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(SalesByCustomer.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public SalesByCustomer findSalesByCustomer(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(SalesByCustomer.class, id);
        } finally {
            em.close();
        }
    }

    public int getSalesByCustomerCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<SalesByCustomer> rt = cq.from(SalesByCustomer.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
