/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import controllers.exceptions.IllegalOrphanException;
import controllers.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import entities.Galpon;
import entities.Mortality;
import java.util.ArrayList;
import java.util.List;
import entities.ParvadaFood;
import entities.ProductionRate;
import entities.Temperature;
import entities.MedicineApplication;
import entities.Parvada;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Jean Carlo
 */
public class ParvadaJpaController implements Serializable {

    public ParvadaJpaController() {
        this.emf = Persistence.createEntityManagerFactory("AviPU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Parvada parvada) {
        if (parvada.getMortalityList() == null) {
            parvada.setMortalityList(new ArrayList<Mortality>());
        }
        if (parvada.getParvadaFoodList() == null) {
            parvada.setParvadaFoodList(new ArrayList<ParvadaFood>());
        }
        if (parvada.getProductionRateList() == null) {
            parvada.setProductionRateList(new ArrayList<ProductionRate>());
        }
        if (parvada.getTemperatureList() == null) {
            parvada.setTemperatureList(new ArrayList<Temperature>());
        }
        if (parvada.getMedicineApplicationList() == null) {
            parvada.setMedicineApplicationList(new ArrayList<MedicineApplication>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Galpon galponid = parvada.getGalponid();
            if (galponid != null) {
                galponid = em.getReference(galponid.getClass(), galponid.getGalponid());
                parvada.setGalponid(galponid);
            }
            List<Mortality> attachedMortalityList = new ArrayList<Mortality>();
            for (Mortality mortalityListMortalityToAttach : parvada.getMortalityList()) {
                mortalityListMortalityToAttach = em.getReference(mortalityListMortalityToAttach.getClass(), mortalityListMortalityToAttach.getMortalityid());
                attachedMortalityList.add(mortalityListMortalityToAttach);
            }
            parvada.setMortalityList(attachedMortalityList);
            List<ParvadaFood> attachedParvadaFoodList = new ArrayList<ParvadaFood>();
            for (ParvadaFood parvadaFoodListParvadaFoodToAttach : parvada.getParvadaFoodList()) {
                parvadaFoodListParvadaFoodToAttach = em.getReference(parvadaFoodListParvadaFoodToAttach.getClass(), parvadaFoodListParvadaFoodToAttach.getParvadasfoodid());
                attachedParvadaFoodList.add(parvadaFoodListParvadaFoodToAttach);
            }
            parvada.setParvadaFoodList(attachedParvadaFoodList);
            List<ProductionRate> attachedProductionRateList = new ArrayList<ProductionRate>();
            for (ProductionRate productionRateListProductionRateToAttach : parvada.getProductionRateList()) {
                productionRateListProductionRateToAttach = em.getReference(productionRateListProductionRateToAttach.getClass(), productionRateListProductionRateToAttach.getProductionrateid());
                attachedProductionRateList.add(productionRateListProductionRateToAttach);
            }
            parvada.setProductionRateList(attachedProductionRateList);
            List<Temperature> attachedTemperatureList = new ArrayList<Temperature>();
            for (Temperature temperatureListTemperatureToAttach : parvada.getTemperatureList()) {
                temperatureListTemperatureToAttach = em.getReference(temperatureListTemperatureToAttach.getClass(), temperatureListTemperatureToAttach.getTemperatureid());
                attachedTemperatureList.add(temperatureListTemperatureToAttach);
            }
            parvada.setTemperatureList(attachedTemperatureList);
            List<MedicineApplication> attachedMedicineApplicationList = new ArrayList<MedicineApplication>();
            for (MedicineApplication medicineApplicationListMedicineApplicationToAttach : parvada.getMedicineApplicationList()) {
                medicineApplicationListMedicineApplicationToAttach = em.getReference(medicineApplicationListMedicineApplicationToAttach.getClass(), medicineApplicationListMedicineApplicationToAttach.getMedicineapplicationid());
                attachedMedicineApplicationList.add(medicineApplicationListMedicineApplicationToAttach);
            }
            parvada.setMedicineApplicationList(attachedMedicineApplicationList);
            em.persist(parvada);
            if (galponid != null) {
                galponid.getParvadaList().add(parvada);
                galponid = em.merge(galponid);
            }
            for (Mortality mortalityListMortality : parvada.getMortalityList()) {
                Parvada oldParvadaidOfMortalityListMortality = mortalityListMortality.getParvadaid();
                mortalityListMortality.setParvadaid(parvada);
                mortalityListMortality = em.merge(mortalityListMortality);
                if (oldParvadaidOfMortalityListMortality != null) {
                    oldParvadaidOfMortalityListMortality.getMortalityList().remove(mortalityListMortality);
                    oldParvadaidOfMortalityListMortality = em.merge(oldParvadaidOfMortalityListMortality);
                }
            }
            for (ParvadaFood parvadaFoodListParvadaFood : parvada.getParvadaFoodList()) {
                Parvada oldParvadaidOfParvadaFoodListParvadaFood = parvadaFoodListParvadaFood.getParvadaid();
                parvadaFoodListParvadaFood.setParvadaid(parvada);
                parvadaFoodListParvadaFood = em.merge(parvadaFoodListParvadaFood);
                if (oldParvadaidOfParvadaFoodListParvadaFood != null) {
                    oldParvadaidOfParvadaFoodListParvadaFood.getParvadaFoodList().remove(parvadaFoodListParvadaFood);
                    oldParvadaidOfParvadaFoodListParvadaFood = em.merge(oldParvadaidOfParvadaFoodListParvadaFood);
                }
            }
            for (ProductionRate productionRateListProductionRate : parvada.getProductionRateList()) {
                Parvada oldParvadaidOfProductionRateListProductionRate = productionRateListProductionRate.getParvadaid();
                productionRateListProductionRate.setParvadaid(parvada);
                productionRateListProductionRate = em.merge(productionRateListProductionRate);
                if (oldParvadaidOfProductionRateListProductionRate != null) {
                    oldParvadaidOfProductionRateListProductionRate.getProductionRateList().remove(productionRateListProductionRate);
                    oldParvadaidOfProductionRateListProductionRate = em.merge(oldParvadaidOfProductionRateListProductionRate);
                }
            }
            for (Temperature temperatureListTemperature : parvada.getTemperatureList()) {
                Parvada oldParvadaidOfTemperatureListTemperature = temperatureListTemperature.getParvadaid();
                temperatureListTemperature.setParvadaid(parvada);
                temperatureListTemperature = em.merge(temperatureListTemperature);
                if (oldParvadaidOfTemperatureListTemperature != null) {
                    oldParvadaidOfTemperatureListTemperature.getTemperatureList().remove(temperatureListTemperature);
                    oldParvadaidOfTemperatureListTemperature = em.merge(oldParvadaidOfTemperatureListTemperature);
                }
            }
            for (MedicineApplication medicineApplicationListMedicineApplication : parvada.getMedicineApplicationList()) {
                Parvada oldParvadaidOfMedicineApplicationListMedicineApplication = medicineApplicationListMedicineApplication.getParvadaid();
                medicineApplicationListMedicineApplication.setParvadaid(parvada);
                medicineApplicationListMedicineApplication = em.merge(medicineApplicationListMedicineApplication);
                if (oldParvadaidOfMedicineApplicationListMedicineApplication != null) {
                    oldParvadaidOfMedicineApplicationListMedicineApplication.getMedicineApplicationList().remove(medicineApplicationListMedicineApplication);
                    oldParvadaidOfMedicineApplicationListMedicineApplication = em.merge(oldParvadaidOfMedicineApplicationListMedicineApplication);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Parvada parvada) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Parvada persistentParvada = em.find(Parvada.class, parvada.getParvadaid());
            Galpon galponidOld = persistentParvada.getGalponid();
            Galpon galponidNew = parvada.getGalponid();
            List<Mortality> mortalityListOld = persistentParvada.getMortalityList();
            List<Mortality> mortalityListNew = parvada.getMortalityList();
            List<ParvadaFood> parvadaFoodListOld = persistentParvada.getParvadaFoodList();
            List<ParvadaFood> parvadaFoodListNew = parvada.getParvadaFoodList();
            List<ProductionRate> productionRateListOld = persistentParvada.getProductionRateList();
            List<ProductionRate> productionRateListNew = parvada.getProductionRateList();
            List<Temperature> temperatureListOld = persistentParvada.getTemperatureList();
            List<Temperature> temperatureListNew = parvada.getTemperatureList();
            List<MedicineApplication> medicineApplicationListOld = persistentParvada.getMedicineApplicationList();
            List<MedicineApplication> medicineApplicationListNew = parvada.getMedicineApplicationList();
            List<String> illegalOrphanMessages = null;
            for (ParvadaFood parvadaFoodListOldParvadaFood : parvadaFoodListOld) {
                if (!parvadaFoodListNew.contains(parvadaFoodListOldParvadaFood)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain ParvadaFood " + parvadaFoodListOldParvadaFood + " since its parvadaid field is not nullable.");
                }
            }
            for (ProductionRate productionRateListOldProductionRate : productionRateListOld) {
                if (!productionRateListNew.contains(productionRateListOldProductionRate)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain ProductionRate " + productionRateListOldProductionRate + " since its parvadaid field is not nullable.");
                }
            }
            for (Temperature temperatureListOldTemperature : temperatureListOld) {
                if (!temperatureListNew.contains(temperatureListOldTemperature)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Temperature " + temperatureListOldTemperature + " since its parvadaid field is not nullable.");
                }
            }
            for (MedicineApplication medicineApplicationListOldMedicineApplication : medicineApplicationListOld) {
                if (!medicineApplicationListNew.contains(medicineApplicationListOldMedicineApplication)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain MedicineApplication " + medicineApplicationListOldMedicineApplication + " since its parvadaid field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (galponidNew != null) {
                galponidNew = em.getReference(galponidNew.getClass(), galponidNew.getGalponid());
                parvada.setGalponid(galponidNew);
            }
            List<Mortality> attachedMortalityListNew = new ArrayList<Mortality>();
            for (Mortality mortalityListNewMortalityToAttach : mortalityListNew) {
                mortalityListNewMortalityToAttach = em.getReference(mortalityListNewMortalityToAttach.getClass(), mortalityListNewMortalityToAttach.getMortalityid());
                attachedMortalityListNew.add(mortalityListNewMortalityToAttach);
            }
            mortalityListNew = attachedMortalityListNew;
            parvada.setMortalityList(mortalityListNew);
            List<ParvadaFood> attachedParvadaFoodListNew = new ArrayList<ParvadaFood>();
            for (ParvadaFood parvadaFoodListNewParvadaFoodToAttach : parvadaFoodListNew) {
                parvadaFoodListNewParvadaFoodToAttach = em.getReference(parvadaFoodListNewParvadaFoodToAttach.getClass(), parvadaFoodListNewParvadaFoodToAttach.getParvadasfoodid());
                attachedParvadaFoodListNew.add(parvadaFoodListNewParvadaFoodToAttach);
            }
            parvadaFoodListNew = attachedParvadaFoodListNew;
            parvada.setParvadaFoodList(parvadaFoodListNew);
            List<ProductionRate> attachedProductionRateListNew = new ArrayList<ProductionRate>();
            for (ProductionRate productionRateListNewProductionRateToAttach : productionRateListNew) {
                productionRateListNewProductionRateToAttach = em.getReference(productionRateListNewProductionRateToAttach.getClass(), productionRateListNewProductionRateToAttach.getProductionrateid());
                attachedProductionRateListNew.add(productionRateListNewProductionRateToAttach);
            }
            productionRateListNew = attachedProductionRateListNew;
            parvada.setProductionRateList(productionRateListNew);
            List<Temperature> attachedTemperatureListNew = new ArrayList<Temperature>();
            for (Temperature temperatureListNewTemperatureToAttach : temperatureListNew) {
                temperatureListNewTemperatureToAttach = em.getReference(temperatureListNewTemperatureToAttach.getClass(), temperatureListNewTemperatureToAttach.getTemperatureid());
                attachedTemperatureListNew.add(temperatureListNewTemperatureToAttach);
            }
            temperatureListNew = attachedTemperatureListNew;
            parvada.setTemperatureList(temperatureListNew);
            List<MedicineApplication> attachedMedicineApplicationListNew = new ArrayList<MedicineApplication>();
            for (MedicineApplication medicineApplicationListNewMedicineApplicationToAttach : medicineApplicationListNew) {
                medicineApplicationListNewMedicineApplicationToAttach = em.getReference(medicineApplicationListNewMedicineApplicationToAttach.getClass(), medicineApplicationListNewMedicineApplicationToAttach.getMedicineapplicationid());
                attachedMedicineApplicationListNew.add(medicineApplicationListNewMedicineApplicationToAttach);
            }
            medicineApplicationListNew = attachedMedicineApplicationListNew;
            parvada.setMedicineApplicationList(medicineApplicationListNew);
            parvada = em.merge(parvada);
            if (galponidOld != null && !galponidOld.equals(galponidNew)) {
                galponidOld.getParvadaList().remove(parvada);
                galponidOld = em.merge(galponidOld);
            }
            if (galponidNew != null && !galponidNew.equals(galponidOld)) {
                galponidNew.getParvadaList().add(parvada);
                galponidNew = em.merge(galponidNew);
            }
            for (Mortality mortalityListOldMortality : mortalityListOld) {
                if (!mortalityListNew.contains(mortalityListOldMortality)) {
                    mortalityListOldMortality.setParvadaid(null);
                    mortalityListOldMortality = em.merge(mortalityListOldMortality);
                }
            }
            for (Mortality mortalityListNewMortality : mortalityListNew) {
                if (!mortalityListOld.contains(mortalityListNewMortality)) {
                    Parvada oldParvadaidOfMortalityListNewMortality = mortalityListNewMortality.getParvadaid();
                    mortalityListNewMortality.setParvadaid(parvada);
                    mortalityListNewMortality = em.merge(mortalityListNewMortality);
                    if (oldParvadaidOfMortalityListNewMortality != null && !oldParvadaidOfMortalityListNewMortality.equals(parvada)) {
                        oldParvadaidOfMortalityListNewMortality.getMortalityList().remove(mortalityListNewMortality);
                        oldParvadaidOfMortalityListNewMortality = em.merge(oldParvadaidOfMortalityListNewMortality);
                    }
                }
            }
            for (ParvadaFood parvadaFoodListNewParvadaFood : parvadaFoodListNew) {
                if (!parvadaFoodListOld.contains(parvadaFoodListNewParvadaFood)) {
                    Parvada oldParvadaidOfParvadaFoodListNewParvadaFood = parvadaFoodListNewParvadaFood.getParvadaid();
                    parvadaFoodListNewParvadaFood.setParvadaid(parvada);
                    parvadaFoodListNewParvadaFood = em.merge(parvadaFoodListNewParvadaFood);
                    if (oldParvadaidOfParvadaFoodListNewParvadaFood != null && !oldParvadaidOfParvadaFoodListNewParvadaFood.equals(parvada)) {
                        oldParvadaidOfParvadaFoodListNewParvadaFood.getParvadaFoodList().remove(parvadaFoodListNewParvadaFood);
                        oldParvadaidOfParvadaFoodListNewParvadaFood = em.merge(oldParvadaidOfParvadaFoodListNewParvadaFood);
                    }
                }
            }
            for (ProductionRate productionRateListNewProductionRate : productionRateListNew) {
                if (!productionRateListOld.contains(productionRateListNewProductionRate)) {
                    Parvada oldParvadaidOfProductionRateListNewProductionRate = productionRateListNewProductionRate.getParvadaid();
                    productionRateListNewProductionRate.setParvadaid(parvada);
                    productionRateListNewProductionRate = em.merge(productionRateListNewProductionRate);
                    if (oldParvadaidOfProductionRateListNewProductionRate != null && !oldParvadaidOfProductionRateListNewProductionRate.equals(parvada)) {
                        oldParvadaidOfProductionRateListNewProductionRate.getProductionRateList().remove(productionRateListNewProductionRate);
                        oldParvadaidOfProductionRateListNewProductionRate = em.merge(oldParvadaidOfProductionRateListNewProductionRate);
                    }
                }
            }
            for (Temperature temperatureListNewTemperature : temperatureListNew) {
                if (!temperatureListOld.contains(temperatureListNewTemperature)) {
                    Parvada oldParvadaidOfTemperatureListNewTemperature = temperatureListNewTemperature.getParvadaid();
                    temperatureListNewTemperature.setParvadaid(parvada);
                    temperatureListNewTemperature = em.merge(temperatureListNewTemperature);
                    if (oldParvadaidOfTemperatureListNewTemperature != null && !oldParvadaidOfTemperatureListNewTemperature.equals(parvada)) {
                        oldParvadaidOfTemperatureListNewTemperature.getTemperatureList().remove(temperatureListNewTemperature);
                        oldParvadaidOfTemperatureListNewTemperature = em.merge(oldParvadaidOfTemperatureListNewTemperature);
                    }
                }
            }
            for (MedicineApplication medicineApplicationListNewMedicineApplication : medicineApplicationListNew) {
                if (!medicineApplicationListOld.contains(medicineApplicationListNewMedicineApplication)) {
                    Parvada oldParvadaidOfMedicineApplicationListNewMedicineApplication = medicineApplicationListNewMedicineApplication.getParvadaid();
                    medicineApplicationListNewMedicineApplication.setParvadaid(parvada);
                    medicineApplicationListNewMedicineApplication = em.merge(medicineApplicationListNewMedicineApplication);
                    if (oldParvadaidOfMedicineApplicationListNewMedicineApplication != null && !oldParvadaidOfMedicineApplicationListNewMedicineApplication.equals(parvada)) {
                        oldParvadaidOfMedicineApplicationListNewMedicineApplication.getMedicineApplicationList().remove(medicineApplicationListNewMedicineApplication);
                        oldParvadaidOfMedicineApplicationListNewMedicineApplication = em.merge(oldParvadaidOfMedicineApplicationListNewMedicineApplication);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = parvada.getParvadaid();
                if (findParvada(id) == null) {
                    throw new NonexistentEntityException("The parvada with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Parvada parvada;
            try {
                parvada = em.getReference(Parvada.class, id);
                parvada.getParvadaid();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The parvada with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<ParvadaFood> parvadaFoodListOrphanCheck = parvada.getParvadaFoodList();
            for (ParvadaFood parvadaFoodListOrphanCheckParvadaFood : parvadaFoodListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Parvada (" + parvada + ") cannot be destroyed since the ParvadaFood " + parvadaFoodListOrphanCheckParvadaFood + " in its parvadaFoodList field has a non-nullable parvadaid field.");
            }
            List<ProductionRate> productionRateListOrphanCheck = parvada.getProductionRateList();
            for (ProductionRate productionRateListOrphanCheckProductionRate : productionRateListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Parvada (" + parvada + ") cannot be destroyed since the ProductionRate " + productionRateListOrphanCheckProductionRate + " in its productionRateList field has a non-nullable parvadaid field.");
            }
            List<Temperature> temperatureListOrphanCheck = parvada.getTemperatureList();
            for (Temperature temperatureListOrphanCheckTemperature : temperatureListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Parvada (" + parvada + ") cannot be destroyed since the Temperature " + temperatureListOrphanCheckTemperature + " in its temperatureList field has a non-nullable parvadaid field.");
            }
            List<MedicineApplication> medicineApplicationListOrphanCheck = parvada.getMedicineApplicationList();
            for (MedicineApplication medicineApplicationListOrphanCheckMedicineApplication : medicineApplicationListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Parvada (" + parvada + ") cannot be destroyed since the MedicineApplication " + medicineApplicationListOrphanCheckMedicineApplication + " in its medicineApplicationList field has a non-nullable parvadaid field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Galpon galponid = parvada.getGalponid();
            if (galponid != null) {
                galponid.getParvadaList().remove(parvada);
                galponid = em.merge(galponid);
            }
            List<Mortality> mortalityList = parvada.getMortalityList();
            for (Mortality mortalityListMortality : mortalityList) {
                mortalityListMortality.setParvadaid(null);
                mortalityListMortality = em.merge(mortalityListMortality);
            }
            em.remove(parvada);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Parvada> findParvadaEntities() {
        return findParvadaEntities(true, -1, -1);
    }

    public List<Parvada> findParvadaEntities(int maxResults, int firstResult) {
        return findParvadaEntities(false, maxResults, firstResult);
    }

    private List<Parvada> findParvadaEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Parvada.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Parvada findParvada(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Parvada.class, id);
        } finally {
            em.close();
        }
    }

    public int getParvadaCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Parvada> rt = cq.from(Parvada.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
