/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import controllers.exceptions.IllegalOrphanException;
import controllers.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import entities.Enterprise;
import entities.Gallinaza;
import entities.Galpon;
import java.util.ArrayList;
import java.util.List;
import entities.GalponInventory;
import entities.Parvada;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Jean Carlo
 */
public class GalponJpaController implements Serializable {

    public GalponJpaController() {
        this.emf = Persistence.createEntityManagerFactory("AviPU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Galpon galpon) {
        if (galpon.getGallinazaList() == null) {
            galpon.setGallinazaList(new ArrayList<Gallinaza>());
        }
        if (galpon.getGalponInventoryList() == null) {
            galpon.setGalponInventoryList(new ArrayList<GalponInventory>());
        }
        if (galpon.getParvadaList() == null) {
            galpon.setParvadaList(new ArrayList<Parvada>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Enterprise enterpriseid = galpon.getEnterpriseid();
            if (enterpriseid != null) {
                enterpriseid = em.getReference(enterpriseid.getClass(), enterpriseid.getEnterpriseid());
                galpon.setEnterpriseid(enterpriseid);
            }
            List<Gallinaza> attachedGallinazaList = new ArrayList<Gallinaza>();
            for (Gallinaza gallinazaListGallinazaToAttach : galpon.getGallinazaList()) {
                gallinazaListGallinazaToAttach = em.getReference(gallinazaListGallinazaToAttach.getClass(), gallinazaListGallinazaToAttach.getGallinazaid());
                attachedGallinazaList.add(gallinazaListGallinazaToAttach);
            }
            galpon.setGallinazaList(attachedGallinazaList);
            List<GalponInventory> attachedGalponInventoryList = new ArrayList<GalponInventory>();
            for (GalponInventory galponInventoryListGalponInventoryToAttach : galpon.getGalponInventoryList()) {
                galponInventoryListGalponInventoryToAttach = em.getReference(galponInventoryListGalponInventoryToAttach.getClass(), galponInventoryListGalponInventoryToAttach.getGalponinventoryid());
                attachedGalponInventoryList.add(galponInventoryListGalponInventoryToAttach);
            }
            galpon.setGalponInventoryList(attachedGalponInventoryList);
            List<Parvada> attachedParvadaList = new ArrayList<Parvada>();
            for (Parvada parvadaListParvadaToAttach : galpon.getParvadaList()) {
                parvadaListParvadaToAttach = em.getReference(parvadaListParvadaToAttach.getClass(), parvadaListParvadaToAttach.getParvadaid());
                attachedParvadaList.add(parvadaListParvadaToAttach);
            }
            galpon.setParvadaList(attachedParvadaList);
            em.persist(galpon);
            if (enterpriseid != null) {
                enterpriseid.getGalponList().add(galpon);
                enterpriseid = em.merge(enterpriseid);
            }
            for (Gallinaza gallinazaListGallinaza : galpon.getGallinazaList()) {
                Galpon oldGalponidOfGallinazaListGallinaza = gallinazaListGallinaza.getGalponid();
                gallinazaListGallinaza.setGalponid(galpon);
                gallinazaListGallinaza = em.merge(gallinazaListGallinaza);
                if (oldGalponidOfGallinazaListGallinaza != null) {
                    oldGalponidOfGallinazaListGallinaza.getGallinazaList().remove(gallinazaListGallinaza);
                    oldGalponidOfGallinazaListGallinaza = em.merge(oldGalponidOfGallinazaListGallinaza);
                }
            }
            for (GalponInventory galponInventoryListGalponInventory : galpon.getGalponInventoryList()) {
                Galpon oldGalponidOfGalponInventoryListGalponInventory = galponInventoryListGalponInventory.getGalponid();
                galponInventoryListGalponInventory.setGalponid(galpon);
                galponInventoryListGalponInventory = em.merge(galponInventoryListGalponInventory);
                if (oldGalponidOfGalponInventoryListGalponInventory != null) {
                    oldGalponidOfGalponInventoryListGalponInventory.getGalponInventoryList().remove(galponInventoryListGalponInventory);
                    oldGalponidOfGalponInventoryListGalponInventory = em.merge(oldGalponidOfGalponInventoryListGalponInventory);
                }
            }
            for (Parvada parvadaListParvada : galpon.getParvadaList()) {
                Galpon oldGalponidOfParvadaListParvada = parvadaListParvada.getGalponid();
                parvadaListParvada.setGalponid(galpon);
                parvadaListParvada = em.merge(parvadaListParvada);
                if (oldGalponidOfParvadaListParvada != null) {
                    oldGalponidOfParvadaListParvada.getParvadaList().remove(parvadaListParvada);
                    oldGalponidOfParvadaListParvada = em.merge(oldGalponidOfParvadaListParvada);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Galpon galpon) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Galpon persistentGalpon = em.find(Galpon.class, galpon.getGalponid());
            Enterprise enterpriseidOld = persistentGalpon.getEnterpriseid();
            Enterprise enterpriseidNew = galpon.getEnterpriseid();
            List<Gallinaza> gallinazaListOld = persistentGalpon.getGallinazaList();
            List<Gallinaza> gallinazaListNew = galpon.getGallinazaList();
            List<GalponInventory> galponInventoryListOld = persistentGalpon.getGalponInventoryList();
            List<GalponInventory> galponInventoryListNew = galpon.getGalponInventoryList();
            List<Parvada> parvadaListOld = persistentGalpon.getParvadaList();
            List<Parvada> parvadaListNew = galpon.getParvadaList();
            List<String> illegalOrphanMessages = null;
            for (Gallinaza gallinazaListOldGallinaza : gallinazaListOld) {
                if (!gallinazaListNew.contains(gallinazaListOldGallinaza)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Gallinaza " + gallinazaListOldGallinaza + " since its galponid field is not nullable.");
                }
            }
            for (GalponInventory galponInventoryListOldGalponInventory : galponInventoryListOld) {
                if (!galponInventoryListNew.contains(galponInventoryListOldGalponInventory)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain GalponInventory " + galponInventoryListOldGalponInventory + " since its galponid field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (enterpriseidNew != null) {
                enterpriseidNew = em.getReference(enterpriseidNew.getClass(), enterpriseidNew.getEnterpriseid());
                galpon.setEnterpriseid(enterpriseidNew);
            }
            List<Gallinaza> attachedGallinazaListNew = new ArrayList<Gallinaza>();
            for (Gallinaza gallinazaListNewGallinazaToAttach : gallinazaListNew) {
                gallinazaListNewGallinazaToAttach = em.getReference(gallinazaListNewGallinazaToAttach.getClass(), gallinazaListNewGallinazaToAttach.getGallinazaid());
                attachedGallinazaListNew.add(gallinazaListNewGallinazaToAttach);
            }
            gallinazaListNew = attachedGallinazaListNew;
            galpon.setGallinazaList(gallinazaListNew);
            List<GalponInventory> attachedGalponInventoryListNew = new ArrayList<GalponInventory>();
            for (GalponInventory galponInventoryListNewGalponInventoryToAttach : galponInventoryListNew) {
                galponInventoryListNewGalponInventoryToAttach = em.getReference(galponInventoryListNewGalponInventoryToAttach.getClass(), galponInventoryListNewGalponInventoryToAttach.getGalponinventoryid());
                attachedGalponInventoryListNew.add(galponInventoryListNewGalponInventoryToAttach);
            }
            galponInventoryListNew = attachedGalponInventoryListNew;
            galpon.setGalponInventoryList(galponInventoryListNew);
            List<Parvada> attachedParvadaListNew = new ArrayList<Parvada>();
            for (Parvada parvadaListNewParvadaToAttach : parvadaListNew) {
                parvadaListNewParvadaToAttach = em.getReference(parvadaListNewParvadaToAttach.getClass(), parvadaListNewParvadaToAttach.getParvadaid());
                attachedParvadaListNew.add(parvadaListNewParvadaToAttach);
            }
            parvadaListNew = attachedParvadaListNew;
            galpon.setParvadaList(parvadaListNew);
            galpon = em.merge(galpon);
            if (enterpriseidOld != null && !enterpriseidOld.equals(enterpriseidNew)) {
                enterpriseidOld.getGalponList().remove(galpon);
                enterpriseidOld = em.merge(enterpriseidOld);
            }
            if (enterpriseidNew != null && !enterpriseidNew.equals(enterpriseidOld)) {
                enterpriseidNew.getGalponList().add(galpon);
                enterpriseidNew = em.merge(enterpriseidNew);
            }
            for (Gallinaza gallinazaListNewGallinaza : gallinazaListNew) {
                if (!gallinazaListOld.contains(gallinazaListNewGallinaza)) {
                    Galpon oldGalponidOfGallinazaListNewGallinaza = gallinazaListNewGallinaza.getGalponid();
                    gallinazaListNewGallinaza.setGalponid(galpon);
                    gallinazaListNewGallinaza = em.merge(gallinazaListNewGallinaza);
                    if (oldGalponidOfGallinazaListNewGallinaza != null && !oldGalponidOfGallinazaListNewGallinaza.equals(galpon)) {
                        oldGalponidOfGallinazaListNewGallinaza.getGallinazaList().remove(gallinazaListNewGallinaza);
                        oldGalponidOfGallinazaListNewGallinaza = em.merge(oldGalponidOfGallinazaListNewGallinaza);
                    }
                }
            }
            for (GalponInventory galponInventoryListNewGalponInventory : galponInventoryListNew) {
                if (!galponInventoryListOld.contains(galponInventoryListNewGalponInventory)) {
                    Galpon oldGalponidOfGalponInventoryListNewGalponInventory = galponInventoryListNewGalponInventory.getGalponid();
                    galponInventoryListNewGalponInventory.setGalponid(galpon);
                    galponInventoryListNewGalponInventory = em.merge(galponInventoryListNewGalponInventory);
                    if (oldGalponidOfGalponInventoryListNewGalponInventory != null && !oldGalponidOfGalponInventoryListNewGalponInventory.equals(galpon)) {
                        oldGalponidOfGalponInventoryListNewGalponInventory.getGalponInventoryList().remove(galponInventoryListNewGalponInventory);
                        oldGalponidOfGalponInventoryListNewGalponInventory = em.merge(oldGalponidOfGalponInventoryListNewGalponInventory);
                    }
                }
            }
            for (Parvada parvadaListOldParvada : parvadaListOld) {
                if (!parvadaListNew.contains(parvadaListOldParvada)) {
                    parvadaListOldParvada.setGalponid(null);
                    parvadaListOldParvada = em.merge(parvadaListOldParvada);
                }
            }
            for (Parvada parvadaListNewParvada : parvadaListNew) {
                if (!parvadaListOld.contains(parvadaListNewParvada)) {
                    Galpon oldGalponidOfParvadaListNewParvada = parvadaListNewParvada.getGalponid();
                    parvadaListNewParvada.setGalponid(galpon);
                    parvadaListNewParvada = em.merge(parvadaListNewParvada);
                    if (oldGalponidOfParvadaListNewParvada != null && !oldGalponidOfParvadaListNewParvada.equals(galpon)) {
                        oldGalponidOfParvadaListNewParvada.getParvadaList().remove(parvadaListNewParvada);
                        oldGalponidOfParvadaListNewParvada = em.merge(oldGalponidOfParvadaListNewParvada);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = galpon.getGalponid();
                if (findGalpon(id) == null) {
                    throw new NonexistentEntityException("The galpon with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Galpon galpon;
            try {
                galpon = em.getReference(Galpon.class, id);
                galpon.getGalponid();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The galpon with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Gallinaza> gallinazaListOrphanCheck = galpon.getGallinazaList();
            for (Gallinaza gallinazaListOrphanCheckGallinaza : gallinazaListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Galpon (" + galpon + ") cannot be destroyed since the Gallinaza " + gallinazaListOrphanCheckGallinaza + " in its gallinazaList field has a non-nullable galponid field.");
            }
            List<GalponInventory> galponInventoryListOrphanCheck = galpon.getGalponInventoryList();
            for (GalponInventory galponInventoryListOrphanCheckGalponInventory : galponInventoryListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Galpon (" + galpon + ") cannot be destroyed since the GalponInventory " + galponInventoryListOrphanCheckGalponInventory + " in its galponInventoryList field has a non-nullable galponid field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Enterprise enterpriseid = galpon.getEnterpriseid();
            if (enterpriseid != null) {
                enterpriseid.getGalponList().remove(galpon);
                enterpriseid = em.merge(enterpriseid);
            }
            List<Parvada> parvadaList = galpon.getParvadaList();
            for (Parvada parvadaListParvada : parvadaList) {
                parvadaListParvada.setGalponid(null);
                parvadaListParvada = em.merge(parvadaListParvada);
            }
            em.remove(galpon);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Galpon> findGalponEntities() {
        return findGalponEntities(true, -1, -1);
    }

    public List<Galpon> findGalponEntities(int maxResults, int firstResult) {
        return findGalponEntities(false, maxResults, firstResult);
    }

    private List<Galpon> findGalponEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Galpon.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Galpon findGalpon(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Galpon.class, id);
        } finally {
            em.close();
        }
    }

    public int getGalponCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Galpon> rt = cq.from(Galpon.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
