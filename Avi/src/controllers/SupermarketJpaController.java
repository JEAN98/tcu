/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import controllers.exceptions.IllegalOrphanException;
import controllers.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import entities.Enterprise;
import entities.SalesBySupermarket;
import entities.Supermarket;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Jean Carlo
 */
public class SupermarketJpaController implements Serializable {

    public SupermarketJpaController() {
        this.emf = Persistence.createEntityManagerFactory("AviPU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Supermarket supermarket) {
        if (supermarket.getSalesBySupermarketList() == null) {
            supermarket.setSalesBySupermarketList(new ArrayList<SalesBySupermarket>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Enterprise enterpriseid = supermarket.getEnterpriseid();
            if (enterpriseid != null) {
                enterpriseid = em.getReference(enterpriseid.getClass(), enterpriseid.getEnterpriseid());
                supermarket.setEnterpriseid(enterpriseid);
            }
            List<SalesBySupermarket> attachedSalesBySupermarketList = new ArrayList<SalesBySupermarket>();
            for (SalesBySupermarket salesBySupermarketListSalesBySupermarketToAttach : supermarket.getSalesBySupermarketList()) {
                salesBySupermarketListSalesBySupermarketToAttach = em.getReference(salesBySupermarketListSalesBySupermarketToAttach.getClass(), salesBySupermarketListSalesBySupermarketToAttach.getSalesbysupermarketid());
                attachedSalesBySupermarketList.add(salesBySupermarketListSalesBySupermarketToAttach);
            }
            supermarket.setSalesBySupermarketList(attachedSalesBySupermarketList);
            em.persist(supermarket);
            if (enterpriseid != null) {
                enterpriseid.getSupermarketList().add(supermarket);
                enterpriseid = em.merge(enterpriseid);
            }
            for (SalesBySupermarket salesBySupermarketListSalesBySupermarket : supermarket.getSalesBySupermarketList()) {
                Supermarket oldSupermarketidOfSalesBySupermarketListSalesBySupermarket = salesBySupermarketListSalesBySupermarket.getSupermarketid();
                salesBySupermarketListSalesBySupermarket.setSupermarketid(supermarket);
                salesBySupermarketListSalesBySupermarket = em.merge(salesBySupermarketListSalesBySupermarket);
                if (oldSupermarketidOfSalesBySupermarketListSalesBySupermarket != null) {
                    oldSupermarketidOfSalesBySupermarketListSalesBySupermarket.getSalesBySupermarketList().remove(salesBySupermarketListSalesBySupermarket);
                    oldSupermarketidOfSalesBySupermarketListSalesBySupermarket = em.merge(oldSupermarketidOfSalesBySupermarketListSalesBySupermarket);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Supermarket supermarket) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Supermarket persistentSupermarket = em.find(Supermarket.class, supermarket.getSupermarketid());
            Enterprise enterpriseidOld = persistentSupermarket.getEnterpriseid();
            Enterprise enterpriseidNew = supermarket.getEnterpriseid();
            List<SalesBySupermarket> salesBySupermarketListOld = persistentSupermarket.getSalesBySupermarketList();
            List<SalesBySupermarket> salesBySupermarketListNew = supermarket.getSalesBySupermarketList();
            List<String> illegalOrphanMessages = null;
            for (SalesBySupermarket salesBySupermarketListOldSalesBySupermarket : salesBySupermarketListOld) {
                if (!salesBySupermarketListNew.contains(salesBySupermarketListOldSalesBySupermarket)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain SalesBySupermarket " + salesBySupermarketListOldSalesBySupermarket + " since its supermarketid field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (enterpriseidNew != null) {
                enterpriseidNew = em.getReference(enterpriseidNew.getClass(), enterpriseidNew.getEnterpriseid());
                supermarket.setEnterpriseid(enterpriseidNew);
            }
            List<SalesBySupermarket> attachedSalesBySupermarketListNew = new ArrayList<SalesBySupermarket>();
            for (SalesBySupermarket salesBySupermarketListNewSalesBySupermarketToAttach : salesBySupermarketListNew) {
                salesBySupermarketListNewSalesBySupermarketToAttach = em.getReference(salesBySupermarketListNewSalesBySupermarketToAttach.getClass(), salesBySupermarketListNewSalesBySupermarketToAttach.getSalesbysupermarketid());
                attachedSalesBySupermarketListNew.add(salesBySupermarketListNewSalesBySupermarketToAttach);
            }
            salesBySupermarketListNew = attachedSalesBySupermarketListNew;
            supermarket.setSalesBySupermarketList(salesBySupermarketListNew);
            supermarket = em.merge(supermarket);
            if (enterpriseidOld != null && !enterpriseidOld.equals(enterpriseidNew)) {
                enterpriseidOld.getSupermarketList().remove(supermarket);
                enterpriseidOld = em.merge(enterpriseidOld);
            }
            if (enterpriseidNew != null && !enterpriseidNew.equals(enterpriseidOld)) {
                enterpriseidNew.getSupermarketList().add(supermarket);
                enterpriseidNew = em.merge(enterpriseidNew);
            }
            for (SalesBySupermarket salesBySupermarketListNewSalesBySupermarket : salesBySupermarketListNew) {
                if (!salesBySupermarketListOld.contains(salesBySupermarketListNewSalesBySupermarket)) {
                    Supermarket oldSupermarketidOfSalesBySupermarketListNewSalesBySupermarket = salesBySupermarketListNewSalesBySupermarket.getSupermarketid();
                    salesBySupermarketListNewSalesBySupermarket.setSupermarketid(supermarket);
                    salesBySupermarketListNewSalesBySupermarket = em.merge(salesBySupermarketListNewSalesBySupermarket);
                    if (oldSupermarketidOfSalesBySupermarketListNewSalesBySupermarket != null && !oldSupermarketidOfSalesBySupermarketListNewSalesBySupermarket.equals(supermarket)) {
                        oldSupermarketidOfSalesBySupermarketListNewSalesBySupermarket.getSalesBySupermarketList().remove(salesBySupermarketListNewSalesBySupermarket);
                        oldSupermarketidOfSalesBySupermarketListNewSalesBySupermarket = em.merge(oldSupermarketidOfSalesBySupermarketListNewSalesBySupermarket);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = supermarket.getSupermarketid();
                if (findSupermarket(id) == null) {
                    throw new NonexistentEntityException("The supermarket with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Supermarket supermarket;
            try {
                supermarket = em.getReference(Supermarket.class, id);
                supermarket.getSupermarketid();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The supermarket with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<SalesBySupermarket> salesBySupermarketListOrphanCheck = supermarket.getSalesBySupermarketList();
            for (SalesBySupermarket salesBySupermarketListOrphanCheckSalesBySupermarket : salesBySupermarketListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Supermarket (" + supermarket + ") cannot be destroyed since the SalesBySupermarket " + salesBySupermarketListOrphanCheckSalesBySupermarket + " in its salesBySupermarketList field has a non-nullable supermarketid field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Enterprise enterpriseid = supermarket.getEnterpriseid();
            if (enterpriseid != null) {
                enterpriseid.getSupermarketList().remove(supermarket);
                enterpriseid = em.merge(enterpriseid);
            }
            em.remove(supermarket);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Supermarket> findSupermarketEntities() {
        return findSupermarketEntities(true, -1, -1);
    }

    public List<Supermarket> findSupermarketEntities(int maxResults, int firstResult) {
        return findSupermarketEntities(false, maxResults, firstResult);
    }

    private List<Supermarket> findSupermarketEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Supermarket.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Supermarket findSupermarket(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Supermarket.class, id);
        } finally {
            em.close();
        }
    }

    public int getSupermarketCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Supermarket> rt = cq.from(Supermarket.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
