/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import controllers.exceptions.IllegalOrphanException;
import controllers.exceptions.NonexistentEntityException;
import entities.Sales;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import entities.SalesByCustomer;
import java.util.ArrayList;
import java.util.List;
import entities.SalesBySupermarket;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Jean Carlo
 */
public class SalesJpaController implements Serializable {

    public SalesJpaController() {
        this.emf = Persistence.createEntityManagerFactory("AviPU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Sales sales) {
        if (sales.getSalesByCustomerList() == null) {
            sales.setSalesByCustomerList(new ArrayList<SalesByCustomer>());
        }
        if (sales.getSalesBySupermarketList() == null) {
            sales.setSalesBySupermarketList(new ArrayList<SalesBySupermarket>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<SalesByCustomer> attachedSalesByCustomerList = new ArrayList<SalesByCustomer>();
            for (SalesByCustomer salesByCustomerListSalesByCustomerToAttach : sales.getSalesByCustomerList()) {
                salesByCustomerListSalesByCustomerToAttach = em.getReference(salesByCustomerListSalesByCustomerToAttach.getClass(), salesByCustomerListSalesByCustomerToAttach.getSalesbyclientid());
                attachedSalesByCustomerList.add(salesByCustomerListSalesByCustomerToAttach);
            }
            sales.setSalesByCustomerList(attachedSalesByCustomerList);
            List<SalesBySupermarket> attachedSalesBySupermarketList = new ArrayList<SalesBySupermarket>();
            for (SalesBySupermarket salesBySupermarketListSalesBySupermarketToAttach : sales.getSalesBySupermarketList()) {
                salesBySupermarketListSalesBySupermarketToAttach = em.getReference(salesBySupermarketListSalesBySupermarketToAttach.getClass(), salesBySupermarketListSalesBySupermarketToAttach.getSalesbysupermarketid());
                attachedSalesBySupermarketList.add(salesBySupermarketListSalesBySupermarketToAttach);
            }
            sales.setSalesBySupermarketList(attachedSalesBySupermarketList);
            em.persist(sales);
            for (SalesByCustomer salesByCustomerListSalesByCustomer : sales.getSalesByCustomerList()) {
                Sales oldSalesidOfSalesByCustomerListSalesByCustomer = salesByCustomerListSalesByCustomer.getSalesid();
                salesByCustomerListSalesByCustomer.setSalesid(sales);
                salesByCustomerListSalesByCustomer = em.merge(salesByCustomerListSalesByCustomer);
                if (oldSalesidOfSalesByCustomerListSalesByCustomer != null) {
                    oldSalesidOfSalesByCustomerListSalesByCustomer.getSalesByCustomerList().remove(salesByCustomerListSalesByCustomer);
                    oldSalesidOfSalesByCustomerListSalesByCustomer = em.merge(oldSalesidOfSalesByCustomerListSalesByCustomer);
                }
            }
            for (SalesBySupermarket salesBySupermarketListSalesBySupermarket : sales.getSalesBySupermarketList()) {
                Sales oldSalesidOfSalesBySupermarketListSalesBySupermarket = salesBySupermarketListSalesBySupermarket.getSalesid();
                salesBySupermarketListSalesBySupermarket.setSalesid(sales);
                salesBySupermarketListSalesBySupermarket = em.merge(salesBySupermarketListSalesBySupermarket);
                if (oldSalesidOfSalesBySupermarketListSalesBySupermarket != null) {
                    oldSalesidOfSalesBySupermarketListSalesBySupermarket.getSalesBySupermarketList().remove(salesBySupermarketListSalesBySupermarket);
                    oldSalesidOfSalesBySupermarketListSalesBySupermarket = em.merge(oldSalesidOfSalesBySupermarketListSalesBySupermarket);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Sales sales) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Sales persistentSales = em.find(Sales.class, sales.getSalesid());
            List<SalesByCustomer> salesByCustomerListOld = persistentSales.getSalesByCustomerList();
            List<SalesByCustomer> salesByCustomerListNew = sales.getSalesByCustomerList();
            List<SalesBySupermarket> salesBySupermarketListOld = persistentSales.getSalesBySupermarketList();
            List<SalesBySupermarket> salesBySupermarketListNew = sales.getSalesBySupermarketList();
            List<String> illegalOrphanMessages = null;
            for (SalesByCustomer salesByCustomerListOldSalesByCustomer : salesByCustomerListOld) {
                if (!salesByCustomerListNew.contains(salesByCustomerListOldSalesByCustomer)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain SalesByCustomer " + salesByCustomerListOldSalesByCustomer + " since its salesid field is not nullable.");
                }
            }
            for (SalesBySupermarket salesBySupermarketListOldSalesBySupermarket : salesBySupermarketListOld) {
                if (!salesBySupermarketListNew.contains(salesBySupermarketListOldSalesBySupermarket)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain SalesBySupermarket " + salesBySupermarketListOldSalesBySupermarket + " since its salesid field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<SalesByCustomer> attachedSalesByCustomerListNew = new ArrayList<SalesByCustomer>();
            for (SalesByCustomer salesByCustomerListNewSalesByCustomerToAttach : salesByCustomerListNew) {
                salesByCustomerListNewSalesByCustomerToAttach = em.getReference(salesByCustomerListNewSalesByCustomerToAttach.getClass(), salesByCustomerListNewSalesByCustomerToAttach.getSalesbyclientid());
                attachedSalesByCustomerListNew.add(salesByCustomerListNewSalesByCustomerToAttach);
            }
            salesByCustomerListNew = attachedSalesByCustomerListNew;
            sales.setSalesByCustomerList(salesByCustomerListNew);
            List<SalesBySupermarket> attachedSalesBySupermarketListNew = new ArrayList<SalesBySupermarket>();
            for (SalesBySupermarket salesBySupermarketListNewSalesBySupermarketToAttach : salesBySupermarketListNew) {
                salesBySupermarketListNewSalesBySupermarketToAttach = em.getReference(salesBySupermarketListNewSalesBySupermarketToAttach.getClass(), salesBySupermarketListNewSalesBySupermarketToAttach.getSalesbysupermarketid());
                attachedSalesBySupermarketListNew.add(salesBySupermarketListNewSalesBySupermarketToAttach);
            }
            salesBySupermarketListNew = attachedSalesBySupermarketListNew;
            sales.setSalesBySupermarketList(salesBySupermarketListNew);
            sales = em.merge(sales);
            for (SalesByCustomer salesByCustomerListNewSalesByCustomer : salesByCustomerListNew) {
                if (!salesByCustomerListOld.contains(salesByCustomerListNewSalesByCustomer)) {
                    Sales oldSalesidOfSalesByCustomerListNewSalesByCustomer = salesByCustomerListNewSalesByCustomer.getSalesid();
                    salesByCustomerListNewSalesByCustomer.setSalesid(sales);
                    salesByCustomerListNewSalesByCustomer = em.merge(salesByCustomerListNewSalesByCustomer);
                    if (oldSalesidOfSalesByCustomerListNewSalesByCustomer != null && !oldSalesidOfSalesByCustomerListNewSalesByCustomer.equals(sales)) {
                        oldSalesidOfSalesByCustomerListNewSalesByCustomer.getSalesByCustomerList().remove(salesByCustomerListNewSalesByCustomer);
                        oldSalesidOfSalesByCustomerListNewSalesByCustomer = em.merge(oldSalesidOfSalesByCustomerListNewSalesByCustomer);
                    }
                }
            }
            for (SalesBySupermarket salesBySupermarketListNewSalesBySupermarket : salesBySupermarketListNew) {
                if (!salesBySupermarketListOld.contains(salesBySupermarketListNewSalesBySupermarket)) {
                    Sales oldSalesidOfSalesBySupermarketListNewSalesBySupermarket = salesBySupermarketListNewSalesBySupermarket.getSalesid();
                    salesBySupermarketListNewSalesBySupermarket.setSalesid(sales);
                    salesBySupermarketListNewSalesBySupermarket = em.merge(salesBySupermarketListNewSalesBySupermarket);
                    if (oldSalesidOfSalesBySupermarketListNewSalesBySupermarket != null && !oldSalesidOfSalesBySupermarketListNewSalesBySupermarket.equals(sales)) {
                        oldSalesidOfSalesBySupermarketListNewSalesBySupermarket.getSalesBySupermarketList().remove(salesBySupermarketListNewSalesBySupermarket);
                        oldSalesidOfSalesBySupermarketListNewSalesBySupermarket = em.merge(oldSalesidOfSalesBySupermarketListNewSalesBySupermarket);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = sales.getSalesid();
                if (findSales(id) == null) {
                    throw new NonexistentEntityException("The sales with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Sales sales;
            try {
                sales = em.getReference(Sales.class, id);
                sales.getSalesid();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The sales with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<SalesByCustomer> salesByCustomerListOrphanCheck = sales.getSalesByCustomerList();
            for (SalesByCustomer salesByCustomerListOrphanCheckSalesByCustomer : salesByCustomerListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Sales (" + sales + ") cannot be destroyed since the SalesByCustomer " + salesByCustomerListOrphanCheckSalesByCustomer + " in its salesByCustomerList field has a non-nullable salesid field.");
            }
            List<SalesBySupermarket> salesBySupermarketListOrphanCheck = sales.getSalesBySupermarketList();
            for (SalesBySupermarket salesBySupermarketListOrphanCheckSalesBySupermarket : salesBySupermarketListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Sales (" + sales + ") cannot be destroyed since the SalesBySupermarket " + salesBySupermarketListOrphanCheckSalesBySupermarket + " in its salesBySupermarketList field has a non-nullable salesid field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(sales);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Sales> findSalesEntities() {
        return findSalesEntities(true, -1, -1);
    }

    public List<Sales> findSalesEntities(int maxResults, int firstResult) {
        return findSalesEntities(false, maxResults, firstResult);
    }

    private List<Sales> findSalesEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Sales.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Sales findSales(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Sales.class, id);
        } finally {
            em.close();
        }
    }

    public int getSalesCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Sales> rt = cq.from(Sales.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
