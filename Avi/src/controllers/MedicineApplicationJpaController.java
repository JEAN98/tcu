/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import controllers.exceptions.NonexistentEntityException;
import entities.MedicineApplication;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import entities.Parvada;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Jean Carlo
 */
public class MedicineApplicationJpaController implements Serializable {

    public MedicineApplicationJpaController() {
        this.emf = Persistence.createEntityManagerFactory("AviPU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(MedicineApplication medicineApplication) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Parvada parvadaid = medicineApplication.getParvadaid();
            if (parvadaid != null) {
                parvadaid = em.getReference(parvadaid.getClass(), parvadaid.getParvadaid());
                medicineApplication.setParvadaid(parvadaid);
            }
            em.persist(medicineApplication);
            if (parvadaid != null) {
                parvadaid.getMedicineApplicationList().add(medicineApplication);
                parvadaid = em.merge(parvadaid);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(MedicineApplication medicineApplication) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            MedicineApplication persistentMedicineApplication = em.find(MedicineApplication.class, medicineApplication.getMedicineapplicationid());
            Parvada parvadaidOld = persistentMedicineApplication.getParvadaid();
            Parvada parvadaidNew = medicineApplication.getParvadaid();
            if (parvadaidNew != null) {
                parvadaidNew = em.getReference(parvadaidNew.getClass(), parvadaidNew.getParvadaid());
                medicineApplication.setParvadaid(parvadaidNew);
            }
            medicineApplication = em.merge(medicineApplication);
            if (parvadaidOld != null && !parvadaidOld.equals(parvadaidNew)) {
                parvadaidOld.getMedicineApplicationList().remove(medicineApplication);
                parvadaidOld = em.merge(parvadaidOld);
            }
            if (parvadaidNew != null && !parvadaidNew.equals(parvadaidOld)) {
                parvadaidNew.getMedicineApplicationList().add(medicineApplication);
                parvadaidNew = em.merge(parvadaidNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = medicineApplication.getMedicineapplicationid();
                if (findMedicineApplication(id) == null) {
                    throw new NonexistentEntityException("The medicineApplication with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            MedicineApplication medicineApplication;
            try {
                medicineApplication = em.getReference(MedicineApplication.class, id);
                medicineApplication.getMedicineapplicationid();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The medicineApplication with id " + id + " no longer exists.", enfe);
            }
            Parvada parvadaid = medicineApplication.getParvadaid();
            if (parvadaid != null) {
                parvadaid.getMedicineApplicationList().remove(medicineApplication);
                parvadaid = em.merge(parvadaid);
            }
            em.remove(medicineApplication);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<MedicineApplication> findMedicineApplicationEntities() {
        return findMedicineApplicationEntities(true, -1, -1);
    }

    public List<MedicineApplication> findMedicineApplicationEntities(int maxResults, int firstResult) {
        return findMedicineApplicationEntities(false, maxResults, firstResult);
    }

    private List<MedicineApplication> findMedicineApplicationEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(MedicineApplication.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public MedicineApplication findMedicineApplication(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(MedicineApplication.class, id);
        } finally {
            em.close();
        }
    }

    public int getMedicineApplicationCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<MedicineApplication> rt = cq.from(MedicineApplication.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
