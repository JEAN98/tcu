/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import controllers.exceptions.IllegalOrphanException;
import controllers.exceptions.NonexistentEntityException;
import entities.Customer;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import entities.Enterprise;
import entities.SalesByCustomer;
import java.util.ArrayList;
import java.util.List;
import entities.SalesByCustomerGallinaza;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Jean Carlo
 */
public class CustomerJpaController implements Serializable {

    public CustomerJpaController() {
        this.emf = Persistence.createEntityManagerFactory("AviPU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Customer customer) {
        if (customer.getSalesByCustomerList() == null) {
            customer.setSalesByCustomerList(new ArrayList<SalesByCustomer>());
        }
        if (customer.getSalesByCustomerGallinazaList() == null) {
            customer.setSalesByCustomerGallinazaList(new ArrayList<SalesByCustomerGallinaza>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Enterprise enterpriseid = customer.getEnterpriseid();
            if (enterpriseid != null) {
                enterpriseid = em.getReference(enterpriseid.getClass(), enterpriseid.getEnterpriseid());
                customer.setEnterpriseid(enterpriseid);
            }
            List<SalesByCustomer> attachedSalesByCustomerList = new ArrayList<SalesByCustomer>();
            for (SalesByCustomer salesByCustomerListSalesByCustomerToAttach : customer.getSalesByCustomerList()) {
                salesByCustomerListSalesByCustomerToAttach = em.getReference(salesByCustomerListSalesByCustomerToAttach.getClass(), salesByCustomerListSalesByCustomerToAttach.getSalesbyclientid());
                attachedSalesByCustomerList.add(salesByCustomerListSalesByCustomerToAttach);
            }
            customer.setSalesByCustomerList(attachedSalesByCustomerList);
            List<SalesByCustomerGallinaza> attachedSalesByCustomerGallinazaList = new ArrayList<SalesByCustomerGallinaza>();
            for (SalesByCustomerGallinaza salesByCustomerGallinazaListSalesByCustomerGallinazaToAttach : customer.getSalesByCustomerGallinazaList()) {
                salesByCustomerGallinazaListSalesByCustomerGallinazaToAttach = em.getReference(salesByCustomerGallinazaListSalesByCustomerGallinazaToAttach.getClass(), salesByCustomerGallinazaListSalesByCustomerGallinazaToAttach.getSalesbyclientgallinazaid());
                attachedSalesByCustomerGallinazaList.add(salesByCustomerGallinazaListSalesByCustomerGallinazaToAttach);
            }
            customer.setSalesByCustomerGallinazaList(attachedSalesByCustomerGallinazaList);
            em.persist(customer);
            if (enterpriseid != null) {
                enterpriseid.getCustomerList().add(customer);
                enterpriseid = em.merge(enterpriseid);
            }
            for (SalesByCustomer salesByCustomerListSalesByCustomer : customer.getSalesByCustomerList()) {
                Customer oldClientidOfSalesByCustomerListSalesByCustomer = salesByCustomerListSalesByCustomer.getClientid();
                salesByCustomerListSalesByCustomer.setClientid(customer);
                salesByCustomerListSalesByCustomer = em.merge(salesByCustomerListSalesByCustomer);
                if (oldClientidOfSalesByCustomerListSalesByCustomer != null) {
                    oldClientidOfSalesByCustomerListSalesByCustomer.getSalesByCustomerList().remove(salesByCustomerListSalesByCustomer);
                    oldClientidOfSalesByCustomerListSalesByCustomer = em.merge(oldClientidOfSalesByCustomerListSalesByCustomer);
                }
            }
            for (SalesByCustomerGallinaza salesByCustomerGallinazaListSalesByCustomerGallinaza : customer.getSalesByCustomerGallinazaList()) {
                Customer oldClientidOfSalesByCustomerGallinazaListSalesByCustomerGallinaza = salesByCustomerGallinazaListSalesByCustomerGallinaza.getClientid();
                salesByCustomerGallinazaListSalesByCustomerGallinaza.setClientid(customer);
                salesByCustomerGallinazaListSalesByCustomerGallinaza = em.merge(salesByCustomerGallinazaListSalesByCustomerGallinaza);
                if (oldClientidOfSalesByCustomerGallinazaListSalesByCustomerGallinaza != null) {
                    oldClientidOfSalesByCustomerGallinazaListSalesByCustomerGallinaza.getSalesByCustomerGallinazaList().remove(salesByCustomerGallinazaListSalesByCustomerGallinaza);
                    oldClientidOfSalesByCustomerGallinazaListSalesByCustomerGallinaza = em.merge(oldClientidOfSalesByCustomerGallinazaListSalesByCustomerGallinaza);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Customer customer) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Customer persistentCustomer = em.find(Customer.class, customer.getClientid());
            Enterprise enterpriseidOld = persistentCustomer.getEnterpriseid();
            Enterprise enterpriseidNew = customer.getEnterpriseid();
            List<SalesByCustomer> salesByCustomerListOld = persistentCustomer.getSalesByCustomerList();
            List<SalesByCustomer> salesByCustomerListNew = customer.getSalesByCustomerList();
            List<SalesByCustomerGallinaza> salesByCustomerGallinazaListOld = persistentCustomer.getSalesByCustomerGallinazaList();
            List<SalesByCustomerGallinaza> salesByCustomerGallinazaListNew = customer.getSalesByCustomerGallinazaList();
            List<String> illegalOrphanMessages = null;
            for (SalesByCustomer salesByCustomerListOldSalesByCustomer : salesByCustomerListOld) {
                if (!salesByCustomerListNew.contains(salesByCustomerListOldSalesByCustomer)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain SalesByCustomer " + salesByCustomerListOldSalesByCustomer + " since its clientid field is not nullable.");
                }
            }
            for (SalesByCustomerGallinaza salesByCustomerGallinazaListOldSalesByCustomerGallinaza : salesByCustomerGallinazaListOld) {
                if (!salesByCustomerGallinazaListNew.contains(salesByCustomerGallinazaListOldSalesByCustomerGallinaza)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain SalesByCustomerGallinaza " + salesByCustomerGallinazaListOldSalesByCustomerGallinaza + " since its clientid field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (enterpriseidNew != null) {
                enterpriseidNew = em.getReference(enterpriseidNew.getClass(), enterpriseidNew.getEnterpriseid());
                customer.setEnterpriseid(enterpriseidNew);
            }
            List<SalesByCustomer> attachedSalesByCustomerListNew = new ArrayList<SalesByCustomer>();
            for (SalesByCustomer salesByCustomerListNewSalesByCustomerToAttach : salesByCustomerListNew) {
                salesByCustomerListNewSalesByCustomerToAttach = em.getReference(salesByCustomerListNewSalesByCustomerToAttach.getClass(), salesByCustomerListNewSalesByCustomerToAttach.getSalesbyclientid());
                attachedSalesByCustomerListNew.add(salesByCustomerListNewSalesByCustomerToAttach);
            }
            salesByCustomerListNew = attachedSalesByCustomerListNew;
            customer.setSalesByCustomerList(salesByCustomerListNew);
            List<SalesByCustomerGallinaza> attachedSalesByCustomerGallinazaListNew = new ArrayList<SalesByCustomerGallinaza>();
            for (SalesByCustomerGallinaza salesByCustomerGallinazaListNewSalesByCustomerGallinazaToAttach : salesByCustomerGallinazaListNew) {
                salesByCustomerGallinazaListNewSalesByCustomerGallinazaToAttach = em.getReference(salesByCustomerGallinazaListNewSalesByCustomerGallinazaToAttach.getClass(), salesByCustomerGallinazaListNewSalesByCustomerGallinazaToAttach.getSalesbyclientgallinazaid());
                attachedSalesByCustomerGallinazaListNew.add(salesByCustomerGallinazaListNewSalesByCustomerGallinazaToAttach);
            }
            salesByCustomerGallinazaListNew = attachedSalesByCustomerGallinazaListNew;
            customer.setSalesByCustomerGallinazaList(salesByCustomerGallinazaListNew);
            customer = em.merge(customer);
            if (enterpriseidOld != null && !enterpriseidOld.equals(enterpriseidNew)) {
                enterpriseidOld.getCustomerList().remove(customer);
                enterpriseidOld = em.merge(enterpriseidOld);
            }
            if (enterpriseidNew != null && !enterpriseidNew.equals(enterpriseidOld)) {
                enterpriseidNew.getCustomerList().add(customer);
                enterpriseidNew = em.merge(enterpriseidNew);
            }
            for (SalesByCustomer salesByCustomerListNewSalesByCustomer : salesByCustomerListNew) {
                if (!salesByCustomerListOld.contains(salesByCustomerListNewSalesByCustomer)) {
                    Customer oldClientidOfSalesByCustomerListNewSalesByCustomer = salesByCustomerListNewSalesByCustomer.getClientid();
                    salesByCustomerListNewSalesByCustomer.setClientid(customer);
                    salesByCustomerListNewSalesByCustomer = em.merge(salesByCustomerListNewSalesByCustomer);
                    if (oldClientidOfSalesByCustomerListNewSalesByCustomer != null && !oldClientidOfSalesByCustomerListNewSalesByCustomer.equals(customer)) {
                        oldClientidOfSalesByCustomerListNewSalesByCustomer.getSalesByCustomerList().remove(salesByCustomerListNewSalesByCustomer);
                        oldClientidOfSalesByCustomerListNewSalesByCustomer = em.merge(oldClientidOfSalesByCustomerListNewSalesByCustomer);
                    }
                }
            }
            for (SalesByCustomerGallinaza salesByCustomerGallinazaListNewSalesByCustomerGallinaza : salesByCustomerGallinazaListNew) {
                if (!salesByCustomerGallinazaListOld.contains(salesByCustomerGallinazaListNewSalesByCustomerGallinaza)) {
                    Customer oldClientidOfSalesByCustomerGallinazaListNewSalesByCustomerGallinaza = salesByCustomerGallinazaListNewSalesByCustomerGallinaza.getClientid();
                    salesByCustomerGallinazaListNewSalesByCustomerGallinaza.setClientid(customer);
                    salesByCustomerGallinazaListNewSalesByCustomerGallinaza = em.merge(salesByCustomerGallinazaListNewSalesByCustomerGallinaza);
                    if (oldClientidOfSalesByCustomerGallinazaListNewSalesByCustomerGallinaza != null && !oldClientidOfSalesByCustomerGallinazaListNewSalesByCustomerGallinaza.equals(customer)) {
                        oldClientidOfSalesByCustomerGallinazaListNewSalesByCustomerGallinaza.getSalesByCustomerGallinazaList().remove(salesByCustomerGallinazaListNewSalesByCustomerGallinaza);
                        oldClientidOfSalesByCustomerGallinazaListNewSalesByCustomerGallinaza = em.merge(oldClientidOfSalesByCustomerGallinazaListNewSalesByCustomerGallinaza);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = customer.getClientid();
                if (findCustomer(id) == null) {
                    throw new NonexistentEntityException("The customer with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Customer customer;
            try {
                customer = em.getReference(Customer.class, id);
                customer.getClientid();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The customer with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<SalesByCustomer> salesByCustomerListOrphanCheck = customer.getSalesByCustomerList();
            for (SalesByCustomer salesByCustomerListOrphanCheckSalesByCustomer : salesByCustomerListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Customer (" + customer + ") cannot be destroyed since the SalesByCustomer " + salesByCustomerListOrphanCheckSalesByCustomer + " in its salesByCustomerList field has a non-nullable clientid field.");
            }
            List<SalesByCustomerGallinaza> salesByCustomerGallinazaListOrphanCheck = customer.getSalesByCustomerGallinazaList();
            for (SalesByCustomerGallinaza salesByCustomerGallinazaListOrphanCheckSalesByCustomerGallinaza : salesByCustomerGallinazaListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Customer (" + customer + ") cannot be destroyed since the SalesByCustomerGallinaza " + salesByCustomerGallinazaListOrphanCheckSalesByCustomerGallinaza + " in its salesByCustomerGallinazaList field has a non-nullable clientid field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Enterprise enterpriseid = customer.getEnterpriseid();
            if (enterpriseid != null) {
                enterpriseid.getCustomerList().remove(customer);
                enterpriseid = em.merge(enterpriseid);
            }
            em.remove(customer);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Customer> findCustomerEntities() {
        return findCustomerEntities(true, -1, -1);
    }

    public List<Customer> findCustomerEntities(int maxResults, int firstResult) {
        return findCustomerEntities(false, maxResults, firstResult);
    }

    private List<Customer> findCustomerEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Customer.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Customer findCustomer(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Customer.class, id);
        } finally {
            em.close();
        }
    }

    public int getCustomerCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Customer> rt = cq.from(Customer.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
