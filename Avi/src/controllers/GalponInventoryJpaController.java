/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import controllers.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import entities.Galpon;
import entities.GalponInventory;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Jean Carlo
 */
public class GalponInventoryJpaController implements Serializable {

    public GalponInventoryJpaController() {
        this.emf = Persistence.createEntityManagerFactory("AviPU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(GalponInventory galponInventory) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Galpon galponid = galponInventory.getGalponid();
            if (galponid != null) {
                galponid = em.getReference(galponid.getClass(), galponid.getGalponid());
                galponInventory.setGalponid(galponid);
            }
            em.persist(galponInventory);
            if (galponid != null) {
                galponid.getGalponInventoryList().add(galponInventory);
                galponid = em.merge(galponid);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(GalponInventory galponInventory) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            GalponInventory persistentGalponInventory = em.find(GalponInventory.class, galponInventory.getGalponinventoryid());
            Galpon galponidOld = persistentGalponInventory.getGalponid();
            Galpon galponidNew = galponInventory.getGalponid();
            if (galponidNew != null) {
                galponidNew = em.getReference(galponidNew.getClass(), galponidNew.getGalponid());
                galponInventory.setGalponid(galponidNew);
            }
            galponInventory = em.merge(galponInventory);
            if (galponidOld != null && !galponidOld.equals(galponidNew)) {
                galponidOld.getGalponInventoryList().remove(galponInventory);
                galponidOld = em.merge(galponidOld);
            }
            if (galponidNew != null && !galponidNew.equals(galponidOld)) {
                galponidNew.getGalponInventoryList().add(galponInventory);
                galponidNew = em.merge(galponidNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = galponInventory.getGalponinventoryid();
                if (findGalponInventory(id) == null) {
                    throw new NonexistentEntityException("The galponInventory with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            GalponInventory galponInventory;
            try {
                galponInventory = em.getReference(GalponInventory.class, id);
                galponInventory.getGalponinventoryid();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The galponInventory with id " + id + " no longer exists.", enfe);
            }
            Galpon galponid = galponInventory.getGalponid();
            if (galponid != null) {
                galponid.getGalponInventoryList().remove(galponInventory);
                galponid = em.merge(galponid);
            }
            em.remove(galponInventory);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<GalponInventory> findGalponInventoryEntities() {
        return findGalponInventoryEntities(true, -1, -1);
    }

    public List<GalponInventory> findGalponInventoryEntities(int maxResults, int firstResult) {
        return findGalponInventoryEntities(false, maxResults, firstResult);
    }

    private List<GalponInventory> findGalponInventoryEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(GalponInventory.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public GalponInventory findGalponInventory(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(GalponInventory.class, id);
        } finally {
            em.close();
        }
    }

    public int getGalponInventoryCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<GalponInventory> rt = cq.from(GalponInventory.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
