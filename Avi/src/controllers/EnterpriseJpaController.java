/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import controllers.exceptions.IllegalOrphanException;
import controllers.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import entities.Galpon;
import java.util.ArrayList;
import java.util.List;
import entities.Customer;
import entities.Enterprise;
import entities.Supermarket;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Jean Carlo
 */
public class EnterpriseJpaController implements Serializable {
 private EntityManagerFactory emf = null;
    public EnterpriseJpaController() {
        this.emf = Persistence.createEntityManagerFactory("AviPU");
    }
   

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Enterprise enterprise) {
        if (enterprise.getGalponList() == null) {
            enterprise.setGalponList(new ArrayList<Galpon>());
        }
        if (enterprise.getCustomerList() == null) {
            enterprise.setCustomerList(new ArrayList<Customer>());
        }
        if (enterprise.getSupermarketList() == null) {
            enterprise.setSupermarketList(new ArrayList<Supermarket>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Galpon> attachedGalponList = new ArrayList<Galpon>();
            for (Galpon galponListGalponToAttach : enterprise.getGalponList()) {
                galponListGalponToAttach = em.getReference(galponListGalponToAttach.getClass(), galponListGalponToAttach.getGalponid());
                attachedGalponList.add(galponListGalponToAttach);
            }
            enterprise.setGalponList(attachedGalponList);
            List<Customer> attachedCustomerList = new ArrayList<Customer>();
            for (Customer customerListCustomerToAttach : enterprise.getCustomerList()) {
                customerListCustomerToAttach = em.getReference(customerListCustomerToAttach.getClass(), customerListCustomerToAttach.getClientid());
                attachedCustomerList.add(customerListCustomerToAttach);
            }
            enterprise.setCustomerList(attachedCustomerList);
            List<Supermarket> attachedSupermarketList = new ArrayList<Supermarket>();
            for (Supermarket supermarketListSupermarketToAttach : enterprise.getSupermarketList()) {
                supermarketListSupermarketToAttach = em.getReference(supermarketListSupermarketToAttach.getClass(), supermarketListSupermarketToAttach.getSupermarketid());
                attachedSupermarketList.add(supermarketListSupermarketToAttach);
            }
            enterprise.setSupermarketList(attachedSupermarketList);
            em.persist(enterprise);
            for (Galpon galponListGalpon : enterprise.getGalponList()) {
                Enterprise oldEnterpriseidOfGalponListGalpon = galponListGalpon.getEnterpriseid();
                galponListGalpon.setEnterpriseid(enterprise);
                galponListGalpon = em.merge(galponListGalpon);
                if (oldEnterpriseidOfGalponListGalpon != null) {
                    oldEnterpriseidOfGalponListGalpon.getGalponList().remove(galponListGalpon);
                    oldEnterpriseidOfGalponListGalpon = em.merge(oldEnterpriseidOfGalponListGalpon);
                }
            }
            for (Customer customerListCustomer : enterprise.getCustomerList()) {
                Enterprise oldEnterpriseidOfCustomerListCustomer = customerListCustomer.getEnterpriseid();
                customerListCustomer.setEnterpriseid(enterprise);
                customerListCustomer = em.merge(customerListCustomer);
                if (oldEnterpriseidOfCustomerListCustomer != null) {
                    oldEnterpriseidOfCustomerListCustomer.getCustomerList().remove(customerListCustomer);
                    oldEnterpriseidOfCustomerListCustomer = em.merge(oldEnterpriseidOfCustomerListCustomer);
                }
            }
            for (Supermarket supermarketListSupermarket : enterprise.getSupermarketList()) {
                Enterprise oldEnterpriseidOfSupermarketListSupermarket = supermarketListSupermarket.getEnterpriseid();
                supermarketListSupermarket.setEnterpriseid(enterprise);
                supermarketListSupermarket = em.merge(supermarketListSupermarket);
                if (oldEnterpriseidOfSupermarketListSupermarket != null) {
                    oldEnterpriseidOfSupermarketListSupermarket.getSupermarketList().remove(supermarketListSupermarket);
                    oldEnterpriseidOfSupermarketListSupermarket = em.merge(oldEnterpriseidOfSupermarketListSupermarket);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Enterprise enterprise) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Enterprise persistentEnterprise = em.find(Enterprise.class, enterprise.getEnterpriseid());
            List<Galpon> galponListOld = persistentEnterprise.getGalponList();
            List<Galpon> galponListNew = enterprise.getGalponList();
            List<Customer> customerListOld = persistentEnterprise.getCustomerList();
            List<Customer> customerListNew = enterprise.getCustomerList();
            List<Supermarket> supermarketListOld = persistentEnterprise.getSupermarketList();
            List<Supermarket> supermarketListNew = enterprise.getSupermarketList();
            List<String> illegalOrphanMessages = null;
            for (Supermarket supermarketListOldSupermarket : supermarketListOld) {
                if (!supermarketListNew.contains(supermarketListOldSupermarket)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Supermarket " + supermarketListOldSupermarket + " since its enterpriseid field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<Galpon> attachedGalponListNew = new ArrayList<Galpon>();
            for (Galpon galponListNewGalponToAttach : galponListNew) {
                galponListNewGalponToAttach = em.getReference(galponListNewGalponToAttach.getClass(), galponListNewGalponToAttach.getGalponid());
                attachedGalponListNew.add(galponListNewGalponToAttach);
            }
            galponListNew = attachedGalponListNew;
            enterprise.setGalponList(galponListNew);
            List<Customer> attachedCustomerListNew = new ArrayList<Customer>();
            for (Customer customerListNewCustomerToAttach : customerListNew) {
                customerListNewCustomerToAttach = em.getReference(customerListNewCustomerToAttach.getClass(), customerListNewCustomerToAttach.getClientid());
                attachedCustomerListNew.add(customerListNewCustomerToAttach);
            }
            customerListNew = attachedCustomerListNew;
            enterprise.setCustomerList(customerListNew);
            List<Supermarket> attachedSupermarketListNew = new ArrayList<Supermarket>();
            for (Supermarket supermarketListNewSupermarketToAttach : supermarketListNew) {
                supermarketListNewSupermarketToAttach = em.getReference(supermarketListNewSupermarketToAttach.getClass(), supermarketListNewSupermarketToAttach.getSupermarketid());
                attachedSupermarketListNew.add(supermarketListNewSupermarketToAttach);
            }
            supermarketListNew = attachedSupermarketListNew;
            enterprise.setSupermarketList(supermarketListNew);
            enterprise = em.merge(enterprise);
            for (Galpon galponListOldGalpon : galponListOld) {
                if (!galponListNew.contains(galponListOldGalpon)) {
                    galponListOldGalpon.setEnterpriseid(null);
                    galponListOldGalpon = em.merge(galponListOldGalpon);
                }
            }
            for (Galpon galponListNewGalpon : galponListNew) {
                if (!galponListOld.contains(galponListNewGalpon)) {
                    Enterprise oldEnterpriseidOfGalponListNewGalpon = galponListNewGalpon.getEnterpriseid();
                    galponListNewGalpon.setEnterpriseid(enterprise);
                    galponListNewGalpon = em.merge(galponListNewGalpon);
                    if (oldEnterpriseidOfGalponListNewGalpon != null && !oldEnterpriseidOfGalponListNewGalpon.equals(enterprise)) {
                        oldEnterpriseidOfGalponListNewGalpon.getGalponList().remove(galponListNewGalpon);
                        oldEnterpriseidOfGalponListNewGalpon = em.merge(oldEnterpriseidOfGalponListNewGalpon);
                    }
                }
            }
            for (Customer customerListOldCustomer : customerListOld) {
                if (!customerListNew.contains(customerListOldCustomer)) {
                    customerListOldCustomer.setEnterpriseid(null);
                    customerListOldCustomer = em.merge(customerListOldCustomer);
                }
            }
            for (Customer customerListNewCustomer : customerListNew) {
                if (!customerListOld.contains(customerListNewCustomer)) {
                    Enterprise oldEnterpriseidOfCustomerListNewCustomer = customerListNewCustomer.getEnterpriseid();
                    customerListNewCustomer.setEnterpriseid(enterprise);
                    customerListNewCustomer = em.merge(customerListNewCustomer);
                    if (oldEnterpriseidOfCustomerListNewCustomer != null && !oldEnterpriseidOfCustomerListNewCustomer.equals(enterprise)) {
                        oldEnterpriseidOfCustomerListNewCustomer.getCustomerList().remove(customerListNewCustomer);
                        oldEnterpriseidOfCustomerListNewCustomer = em.merge(oldEnterpriseidOfCustomerListNewCustomer);
                    }
                }
            }
            for (Supermarket supermarketListNewSupermarket : supermarketListNew) {
                if (!supermarketListOld.contains(supermarketListNewSupermarket)) {
                    Enterprise oldEnterpriseidOfSupermarketListNewSupermarket = supermarketListNewSupermarket.getEnterpriseid();
                    supermarketListNewSupermarket.setEnterpriseid(enterprise);
                    supermarketListNewSupermarket = em.merge(supermarketListNewSupermarket);
                    if (oldEnterpriseidOfSupermarketListNewSupermarket != null && !oldEnterpriseidOfSupermarketListNewSupermarket.equals(enterprise)) {
                        oldEnterpriseidOfSupermarketListNewSupermarket.getSupermarketList().remove(supermarketListNewSupermarket);
                        oldEnterpriseidOfSupermarketListNewSupermarket = em.merge(oldEnterpriseidOfSupermarketListNewSupermarket);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = enterprise.getEnterpriseid();
                if (findEnterprise(id) == null) {
                    throw new NonexistentEntityException("The enterprise with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Enterprise enterprise;
            try {
                enterprise = em.getReference(Enterprise.class, id);
                enterprise.getEnterpriseid();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The enterprise with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Supermarket> supermarketListOrphanCheck = enterprise.getSupermarketList();
            for (Supermarket supermarketListOrphanCheckSupermarket : supermarketListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Enterprise (" + enterprise + ") cannot be destroyed since the Supermarket " + supermarketListOrphanCheckSupermarket + " in its supermarketList field has a non-nullable enterpriseid field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<Galpon> galponList = enterprise.getGalponList();
            for (Galpon galponListGalpon : galponList) {
                galponListGalpon.setEnterpriseid(null);
                galponListGalpon = em.merge(galponListGalpon);
            }
            List<Customer> customerList = enterprise.getCustomerList();
            for (Customer customerListCustomer : customerList) {
                customerListCustomer.setEnterpriseid(null);
                customerListCustomer = em.merge(customerListCustomer);
            }
            em.remove(enterprise);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Enterprise> findEnterpriseEntities() {
        return findEnterpriseEntities(true, -1, -1);
    }

    public List<Enterprise> findEnterpriseEntities(int maxResults, int firstResult) {
        return findEnterpriseEntities(false, maxResults, firstResult);
    }

    private List<Enterprise> findEnterpriseEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Enterprise.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Enterprise findEnterprise(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Enterprise.class, id);
        } finally {
            em.close();
        }
    }

    public int getEnterpriseCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Enterprise> rt = cq.from(Enterprise.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
