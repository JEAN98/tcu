/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import controllers.exceptions.NonexistentEntityException;
import entities.Gallinaza;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import entities.Galpon;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Jean Carlo
 */
public class GallinazaJpaController implements Serializable {

    public GallinazaJpaController() {
        this.emf = Persistence.createEntityManagerFactory("AviPU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Gallinaza gallinaza) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Galpon galponid = gallinaza.getGalponid();
            if (galponid != null) {
                galponid = em.getReference(galponid.getClass(), galponid.getGalponid());
                gallinaza.setGalponid(galponid);
            }
            em.persist(gallinaza);
            if (galponid != null) {
                galponid.getGallinazaList().add(gallinaza);
                galponid = em.merge(galponid);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Gallinaza gallinaza) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Gallinaza persistentGallinaza = em.find(Gallinaza.class, gallinaza.getGallinazaid());
            Galpon galponidOld = persistentGallinaza.getGalponid();
            Galpon galponidNew = gallinaza.getGalponid();
            if (galponidNew != null) {
                galponidNew = em.getReference(galponidNew.getClass(), galponidNew.getGalponid());
                gallinaza.setGalponid(galponidNew);
            }
            gallinaza = em.merge(gallinaza);
            if (galponidOld != null && !galponidOld.equals(galponidNew)) {
                galponidOld.getGallinazaList().remove(gallinaza);
                galponidOld = em.merge(galponidOld);
            }
            if (galponidNew != null && !galponidNew.equals(galponidOld)) {
                galponidNew.getGallinazaList().add(gallinaza);
                galponidNew = em.merge(galponidNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = gallinaza.getGallinazaid();
                if (findGallinaza(id) == null) {
                    throw new NonexistentEntityException("The gallinaza with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Gallinaza gallinaza;
            try {
                gallinaza = em.getReference(Gallinaza.class, id);
                gallinaza.getGallinazaid();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The gallinaza with id " + id + " no longer exists.", enfe);
            }
            Galpon galponid = gallinaza.getGalponid();
            if (galponid != null) {
                galponid.getGallinazaList().remove(gallinaza);
                galponid = em.merge(galponid);
            }
            em.remove(gallinaza);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Gallinaza> findGallinazaEntities() {
        return findGallinazaEntities(true, -1, -1);
    }

    public List<Gallinaza> findGallinazaEntities(int maxResults, int firstResult) {
        return findGallinazaEntities(false, maxResults, firstResult);
    }

    private List<Gallinaza> findGallinazaEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Gallinaza.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Gallinaza findGallinaza(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Gallinaza.class, id);
        } finally {
            em.close();
        }
    }

    public int getGallinazaCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Gallinaza> rt = cq.from(Gallinaza.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
