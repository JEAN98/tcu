/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import controllers.exceptions.NonexistentEntityException;
import entities.Mortality;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import entities.Parvada;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Jean Carlo
 */
public class MortalityJpaController implements Serializable {

    public MortalityJpaController() {
        this.emf = Persistence.createEntityManagerFactory("AviPU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Mortality mortality) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Parvada parvadaid = mortality.getParvadaid();
            if (parvadaid != null) {
                parvadaid = em.getReference(parvadaid.getClass(), parvadaid.getParvadaid());
                mortality.setParvadaid(parvadaid);
            }
            em.persist(mortality);
            if (parvadaid != null) {
                parvadaid.getMortalityList().add(mortality);
                parvadaid = em.merge(parvadaid);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Mortality mortality) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Mortality persistentMortality = em.find(Mortality.class, mortality.getMortalityid());
            Parvada parvadaidOld = persistentMortality.getParvadaid();
            Parvada parvadaidNew = mortality.getParvadaid();
            if (parvadaidNew != null) {
                parvadaidNew = em.getReference(parvadaidNew.getClass(), parvadaidNew.getParvadaid());
                mortality.setParvadaid(parvadaidNew);
            }
            mortality = em.merge(mortality);
            if (parvadaidOld != null && !parvadaidOld.equals(parvadaidNew)) {
                parvadaidOld.getMortalityList().remove(mortality);
                parvadaidOld = em.merge(parvadaidOld);
            }
            if (parvadaidNew != null && !parvadaidNew.equals(parvadaidOld)) {
                parvadaidNew.getMortalityList().add(mortality);
                parvadaidNew = em.merge(parvadaidNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = mortality.getMortalityid();
                if (findMortality(id) == null) {
                    throw new NonexistentEntityException("The mortality with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Mortality mortality;
            try {
                mortality = em.getReference(Mortality.class, id);
                mortality.getMortalityid();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The mortality with id " + id + " no longer exists.", enfe);
            }
            Parvada parvadaid = mortality.getParvadaid();
            if (parvadaid != null) {
                parvadaid.getMortalityList().remove(mortality);
                parvadaid = em.merge(parvadaid);
            }
            em.remove(mortality);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Mortality> findMortalityEntities() {
        return findMortalityEntities(true, -1, -1);
    }

    public List<Mortality> findMortalityEntities(int maxResults, int firstResult) {
        return findMortalityEntities(false, maxResults, firstResult);
    }

    private List<Mortality> findMortalityEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Mortality.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Mortality findMortality(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Mortality.class, id);
        } finally {
            em.close();
        }
    }

    public int getMortalityCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Mortality> rt = cq.from(Mortality.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
