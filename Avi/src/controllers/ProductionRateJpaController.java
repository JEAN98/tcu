/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import controllers.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import entities.Parvada;
import entities.ProductionRate;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Jean Carlo
 */
public class ProductionRateJpaController implements Serializable {

    public ProductionRateJpaController() {
        this.emf = Persistence.createEntityManagerFactory("AviPU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(ProductionRate productionRate) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Parvada parvadaid = productionRate.getParvadaid();
            if (parvadaid != null) {
                parvadaid = em.getReference(parvadaid.getClass(), parvadaid.getParvadaid());
                productionRate.setParvadaid(parvadaid);
            }
            em.persist(productionRate);
            if (parvadaid != null) {
                parvadaid.getProductionRateList().add(productionRate);
                parvadaid = em.merge(parvadaid);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(ProductionRate productionRate) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            ProductionRate persistentProductionRate = em.find(ProductionRate.class, productionRate.getProductionrateid());
            Parvada parvadaidOld = persistentProductionRate.getParvadaid();
            Parvada parvadaidNew = productionRate.getParvadaid();
            if (parvadaidNew != null) {
                parvadaidNew = em.getReference(parvadaidNew.getClass(), parvadaidNew.getParvadaid());
                productionRate.setParvadaid(parvadaidNew);
            }
            productionRate = em.merge(productionRate);
            if (parvadaidOld != null && !parvadaidOld.equals(parvadaidNew)) {
                parvadaidOld.getProductionRateList().remove(productionRate);
                parvadaidOld = em.merge(parvadaidOld);
            }
            if (parvadaidNew != null && !parvadaidNew.equals(parvadaidOld)) {
                parvadaidNew.getProductionRateList().add(productionRate);
                parvadaidNew = em.merge(parvadaidNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = productionRate.getProductionrateid();
                if (findProductionRate(id) == null) {
                    throw new NonexistentEntityException("The productionRate with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            ProductionRate productionRate;
            try {
                productionRate = em.getReference(ProductionRate.class, id);
                productionRate.getProductionrateid();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The productionRate with id " + id + " no longer exists.", enfe);
            }
            Parvada parvadaid = productionRate.getParvadaid();
            if (parvadaid != null) {
                parvadaid.getProductionRateList().remove(productionRate);
                parvadaid = em.merge(parvadaid);
            }
            em.remove(productionRate);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<ProductionRate> findProductionRateEntities() {
        return findProductionRateEntities(true, -1, -1);
    }

    public List<ProductionRate> findProductionRateEntities(int maxResults, int firstResult) {
        return findProductionRateEntities(false, maxResults, firstResult);
    }

    private List<ProductionRate> findProductionRateEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(ProductionRate.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public ProductionRate findProductionRate(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(ProductionRate.class, id);
        } finally {
            em.close();
        }
    }

    public int getProductionRateCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<ProductionRate> rt = cq.from(ProductionRate.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
