/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author Jean Carlo
 */
@Entity
@Table(name = "customers")
@NamedQueries({
    @NamedQuery(name = "Customer.findAll", query = "SELECT c FROM Customer c")
    , @NamedQuery(name = "Customer.findByClientid", query = "SELECT c FROM Customer c WHERE c.clientid = :clientid")
    , @NamedQuery(name = "Customer.findByClientname", query = "SELECT c FROM Customer c WHERE c.clientname = :clientname")
    , @NamedQuery(name = "Customer.findByFirstlastname", query = "SELECT c FROM Customer c WHERE c.firstlastname = :firstlastname")
    , @NamedQuery(name = "Customer.findBySecondlastname", query = "SELECT c FROM Customer c WHERE c.secondlastname = :secondlastname")
    , @NamedQuery(name = "Customer.findByCellphonenumber", query = "SELECT c FROM Customer c WHERE c.cellphonenumber = :cellphonenumber")
    , @NamedQuery(name = "Customer.findByTelephonenumber", query = "SELECT c FROM Customer c WHERE c.telephonenumber = :telephonenumber")
    , @NamedQuery(name = "Customer.findByEggscartonsnormallybuy", query = "SELECT c FROM Customer c WHERE c.eggscartonsnormallybuy = :eggscartonsnormallybuy")
    , @NamedQuery(name = "Customer.findByCustomerstatus", query = "SELECT c FROM Customer c WHERE c.customerstatus = :customerstatus")})
public class Customer implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "clientid")
    private Integer clientid;
    @Basic(optional = false)
    @Column(name = "clientname")
    private String clientname;
    @Basic(optional = false)
    @Column(name = "firstlastname")
    private String firstlastname;
    @Basic(optional = false)
    @Column(name = "secondlastname")
    private String secondlastname;
    @Basic(optional = false)
    @Column(name = "cellphonenumber")
    private String cellphonenumber;
    @Basic(optional = false)
    @Column(name = "telephonenumber")
    private String telephonenumber;
    @Basic(optional = false)
    @Column(name = "eggscartonsnormallybuy")
    private int eggscartonsnormallybuy;
    @Basic(optional = false)
    @Column(name = "customerstatus")
    private boolean customerstatus;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "clientid")
    private List<SalesByCustomer> salesByCustomerList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "clientid")
    private List<SalesByCustomerGallinaza> salesByCustomerGallinazaList;
    @JoinColumn(name = "enterpriseid", referencedColumnName = "enterpriseid")
    @ManyToOne
    private Enterprise enterpriseid;

    public Customer() {
    }

    public Customer(Integer clientid) {
        this.clientid = clientid;
    }

    public Customer(Integer clientid, String clientname, String firstlastname, String secondlastname, String cellphonenumber, String telephonenumber, int eggscartonsnormallybuy, boolean customerstatus) {
        this.clientid = clientid;
        this.clientname = clientname;
        this.firstlastname = firstlastname;
        this.secondlastname = secondlastname;
        this.cellphonenumber = cellphonenumber;
        this.telephonenumber = telephonenumber;
        this.eggscartonsnormallybuy = eggscartonsnormallybuy;
        this.customerstatus = customerstatus;
    }

    public Integer getClientid() {
        return clientid;
    }

    public void setClientid(Integer clientid) {
        this.clientid = clientid;
    }

    public String getClientname() {
        return clientname;
    }

    public void setClientname(String clientname) {
        this.clientname = clientname;
    }

    public String getFirstlastname() {
        return firstlastname;
    }

    public void setFirstlastname(String firstlastname) {
        this.firstlastname = firstlastname;
    }

    public String getSecondlastname() {
        return secondlastname;
    }

    public void setSecondlastname(String secondlastname) {
        this.secondlastname = secondlastname;
    }

    public String getCellphonenumber() {
        return cellphonenumber;
    }

    public void setCellphonenumber(String cellphonenumber) {
        this.cellphonenumber = cellphonenumber;
    }

    public String getTelephonenumber() {
        return telephonenumber;
    }

    public void setTelephonenumber(String telephonenumber) {
        this.telephonenumber = telephonenumber;
    }

    public int getEggscartonsnormallybuy() {
        return eggscartonsnormallybuy;
    }

    public void setEggscartonsnormallybuy(int eggscartonsnormallybuy) {
        this.eggscartonsnormallybuy = eggscartonsnormallybuy;
    }

    public boolean getCustomerstatus() {
        return customerstatus;
    }

    public void setCustomerstatus(boolean customerstatus) {
        this.customerstatus = customerstatus;
    }

    public List<SalesByCustomer> getSalesByCustomerList() {
        return salesByCustomerList;
    }

    public void setSalesByCustomerList(List<SalesByCustomer> salesByCustomerList) {
        this.salesByCustomerList = salesByCustomerList;
    }

    public List<SalesByCustomerGallinaza> getSalesByCustomerGallinazaList() {
        return salesByCustomerGallinazaList;
    }

    public void setSalesByCustomerGallinazaList(List<SalesByCustomerGallinaza> salesByCustomerGallinazaList) {
        this.salesByCustomerGallinazaList = salesByCustomerGallinazaList;
    }

    public Enterprise getEnterpriseid() {
        return enterpriseid;
    }

    public void setEnterpriseid(Enterprise enterpriseid) {
        this.enterpriseid = enterpriseid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (clientid != null ? clientid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Customer)) {
            return false;
        }
        Customer other = (Customer) object;
        if ((this.clientid == null && other.clientid != null) || (this.clientid != null && !this.clientid.equals(other.clientid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Customer[ clientid=" + clientid + " ]";
    }
    
}
