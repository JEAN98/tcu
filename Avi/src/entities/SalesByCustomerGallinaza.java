/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Jean Carlo
 */
@Entity
@Table(name = "salesbycustomergallinaza")
@NamedQueries({
    @NamedQuery(name = "SalesByCustomerGallinaza.findAll", query = "SELECT s FROM SalesByCustomerGallinaza s")
    , @NamedQuery(name = "SalesByCustomerGallinaza.findBySalesbyclientgallinazaid", query = "SELECT s FROM SalesByCustomerGallinaza s WHERE s.salesbyclientgallinazaid = :salesbyclientgallinazaid")
    , @NamedQuery(name = "SalesByCustomerGallinaza.findByBagsquantity", query = "SELECT s FROM SalesByCustomerGallinaza s WHERE s.bagsquantity = :bagsquantity")
    , @NamedQuery(name = "SalesByCustomerGallinaza.findByPricebybag", query = "SELECT s FROM SalesByCustomerGallinaza s WHERE s.pricebybag = :pricebybag")
    , @NamedQuery(name = "SalesByCustomerGallinaza.findByTotalsold", query = "SELECT s FROM SalesByCustomerGallinaza s WHERE s.totalsold = :totalsold")
    , @NamedQuery(name = "SalesByCustomerGallinaza.findByTrasctiondate", query = "SELECT s FROM SalesByCustomerGallinaza s WHERE s.trasctiondate = :trasctiondate")
    , @NamedQuery(name = "SalesByCustomerGallinaza.findByPaymenttype", query = "SELECT s FROM SalesByCustomerGallinaza s WHERE s.paymenttype = :paymenttype")})
public class SalesByCustomerGallinaza implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "salesbyclientgallinazaid")
    private Integer salesbyclientgallinazaid;
    @Basic(optional = false)
    @Column(name = "bagsquantity")
    private int bagsquantity;
    @Basic(optional = false)
    @Column(name = "pricebybag")
    private float pricebybag;
    @Basic(optional = false)
    @Column(name = "totalsold")
    private float totalsold;
    @Column(name = "trasctiondate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date trasctiondate;
    @Basic(optional = false)
    @Column(name = "paymenttype")
    private String paymenttype;
    @JoinColumn(name = "clientid", referencedColumnName = "clientid")
    @ManyToOne(optional = false)
    private Customer clientid;

    public SalesByCustomerGallinaza() {
    }

    public SalesByCustomerGallinaza(Integer salesbyclientgallinazaid) {
        this.salesbyclientgallinazaid = salesbyclientgallinazaid;
    }

    public SalesByCustomerGallinaza(Integer salesbyclientgallinazaid, int bagsquantity, float pricebybag, float totalsold, String paymenttype) {
        this.salesbyclientgallinazaid = salesbyclientgallinazaid;
        this.bagsquantity = bagsquantity;
        this.pricebybag = pricebybag;
        this.totalsold = totalsold;
        this.paymenttype = paymenttype;
    }

    public Integer getSalesbyclientgallinazaid() {
        return salesbyclientgallinazaid;
    }

    public void setSalesbyclientgallinazaid(Integer salesbyclientgallinazaid) {
        this.salesbyclientgallinazaid = salesbyclientgallinazaid;
    }

    public int getBagsquantity() {
        return bagsquantity;
    }

    public void setBagsquantity(int bagsquantity) {
        this.bagsquantity = bagsquantity;
    }

    public float getPricebybag() {
        return pricebybag;
    }

    public void setPricebybag(float pricebybag) {
        this.pricebybag = pricebybag;
    }

    public float getTotalsold() {
        return totalsold;
    }

    public void setTotalsold(float totalsold) {
        this.totalsold = totalsold;
    }

    public Date getTrasctiondate() {
        return trasctiondate;
    }

    public void setTrasctiondate(Date trasctiondate) {
        this.trasctiondate = trasctiondate;
    }

    public String getPaymenttype() {
        return paymenttype;
    }

    public void setPaymenttype(String paymenttype) {
        this.paymenttype = paymenttype;
    }

    public Customer getClientid() {
        return clientid;
    }

    public void setClientid(Customer clientid) {
        this.clientid = clientid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (salesbyclientgallinazaid != null ? salesbyclientgallinazaid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SalesByCustomerGallinaza)) {
            return false;
        }
        SalesByCustomerGallinaza other = (SalesByCustomerGallinaza) object;
        if ((this.salesbyclientgallinazaid == null && other.salesbyclientgallinazaid != null) || (this.salesbyclientgallinazaid != null && !this.salesbyclientgallinazaid.equals(other.salesbyclientgallinazaid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.SalesByCustomerGallinaza[ salesbyclientgallinazaid=" + salesbyclientgallinazaid + " ]";
    }
    
}
