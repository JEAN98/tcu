 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 *
 * @author Jean Carlo
 */
@Entity
@Table(name = "galpons")
@NamedQueries({
    @NamedQuery(name = "Galpon.findAll", query = "SELECT g FROM Galpon g")
    , @NamedQuery(name = "Galpon.findByGalponid", query = "SELECT g FROM Galpon g WHERE g.galponid = :galponid")
    , @NamedQuery(name = "Galpon.findByGalponcode", query = "SELECT g FROM Galpon g WHERE g.galponcode = :galponcode")
    , @NamedQuery(name = "Galpon.findByMeterslarge", query = "SELECT g FROM Galpon g WHERE g.meterslarge = :meterslarge")
    , @NamedQuery(name = "Galpon.findByMeterswidth", query = "SELECT g FROM Galpon g WHERE g.meterswidth = :meterswidth")
    , @NamedQuery(name = "Galpon.findByMaximumchickens", query = "SELECT g FROM Galpon g WHERE g.maximumchickens = :maximumchickens")
    , @NamedQuery(name = "Galpon.findByConstructiondate", query = "SELECT g FROM Galpon g WHERE g.constructiondate = :constructiondate")})
public class Galpon implements Serializable {

    @Transient
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "galponid")
    private Integer galponid;
    @Basic(optional = false)
    @Column(name = "galponcode")
    private String galponcode;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "meterslarge")
    private Float meterslarge;
    @Column(name = "meterswidth")
    private Float meterswidth;
    @Basic(optional = false)
    @Column(name = "maximumchickens")
    private int maximumchickens;
    @Column(name = "constructiondate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date constructiondate;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "galponid")
    private List<Gallinaza> gallinazaList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "galponid")
    private List<GalponInventory> galponInventoryList;
    @JoinColumn(name = "enterpriseid", referencedColumnName = "enterpriseid")
    @ManyToOne
    private Enterprise enterpriseid;
    @OneToMany(mappedBy = "galponid")
    private List<Parvada> parvadaList;

    public Galpon() {
    }

    public Galpon(Integer galponid) {
        this.galponid = galponid;
    }

    public Galpon(Integer galponid, String galponcode, int maximumchickens) {
        this.galponid = galponid;
        this.galponcode = galponcode;
        this.maximumchickens = maximumchickens;
    }

    public Integer getGalponid() {
        return galponid;
    }

    public void setGalponid(Integer galponid) {
        Integer oldGalponid = this.galponid;
        this.galponid = galponid;
        changeSupport.firePropertyChange("galponid", oldGalponid, galponid);
    }

    public String getGalponcode() {
        return galponcode;
    }

    public void setGalponcode(String galponcode) {
        String oldGalponcode = this.galponcode;
        this.galponcode = galponcode;
        changeSupport.firePropertyChange("galponcode", oldGalponcode, galponcode);
    }

    public Float getMeterslarge() {
        return meterslarge;
    }

    public void setMeterslarge(Float meterslarge) {
        Float oldMeterslarge = this.meterslarge;
        this.meterslarge = meterslarge;
        changeSupport.firePropertyChange("meterslarge", oldMeterslarge, meterslarge);
    }

    public Float getMeterswidth() {
        return meterswidth;
    }

    public void setMeterswidth(Float meterswidth) {
        Float oldMeterswidth = this.meterswidth;
        this.meterswidth = meterswidth;
        changeSupport.firePropertyChange("meterswidth", oldMeterswidth, meterswidth);
    }

    public int getMaximumchickens() {
        return maximumchickens;
    }

    public void setMaximumchickens(int maximumchickens) {
        int oldMaximumchickens = this.maximumchickens;
        this.maximumchickens = maximumchickens;
        changeSupport.firePropertyChange("maximumchickens", oldMaximumchickens, maximumchickens);
    }

    public Date getConstructiondate() {
        return constructiondate;
    }

    public void setConstructiondate(Date constructiondate) {
        Date oldConstructiondate = this.constructiondate;
        this.constructiondate = constructiondate;
        changeSupport.firePropertyChange("constructiondate", oldConstructiondate, constructiondate);
    }

    public List<Gallinaza> getGallinazaList() {
        return gallinazaList;
    }

    public void setGallinazaList(List<Gallinaza> gallinazaList) {
        this.gallinazaList = gallinazaList;
    }

    public List<GalponInventory> getGalponInventoryList() {
        return galponInventoryList;
    }

    public void setGalponInventoryList(List<GalponInventory> galponInventoryList) {
        this.galponInventoryList = galponInventoryList;
    }

    public Enterprise getEnterpriseid() {
        return enterpriseid;
    }

    public void setEnterpriseid(Enterprise enterpriseid) {
        Enterprise oldEnterpriseid = this.enterpriseid;
        this.enterpriseid = enterpriseid;
        changeSupport.firePropertyChange("enterpriseid", oldEnterpriseid, enterpriseid);
    }

    public List<Parvada> getParvadaList() {
        return parvadaList;
    }

    public void setParvadaList(List<Parvada> parvadaList) {
        this.parvadaList = parvadaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (galponid != null ? galponid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Galpon)) {
            return false;
        }
        Galpon other = (Galpon) object;
        if ((this.galponid == null && other.galponid != null) || (this.galponid != null && !this.galponid.equals(other.galponid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Galpon[ galponid=" + galponid + " ]";
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }
    
}
