/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Jean Carlo
 */
@Entity
@Table(name = "medicineapplications")
@NamedQueries({
    @NamedQuery(name = "MedicineApplication.findAll", query = "SELECT m FROM MedicineApplication m")
    , @NamedQuery(name = "MedicineApplication.findByMedicineapplicationid", query = "SELECT m FROM MedicineApplication m WHERE m.medicineapplicationid = :medicineapplicationid")
    , @NamedQuery(name = "MedicineApplication.findByDatetimeapplication", query = "SELECT m FROM MedicineApplication m WHERE m.datetimeapplication = :datetimeapplication")
    , @NamedQuery(name = "MedicineApplication.findByMedicinename", query = "SELECT m FROM MedicineApplication m WHERE m.medicinename = :medicinename")
    , @NamedQuery(name = "MedicineApplication.findByDescription", query = "SELECT m FROM MedicineApplication m WHERE m.description = :description")})
public class MedicineApplication implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "medicineapplicationid")
    private Integer medicineapplicationid;
    @Basic(optional = false)
    @Column(name = "datetimeapplication")
    @Temporal(TemporalType.TIMESTAMP)
    private Date datetimeapplication;
    @Basic(optional = false)
    @Column(name = "medicinename")
    private String medicinename;
    @Basic(optional = false)
    @Column(name = "description")
    private String description;
    @JoinColumn(name = "parvadaid", referencedColumnName = "parvadaid")
    @ManyToOne(optional = false)
    private Parvada parvadaid;

    public MedicineApplication() {
    }

    public MedicineApplication(Integer medicineapplicationid) {
        this.medicineapplicationid = medicineapplicationid;
    }

    public MedicineApplication(Integer medicineapplicationid, Date datetimeapplication, String medicinename, String description) {
        this.medicineapplicationid = medicineapplicationid;
        this.datetimeapplication = datetimeapplication;
        this.medicinename = medicinename;
        this.description = description;
    }

    public Integer getMedicineapplicationid() {
        return medicineapplicationid;
    }

    public void setMedicineapplicationid(Integer medicineapplicationid) {
        this.medicineapplicationid = medicineapplicationid;
    }

    public Date getDatetimeapplication() {
        return datetimeapplication;
    }

    public void setDatetimeapplication(Date datetimeapplication) {
        this.datetimeapplication = datetimeapplication;
    }

    public String getMedicinename() {
        return medicinename;
    }

    public void setMedicinename(String medicinename) {
        this.medicinename = medicinename;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Parvada getParvadaid() {
        return parvadaid;
    }

    public void setParvadaid(Parvada parvadaid) {
        this.parvadaid = parvadaid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (medicineapplicationid != null ? medicineapplicationid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MedicineApplication)) {
            return false;
        }
        MedicineApplication other = (MedicineApplication) object;
        if ((this.medicineapplicationid == null && other.medicineapplicationid != null) || (this.medicineapplicationid != null && !this.medicineapplicationid.equals(other.medicineapplicationid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.MedicineApplication[ medicineapplicationid=" + medicineapplicationid + " ]";
    }
    
}
