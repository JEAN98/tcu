/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Jean Carlo
 */
@Entity
@Table(name = "parvadas")
@NamedQueries({
    @NamedQuery(name = "Parvada.findAll", query = "SELECT p FROM Parvada p")
    , @NamedQuery(name = "Parvada.findByParvadaid", query = "SELECT p FROM Parvada p WHERE p.parvadaid = :parvadaid")
    , @NamedQuery(name = "Parvada.findByAdmissiondate", query = "SELECT p FROM Parvada p WHERE p.admissiondate = :admissiondate")
    , @NamedQuery(name = "Parvada.findByEntryage", query = "SELECT p FROM Parvada p WHERE p.entryage = :entryage")
    , @NamedQuery(name = "Parvada.findByRace", query = "SELECT p FROM Parvada p WHERE p.race = :race")
    , @NamedQuery(name = "Parvada.findByBeginningposture", query = "SELECT p FROM Parvada p WHERE p.beginningposture = :beginningposture")
    , @NamedQuery(name = "Parvada.findByPostureage", query = "SELECT p FROM Parvada p WHERE p.postureage = :postureage")
    , @NamedQuery(name = "Parvada.findByStartquantitychickens", query = "SELECT p FROM Parvada p WHERE p.startquantitychickens = :startquantitychickens")
    , @NamedQuery(name = "Parvada.findByParvadastatus", query = "SELECT p FROM Parvada p WHERE p.parvadastatus = :parvadastatus")})
public class Parvada implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "parvadaid")
    private Integer parvadaid;
    @Basic(optional = false)
    @Column(name = "admissiondate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date admissiondate;
    @Basic(optional = false)
    @Column(name = "entryage")
    private String entryage;
    @Basic(optional = false)
    @Column(name = "race")
    private String race;
    @Column(name = "beginningposture")
    @Temporal(TemporalType.TIMESTAMP)
    private Date beginningposture;
    @Basic(optional = false)
    @Column(name = "postureage")
    private String postureage;
    @Basic(optional = false)
    @Column(name = "startquantitychickens")
    private int startquantitychickens;
    @Column(name = "parvadastatus")
    private Boolean parvadastatus;
    @OneToMany(mappedBy = "parvadaid")
    private List<Mortality> mortalityList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "parvadaid")
    private List<ParvadaFood> parvadaFoodList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "parvadaid")
    private List<ProductionRate> productionRateList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "parvadaid")
    private List<Temperature> temperatureList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "parvadaid")
    private List<MedicineApplication> medicineApplicationList;
    @JoinColumn(name = "galponid", referencedColumnName = "galponid")
    @ManyToOne
    private Galpon galponid;

    public Parvada() {
    }

    public Parvada(Integer parvadaid) {
        this.parvadaid = parvadaid;
    }

    public Parvada(Integer parvadaid, Date admissiondate, String entryage, String race, String postureage, int startquantitychickens) {
        this.parvadaid = parvadaid;
        this.admissiondate = admissiondate;
        this.entryage = entryage;
        this.race = race;
        this.postureage = postureage;
        this.startquantitychickens = startquantitychickens;
    }

    public Integer getParvadaid() {
        return parvadaid;
    }

    public void setParvadaid(Integer parvadaid) {
        this.parvadaid = parvadaid;
    }

    public Date getAdmissiondate() {
        return admissiondate;
    }

    public void setAdmissiondate(Date admissiondate) {
        this.admissiondate = admissiondate;
    }

    public String getEntryage() {
        return entryage;
    }

    public void setEntryage(String entryage) {
        this.entryage = entryage;
    }

    public String getRace() {
        return race;
    }

    public void setRace(String race) {
        this.race = race;
    }

    public Date getBeginningposture() {
        return beginningposture;
    }

    public void setBeginningposture(Date beginningposture) {
        this.beginningposture = beginningposture;
    }

    public String getPostureage() {
        return postureage;
    }

    public void setPostureage(String postureage) {
        this.postureage = postureage;
    }

    public int getStartquantitychickens() {
        return startquantitychickens;
    }

    public void setStartquantitychickens(int startquantitychickens) {
        this.startquantitychickens = startquantitychickens;
    }

    public Boolean getParvadastatus() {
        return parvadastatus;
    }

    public void setParvadastatus(Boolean parvadastatus) {
        this.parvadastatus = parvadastatus;
    }

    public List<Mortality> getMortalityList() {
        return mortalityList;
    }

    public void setMortalityList(List<Mortality> mortalityList) {
        this.mortalityList = mortalityList;
    }

    public List<ParvadaFood> getParvadaFoodList() {
        return parvadaFoodList;
    }

    public void setParvadaFoodList(List<ParvadaFood> parvadaFoodList) {
        this.parvadaFoodList = parvadaFoodList;
    }

    public List<ProductionRate> getProductionRateList() {
        return productionRateList;
    }

    public void setProductionRateList(List<ProductionRate> productionRateList) {
        this.productionRateList = productionRateList;
    }

    public List<Temperature> getTemperatureList() {
        return temperatureList;
    }

    public void setTemperatureList(List<Temperature> temperatureList) {
        this.temperatureList = temperatureList;
    }

    public List<MedicineApplication> getMedicineApplicationList() {
        return medicineApplicationList;
    }

    public void setMedicineApplicationList(List<MedicineApplication> medicineApplicationList) {
        this.medicineApplicationList = medicineApplicationList;
    }

    public Galpon getGalponid() {
        return galponid;
    }

    public void setGalponid(Galpon galponid) {
        this.galponid = galponid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (parvadaid != null ? parvadaid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Parvada)) {
            return false;
        }
        Parvada other = (Parvada) object;
        if ((this.parvadaid == null && other.parvadaid != null) || (this.parvadaid != null && !this.parvadaid.equals(other.parvadaid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Parvada[ parvadaid=" + parvadaid + " ]";
    }
    
}
