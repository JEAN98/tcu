/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author Jean Carlo
 */
@Entity
@Table(name = "salesbysupermarket")
@NamedQueries({
    @NamedQuery(name = "SalesBySupermarket.findAll", query = "SELECT s FROM SalesBySupermarket s")
    , @NamedQuery(name = "SalesBySupermarket.findBySalesbysupermarketid", query = "SELECT s FROM SalesBySupermarket s WHERE s.salesbysupermarketid = :salesbysupermarketid")})
public class SalesBySupermarket implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "salesbysupermarketid")
    private Integer salesbysupermarketid;
    @JoinColumn(name = "salesid", referencedColumnName = "salesid")
    @ManyToOne(optional = false)
    private Sales salesid;
    @JoinColumn(name = "supermarketid", referencedColumnName = "supermarketid")
    @ManyToOne(optional = false)
    private Supermarket supermarketid;

    public SalesBySupermarket() {
    }

    public SalesBySupermarket(Integer salesbysupermarketid) {
        this.salesbysupermarketid = salesbysupermarketid;
    }

    public Integer getSalesbysupermarketid() {
        return salesbysupermarketid;
    }

    public void setSalesbysupermarketid(Integer salesbysupermarketid) {
        this.salesbysupermarketid = salesbysupermarketid;
    }

    public Sales getSalesid() {
        return salesid;
    }

    public void setSalesid(Sales salesid) {
        this.salesid = salesid;
    }

    public Supermarket getSupermarketid() {
        return supermarketid;
    }

    public void setSupermarketid(Supermarket supermarketid) {
        this.supermarketid = supermarketid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (salesbysupermarketid != null ? salesbysupermarketid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SalesBySupermarket)) {
            return false;
        }
        SalesBySupermarket other = (SalesBySupermarket) object;
        if ((this.salesbysupermarketid == null && other.salesbysupermarketid != null) || (this.salesbysupermarketid != null && !this.salesbysupermarketid.equals(other.salesbysupermarketid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.SalesBySupermarket[ salesbysupermarketid=" + salesbysupermarketid + " ]";
    }
    
}
