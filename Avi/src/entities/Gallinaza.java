/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Jean Carlo
 */
@Entity
@Table(name = "gallinaza")
@NamedQueries({
    @NamedQuery(name = "Gallinaza.findAll", query = "SELECT g FROM Gallinaza g")
    , @NamedQuery(name = "Gallinaza.findByGallinazaid", query = "SELECT g FROM Gallinaza g WHERE g.gallinazaid = :gallinazaid")
    , @NamedQuery(name = "Gallinaza.findByBagsquantity", query = "SELECT g FROM Gallinaza g WHERE g.bagsquantity = :bagsquantity")
    , @NamedQuery(name = "Gallinaza.findByPricebybag", query = "SELECT g FROM Gallinaza g WHERE g.pricebybag = :pricebybag")
    , @NamedQuery(name = "Gallinaza.findByCollectiondate", query = "SELECT g FROM Gallinaza g WHERE g.collectiondate = :collectiondate")})
public class Gallinaza implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "gallinazaid")
    private Integer gallinazaid;
    @Basic(optional = false)
    @Column(name = "bagsquantity")
    private int bagsquantity;
    @Basic(optional = false)
    @Column(name = "pricebybag")
    private float pricebybag;
    @Column(name = "collectiondate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date collectiondate;
    @JoinColumn(name = "galponid", referencedColumnName = "galponid")
    @ManyToOne(optional = false)
    private Galpon galponid;

    public Gallinaza() {
    }

    public Gallinaza(Integer gallinazaid) {
        this.gallinazaid = gallinazaid;
    }

    public Gallinaza(Integer gallinazaid, int bagsquantity, float pricebybag) {
        this.gallinazaid = gallinazaid;
        this.bagsquantity = bagsquantity;
        this.pricebybag = pricebybag;
    }

    public Integer getGallinazaid() {
        return gallinazaid;
    }

    public void setGallinazaid(Integer gallinazaid) {
        this.gallinazaid = gallinazaid;
    }

    public int getBagsquantity() {
        return bagsquantity;
    }

    public void setBagsquantity(int bagsquantity) {
        this.bagsquantity = bagsquantity;
    }

    public float getPricebybag() {
        return pricebybag;
    }

    public void setPricebybag(float pricebybag) {
        this.pricebybag = pricebybag;
    }

    public Date getCollectiondate() {
        return collectiondate;
    }

    public void setCollectiondate(Date collectiondate) {
        this.collectiondate = collectiondate;
    }

    public Galpon getGalponid() {
        return galponid;
    }

    public void setGalponid(Galpon galponid) {
        this.galponid = galponid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (gallinazaid != null ? gallinazaid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Gallinaza)) {
            return false;
        }
        Gallinaza other = (Gallinaza) object;
        if ((this.gallinazaid == null && other.gallinazaid != null) || (this.gallinazaid != null && !this.gallinazaid.equals(other.gallinazaid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Gallinaza[ gallinazaid=" + gallinazaid + " ]";
    }
    
}
