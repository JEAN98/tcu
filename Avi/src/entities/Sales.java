/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Jean Carlo
 */
@Entity
@Table(name = "sales")
@NamedQueries({
    @NamedQuery(name = "Sales.findAll", query = "SELECT s FROM Sales s")
    , @NamedQuery(name = "Sales.findBySalesid", query = "SELECT s FROM Sales s WHERE s.salesid = :salesid")
    , @NamedQuery(name = "Sales.findByKilogramstotal", query = "SELECT s FROM Sales s WHERE s.kilogramstotal = :kilogramstotal")
    , @NamedQuery(name = "Sales.findByPricebykilograms", query = "SELECT s FROM Sales s WHERE s.pricebykilograms = :pricebykilograms")
    , @NamedQuery(name = "Sales.findByTotalsold", query = "SELECT s FROM Sales s WHERE s.totalsold = :totalsold")
    , @NamedQuery(name = "Sales.findByTrasctiondate", query = "SELECT s FROM Sales s WHERE s.trasctiondate = :trasctiondate")
    , @NamedQuery(name = "Sales.findByPaymenttype", query = "SELECT s FROM Sales s WHERE s.paymenttype = :paymenttype")})
public class Sales implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "salesid")
    private Integer salesid;
    @Basic(optional = false)
    @Column(name = "kilogramstotal")
    private float kilogramstotal;
    @Basic(optional = false)
    @Column(name = "pricebykilograms")
    private float pricebykilograms;
    @Basic(optional = false)
    @Column(name = "totalsold")
    private float totalsold;
    @Basic(optional = false)
    @Column(name = "trasctiondate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date trasctiondate;
    @Basic(optional = false)
    @Column(name = "paymenttype")
    private String paymenttype;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "salesid")
    private List<SalesByCustomer> salesByCustomerList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "salesid")
    private List<SalesBySupermarket> salesBySupermarketList;

    public Sales() {
    }

    public Sales(Integer salesid) {
        this.salesid = salesid;
    }

    public Sales(Integer salesid, float kilogramstotal, float pricebykilograms, float totalsold, Date trasctiondate, String paymenttype) {
        this.salesid = salesid;
        this.kilogramstotal = kilogramstotal;
        this.pricebykilograms = pricebykilograms;
        this.totalsold = totalsold;
        this.trasctiondate = trasctiondate;
        this.paymenttype = paymenttype;
    }

    public Integer getSalesid() {
        return salesid;
    }

    public void setSalesid(Integer salesid) {
        this.salesid = salesid;
    }

    public float getKilogramstotal() {
        return kilogramstotal;
    }

    public void setKilogramstotal(float kilogramstotal) {
        this.kilogramstotal = kilogramstotal;
    }

    public float getPricebykilograms() {
        return pricebykilograms;
    }

    public void setPricebykilograms(float pricebykilograms) {
        this.pricebykilograms = pricebykilograms;
    }

    public float getTotalsold() {
        return totalsold;
    }

    public void setTotalsold(float totalsold) {
        this.totalsold = totalsold;
    }

    public Date getTrasctiondate() {
        return trasctiondate;
    }

    public void setTrasctiondate(Date trasctiondate) {
        this.trasctiondate = trasctiondate;
    }

    public String getPaymenttype() {
        return paymenttype;
    }

    public void setPaymenttype(String paymenttype) {
        this.paymenttype = paymenttype;
    }

    public List<SalesByCustomer> getSalesByCustomerList() {
        return salesByCustomerList;
    }

    public void setSalesByCustomerList(List<SalesByCustomer> salesByCustomerList) {
        this.salesByCustomerList = salesByCustomerList;
    }

    public List<SalesBySupermarket> getSalesBySupermarketList() {
        return salesBySupermarketList;
    }

    public void setSalesBySupermarketList(List<SalesBySupermarket> salesBySupermarketList) {
        this.salesBySupermarketList = salesBySupermarketList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (salesid != null ? salesid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Sales)) {
            return false;
        }
        Sales other = (Sales) object;
        if ((this.salesid == null && other.salesid != null) || (this.salesid != null && !this.salesid.equals(other.salesid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Sales[ salesid=" + salesid + " ]";
    }
    
}
