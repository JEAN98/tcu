/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Jean Carlo
 */
@Entity
@Table(name = "temperatures")
@NamedQueries({
    @NamedQuery(name = "Temperature.findAll", query = "SELECT t FROM Temperature t")
    , @NamedQuery(name = "Temperature.findByTemperatureid", query = "SELECT t FROM Temperature t WHERE t.temperatureid = :temperatureid")
    , @NamedQuery(name = "Temperature.findByMeasurementdate", query = "SELECT t FROM Temperature t WHERE t.measurementdate = :measurementdate")
    , @NamedQuery(name = "Temperature.findByMeasurementhour", query = "SELECT t FROM Temperature t WHERE t.measurementhour = :measurementhour")
    , @NamedQuery(name = "Temperature.findByTemperatureobtained", query = "SELECT t FROM Temperature t WHERE t.temperatureobtained = :temperatureobtained")})
public class Temperature implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "temperatureid")
    private Integer temperatureid;
    @Column(name = "measurementdate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date measurementdate;
    @Basic(optional = false)
    @Column(name = "measurementhour")
    private String measurementhour;
    @Basic(optional = false)
    @Column(name = "temperatureobtained")
    private String temperatureobtained;
    @JoinColumn(name = "parvadaid", referencedColumnName = "parvadaid")
    @ManyToOne(optional = false)
    private Parvada parvadaid;

    public Temperature() {
    }

    public Temperature(Integer temperatureid) {
        this.temperatureid = temperatureid;
    }

    public Temperature(Integer temperatureid, String measurementhour, String temperatureobtained) {
        this.temperatureid = temperatureid;
        this.measurementhour = measurementhour;
        this.temperatureobtained = temperatureobtained;
    }

    public Integer getTemperatureid() {
        return temperatureid;
    }

    public void setTemperatureid(Integer temperatureid) {
        this.temperatureid = temperatureid;
    }

    public Date getMeasurementdate() {
        return measurementdate;
    }

    public void setMeasurementdate(Date measurementdate) {
        this.measurementdate = measurementdate;
    }

    public String getMeasurementhour() {
        return measurementhour;
    }

    public void setMeasurementhour(String measurementhour) {
        this.measurementhour = measurementhour;
    }

    public String getTemperatureobtained() {
        return temperatureobtained;
    }

    public void setTemperatureobtained(String temperatureobtained) {
        this.temperatureobtained = temperatureobtained;
    }

    public Parvada getParvadaid() {
        return parvadaid;
    }

    public void setParvadaid(Parvada parvadaid) {
        this.parvadaid = parvadaid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (temperatureid != null ? temperatureid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Temperature)) {
            return false;
        }
        Temperature other = (Temperature) object;
        if ((this.temperatureid == null && other.temperatureid != null) || (this.temperatureid != null && !this.temperatureid.equals(other.temperatureid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Temperature[ temperatureid=" + temperatureid + " ]";
    }
    
}
