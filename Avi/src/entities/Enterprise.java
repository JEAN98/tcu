/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Jean Carlo
 */
@Entity
@Table(name = "enterprises")
@NamedQueries({
    @NamedQuery(name = "Enterprise.findAll", query = "SELECT e FROM Enterprise e")
    , @NamedQuery(name = "Enterprise.findByEnterpriseid", query = "SELECT e FROM Enterprise e WHERE e.enterpriseid = :enterpriseid")
    , @NamedQuery(name = "Enterprise.findByEnterprisename", query = "SELECT e FROM Enterprise e WHERE e.enterprisename = :enterprisename")
    , @NamedQuery(name = "Enterprise.findByTelfonnumber", query = "SELECT e FROM Enterprise e WHERE e.telfonnumber = :telfonnumber")
    , @NamedQuery(name = "Enterprise.findByEmail", query = "SELECT e FROM Enterprise e WHERE e.email = :email")
    , @NamedQuery(name = "Enterprise.findByCreationdatetime", query = "SELECT e FROM Enterprise e WHERE e.creationdatetime = :creationdatetime")
    , @NamedQuery(name = "Enterprise.findByEnterpriseurl", query = "SELECT e FROM Enterprise e WHERE e.enterpriseurl = :enterpriseurl")})
public class Enterprise implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "enterpriseid")
    private Integer enterpriseid;
    @Column(name = "enterprisename")
    private String enterprisename;
    @Column(name = "telfonnumber")
    private String telfonnumber;
    @Column(name = "email")
    private String email;
    @Column(name = "creationdatetime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date creationdatetime;
    @Column(name = "enterpriseurl")
    private String enterpriseurl;
    @OneToMany(mappedBy = "enterpriseid")
    private List<User> userList;
    @OneToMany(mappedBy = "enterpriseid")
    private List<Galpon> galponList;
    @OneToMany(mappedBy = "enterpriseid")
    private List<Customer> customerList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "enterpriseid")
    private List<Supermarket> supermarketList;

    public Enterprise() {
    }

    public Enterprise(Integer enterpriseid) {
        this.enterpriseid = enterpriseid;
    }

    public Integer getEnterpriseid() {
        return enterpriseid;
    }

    public void setEnterpriseid(Integer enterpriseid) {
        this.enterpriseid = enterpriseid;
    }

    public String getEnterprisename() {
        return enterprisename;
    }

    public void setEnterprisename(String enterprisename) {
        this.enterprisename = enterprisename;
    }

    public String getTelfonnumber() {
        return telfonnumber;
    }

    public void setTelfonnumber(String telfonnumber) {
        this.telfonnumber = telfonnumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getCreationdatetime() {
        return creationdatetime;
    }

    public void setCreationdatetime(Date creationdatetime) {
        this.creationdatetime = creationdatetime;
    }

    public String getEnterpriseurl() {
        return enterpriseurl;
    }

    public void setEnterpriseurl(String enterpriseurl) {
        this.enterpriseurl = enterpriseurl;
    }

    public List<User> getUserList() {
        return userList;
    }

    public void setUserList(List<User> userList) {
        this.userList = userList;
    }

    public List<Galpon> getGalponList() {
        return galponList;
    }

    public void setGalponList(List<Galpon> galponList) {
        this.galponList = galponList;
    }

    public List<Customer> getCustomerList() {
        return customerList;
    }

    public void setCustomerList(List<Customer> customerList) {
        this.customerList = customerList;
    }

    public List<Supermarket> getSupermarketList() {
        return supermarketList;
    }

    public void setSupermarketList(List<Supermarket> supermarketList) {
        this.supermarketList = supermarketList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (enterpriseid != null ? enterpriseid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Enterprise)) {
            return false;
        }
        Enterprise other = (Enterprise) object;
        if ((this.enterpriseid == null && other.enterpriseid != null) || (this.enterpriseid != null && !this.enterpriseid.equals(other.enterpriseid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Enterprise[ enterpriseid=" + enterpriseid + " ]";
    }
    
}
