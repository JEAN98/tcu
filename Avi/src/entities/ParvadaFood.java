/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Jean Carlo
 */
@Entity
@Table(name = "parvadasfood")
@NamedQueries({
    @NamedQuery(name = "ParvadaFood.findAll", query = "SELECT p FROM ParvadaFood p")
    , @NamedQuery(name = "ParvadaFood.findByParvadasfoodid", query = "SELECT p FROM ParvadaFood p WHERE p.parvadasfoodid = :parvadasfoodid")
    , @NamedQuery(name = "ParvadaFood.findBySuppliedconcentratename", query = "SELECT p FROM ParvadaFood p WHERE p.suppliedconcentratename = :suppliedconcentratename")
    , @NamedQuery(name = "ParvadaFood.findBySuppliedquantity", query = "SELECT p FROM ParvadaFood p WHERE p.suppliedquantity = :suppliedquantity")
    , @NamedQuery(name = "ParvadaFood.findBySupplieddatetime", query = "SELECT p FROM ParvadaFood p WHERE p.supplieddatetime = :supplieddatetime")})
public class ParvadaFood implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "parvadasfoodid")
    private Integer parvadasfoodid;
    @Basic(optional = false)
    @Column(name = "suppliedconcentratename")
    private String suppliedconcentratename;
    @Basic(optional = false)
    @Column(name = "suppliedquantity")
    private float suppliedquantity;
    @Column(name = "supplieddatetime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date supplieddatetime;
    @JoinColumn(name = "parvadaid", referencedColumnName = "parvadaid")
    @ManyToOne(optional = false)
    private Parvada parvadaid;

    public ParvadaFood() {
    }

    public ParvadaFood(Integer parvadasfoodid) {
        this.parvadasfoodid = parvadasfoodid;
    }

    public ParvadaFood(Integer parvadasfoodid, String suppliedconcentratename, float suppliedquantity) {
        this.parvadasfoodid = parvadasfoodid;
        this.suppliedconcentratename = suppliedconcentratename;
        this.suppliedquantity = suppliedquantity;
    }

    public Integer getParvadasfoodid() {
        return parvadasfoodid;
    }

    public void setParvadasfoodid(Integer parvadasfoodid) {
        this.parvadasfoodid = parvadasfoodid;
    }

    public String getSuppliedconcentratename() {
        return suppliedconcentratename;
    }

    public void setSuppliedconcentratename(String suppliedconcentratename) {
        this.suppliedconcentratename = suppliedconcentratename;
    }

    public float getSuppliedquantity() {
        return suppliedquantity;
    }

    public void setSuppliedquantity(float suppliedquantity) {
        this.suppliedquantity = suppliedquantity;
    }

    public Date getSupplieddatetime() {
        return supplieddatetime;
    }

    public void setSupplieddatetime(Date supplieddatetime) {
        this.supplieddatetime = supplieddatetime;
    }

    public Parvada getParvadaid() {
        return parvadaid;
    }

    public void setParvadaid(Parvada parvadaid) {
        this.parvadaid = parvadaid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (parvadasfoodid != null ? parvadasfoodid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ParvadaFood)) {
            return false;
        }
        ParvadaFood other = (ParvadaFood) object;
        if ((this.parvadasfoodid == null && other.parvadasfoodid != null) || (this.parvadasfoodid != null && !this.parvadasfoodid.equals(other.parvadasfoodid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.ParvadaFood[ parvadasfoodid=" + parvadasfoodid + " ]";
    }
    
}
