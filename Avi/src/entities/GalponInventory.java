/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author Jean Carlo
 */
@Entity
@Table(name = "galponinventories")
@NamedQueries({
    @NamedQuery(name = "GalponInventory.findAll", query = "SELECT g FROM GalponInventory g")
    , @NamedQuery(name = "GalponInventory.findByGalponinventoryid", query = "SELECT g FROM GalponInventory g WHERE g.galponinventoryid = :galponinventoryid")
    , @NamedQuery(name = "GalponInventory.findByFeedersquantityactives", query = "SELECT g FROM GalponInventory g WHERE g.feedersquantityactives = :feedersquantityactives")
    , @NamedQuery(name = "GalponInventory.findByFeedersquantitydesactives", query = "SELECT g FROM GalponInventory g WHERE g.feedersquantitydesactives = :feedersquantitydesactives")
    , @NamedQuery(name = "GalponInventory.findByDrinkingfountainsquantityactives", query = "SELECT g FROM GalponInventory g WHERE g.drinkingfountainsquantityactives = :drinkingfountainsquantityactives")
    , @NamedQuery(name = "GalponInventory.findByDrinkingfountainsquantitydesactives", query = "SELECT g FROM GalponInventory g WHERE g.drinkingfountainsquantitydesactives = :drinkingfountainsquantitydesactives")
    , @NamedQuery(name = "GalponInventory.findByLightbulbsactives", query = "SELECT g FROM GalponInventory g WHERE g.lightbulbsactives = :lightbulbsactives")
    , @NamedQuery(name = "GalponInventory.findByLightbulbsdesactives", query = "SELECT g FROM GalponInventory g WHERE g.lightbulbsdesactives = :lightbulbsdesactives")
    , @NamedQuery(name = "GalponInventory.findByNestingboxesactive", query = "SELECT g FROM GalponInventory g WHERE g.nestingboxesactive = :nestingboxesactive")
    , @NamedQuery(name = "GalponInventory.findByNestingboxesdesactives", query = "SELECT g FROM GalponInventory g WHERE g.nestingboxesdesactives = :nestingboxesdesactives")})
public class GalponInventory implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "galponinventoryid")
    private Integer galponinventoryid;
    @Column(name = "feedersquantityactives")
    private Integer feedersquantityactives;
    @Column(name = "feedersquantitydesactives")
    private Integer feedersquantitydesactives;
    @Column(name = "drinkingfountainsquantityactives")
    private Integer drinkingfountainsquantityactives;
    @Column(name = "drinkingfountainsquantitydesactives")
    private Integer drinkingfountainsquantitydesactives;
    @Basic(optional = false)
    @Column(name = "lightbulbsactives")
    private int lightbulbsactives;
    @Basic(optional = false)
    @Column(name = "lightbulbsdesactives")
    private int lightbulbsdesactives;
    @Basic(optional = false)
    @Column(name = "nestingboxesactive")
    private int nestingboxesactive;
    @Basic(optional = false)
    @Column(name = "nestingboxesdesactives")
    private int nestingboxesdesactives;
    @JoinColumn(name = "galponid", referencedColumnName = "galponid")
    @ManyToOne(optional = false)
    private Galpon galponid;

    public GalponInventory() {
    }

    public GalponInventory(Integer galponinventoryid) {
        this.galponinventoryid = galponinventoryid;
    }

    public GalponInventory(Integer galponinventoryid, int lightbulbsactives, int lightbulbsdesactives, int nestingboxesactive, int nestingboxesdesactives) {
        this.galponinventoryid = galponinventoryid;
        this.lightbulbsactives = lightbulbsactives;
        this.lightbulbsdesactives = lightbulbsdesactives;
        this.nestingboxesactive = nestingboxesactive;
        this.nestingboxesdesactives = nestingboxesdesactives;
    }

    public Integer getGalponinventoryid() {
        return galponinventoryid;
    }

    public void setGalponinventoryid(Integer galponinventoryid) {
        this.galponinventoryid = galponinventoryid;
    }

    public Integer getFeedersquantityactives() {
        return feedersquantityactives;
    }

    public void setFeedersquantityactives(Integer feedersquantityactives) {
        this.feedersquantityactives = feedersquantityactives;
    }

    public Integer getFeedersquantitydesactives() {
        return feedersquantitydesactives;
    }

    public void setFeedersquantitydesactives(Integer feedersquantitydesactives) {
        this.feedersquantitydesactives = feedersquantitydesactives;
    }

    public Integer getDrinkingfountainsquantityactives() {
        return drinkingfountainsquantityactives;
    }

    public void setDrinkingfountainsquantityactives(Integer drinkingfountainsquantityactives) {
        this.drinkingfountainsquantityactives = drinkingfountainsquantityactives;
    }

    public Integer getDrinkingfountainsquantitydesactives() {
        return drinkingfountainsquantitydesactives;
    }

    public void setDrinkingfountainsquantitydesactives(Integer drinkingfountainsquantitydesactives) {
        this.drinkingfountainsquantitydesactives = drinkingfountainsquantitydesactives;
    }

    public int getLightbulbsactives() {
        return lightbulbsactives;
    }

    public void setLightbulbsactives(int lightbulbsactives) {
        this.lightbulbsactives = lightbulbsactives;
    }

    public int getLightbulbsdesactives() {
        return lightbulbsdesactives;
    }

    public void setLightbulbsdesactives(int lightbulbsdesactives) {
        this.lightbulbsdesactives = lightbulbsdesactives;
    }

    public int getNestingboxesactive() {
        return nestingboxesactive;
    }

    public void setNestingboxesactive(int nestingboxesactive) {
        this.nestingboxesactive = nestingboxesactive;
    }

    public int getNestingboxesdesactives() {
        return nestingboxesdesactives;
    }

    public void setNestingboxesdesactives(int nestingboxesdesactives) {
        this.nestingboxesdesactives = nestingboxesdesactives;
    }

    public Galpon getGalponid() {
        return galponid;
    }

    public void setGalponid(Galpon galponid) {
        this.galponid = galponid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (galponinventoryid != null ? galponinventoryid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GalponInventory)) {
            return false;
        }
        GalponInventory other = (GalponInventory) object;
        if ((this.galponinventoryid == null && other.galponinventoryid != null) || (this.galponinventoryid != null && !this.galponinventoryid.equals(other.galponinventoryid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.GalponInventory[ galponinventoryid=" + galponinventoryid + " ]";
    }
    
}
