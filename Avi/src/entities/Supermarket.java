/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author Jean Carlo
 */
@Entity
@Table(name = "supermarkets")
@NamedQueries({
    @NamedQuery(name = "Supermarket.findAll", query = "SELECT s FROM Supermarket s")
    , @NamedQuery(name = "Supermarket.findBySupermarketid", query = "SELECT s FROM Supermarket s WHERE s.supermarketid = :supermarketid")
    , @NamedQuery(name = "Supermarket.findBySupermarketname", query = "SELECT s FROM Supermarket s WHERE s.supermarketname = :supermarketname")
    , @NamedQuery(name = "Supermarket.findByAdministratorname", query = "SELECT s FROM Supermarket s WHERE s.administratorname = :administratorname")
    , @NamedQuery(name = "Supermarket.findByFirstlastname", query = "SELECT s FROM Supermarket s WHERE s.firstlastname = :firstlastname")
    , @NamedQuery(name = "Supermarket.findBySecondlastname", query = "SELECT s FROM Supermarket s WHERE s.secondlastname = :secondlastname")
    , @NamedQuery(name = "Supermarket.findByTelephonenumber", query = "SELECT s FROM Supermarket s WHERE s.telephonenumber = :telephonenumber")
    , @NamedQuery(name = "Supermarket.findByEggscartonsnormallybuy", query = "SELECT s FROM Supermarket s WHERE s.eggscartonsnormallybuy = :eggscartonsnormallybuy")
    , @NamedQuery(name = "Supermarket.findBySupermarketstatus", query = "SELECT s FROM Supermarket s WHERE s.supermarketstatus = :supermarketstatus")
    , @NamedQuery(name = "Supermarket.findBySupermarketlocation", query = "SELECT s FROM Supermarket s WHERE s.supermarketlocation = :supermarketlocation")})
public class Supermarket implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "supermarketid")
    private Integer supermarketid;
    @Basic(optional = false)
    @Column(name = "supermarketname")
    private String supermarketname;
    @Basic(optional = false)
    @Column(name = "administratorname")
    private String administratorname;
    @Basic(optional = false)
    @Column(name = "firstlastname")
    private String firstlastname;
    @Basic(optional = false)
    @Column(name = "secondlastname")
    private String secondlastname;
    @Basic(optional = false)
    @Column(name = "telephonenumber")
    private String telephonenumber;
    @Basic(optional = false)
    @Column(name = "eggscartonsnormallybuy")
    private int eggscartonsnormallybuy;
    @Basic(optional = false)
    @Column(name = "supermarketstatus")
    private boolean supermarketstatus;
    @Basic(optional = false)
    @Column(name = "supermarketlocation")
    private String supermarketlocation;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "supermarketid")
    private List<SalesBySupermarket> salesBySupermarketList;
    @JoinColumn(name = "enterpriseid", referencedColumnName = "enterpriseid")
    @ManyToOne(optional = false)
    private Enterprise enterpriseid;

    public Supermarket() {
    }

    public Supermarket(Integer supermarketid) {
        this.supermarketid = supermarketid;
    }

    public Supermarket(Integer supermarketid, String supermarketname, String administratorname, String firstlastname, String secondlastname, String telephonenumber, int eggscartonsnormallybuy, boolean supermarketstatus, String supermarketlocation) {
        this.supermarketid = supermarketid;
        this.supermarketname = supermarketname;
        this.administratorname = administratorname;
        this.firstlastname = firstlastname;
        this.secondlastname = secondlastname;
        this.telephonenumber = telephonenumber;
        this.eggscartonsnormallybuy = eggscartonsnormallybuy;
        this.supermarketstatus = supermarketstatus;
        this.supermarketlocation = supermarketlocation;
    }

    public Integer getSupermarketid() {
        return supermarketid;
    }

    public void setSupermarketid(Integer supermarketid) {
        this.supermarketid = supermarketid;
    }

    public String getSupermarketname() {
        return supermarketname;
    }

    public void setSupermarketname(String supermarketname) {
        this.supermarketname = supermarketname;
    }

    public String getAdministratorname() {
        return administratorname;
    }

    public void setAdministratorname(String administratorname) {
        this.administratorname = administratorname;
    }

    public String getFirstlastname() {
        return firstlastname;
    }

    public void setFirstlastname(String firstlastname) {
        this.firstlastname = firstlastname;
    }

    public String getSecondlastname() {
        return secondlastname;
    }

    public void setSecondlastname(String secondlastname) {
        this.secondlastname = secondlastname;
    }

    public String getTelephonenumber() {
        return telephonenumber;
    }

    public void setTelephonenumber(String telephonenumber) {
        this.telephonenumber = telephonenumber;
    }

    public int getEggscartonsnormallybuy() {
        return eggscartonsnormallybuy;
    }

    public void setEggscartonsnormallybuy(int eggscartonsnormallybuy) {
        this.eggscartonsnormallybuy = eggscartonsnormallybuy;
    }

    public boolean getSupermarketstatus() {
        return supermarketstatus;
    }

    public void setSupermarketstatus(boolean supermarketstatus) {
        this.supermarketstatus = supermarketstatus;
    }

    public String getSupermarketlocation() {
        return supermarketlocation;
    }

    public void setSupermarketlocation(String supermarketlocation) {
        this.supermarketlocation = supermarketlocation;
    }

    public List<SalesBySupermarket> getSalesBySupermarketList() {
        return salesBySupermarketList;
    }

    public void setSalesBySupermarketList(List<SalesBySupermarket> salesBySupermarketList) {
        this.salesBySupermarketList = salesBySupermarketList;
    }

    public Enterprise getEnterpriseid() {
        return enterpriseid;
    }

    public void setEnterpriseid(Enterprise enterpriseid) {
        this.enterpriseid = enterpriseid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (supermarketid != null ? supermarketid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Supermarket)) {
            return false;
        }
        Supermarket other = (Supermarket) object;
        if ((this.supermarketid == null && other.supermarketid != null) || (this.supermarketid != null && !this.supermarketid.equals(other.supermarketid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Supermarket[ supermarketid=" + supermarketid + " ]";
    }
    
}
