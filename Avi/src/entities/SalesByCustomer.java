/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author Jean Carlo
 */
@Entity
@Table(name = "salesbycustomers")
@NamedQueries({
    @NamedQuery(name = "SalesByCustomer.findAll", query = "SELECT s FROM SalesByCustomer s")
    , @NamedQuery(name = "SalesByCustomer.findBySalesbyclientid", query = "SELECT s FROM SalesByCustomer s WHERE s.salesbyclientid = :salesbyclientid")})
public class SalesByCustomer implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "salesbyclientid")
    private Integer salesbyclientid;
    @JoinColumn(name = "clientid", referencedColumnName = "clientid")
    @ManyToOne(optional = false)
    private Customer clientid;
    @JoinColumn(name = "salesid", referencedColumnName = "salesid")
    @ManyToOne(optional = false)
    private Sales salesid;

    public SalesByCustomer() {
    }

    public SalesByCustomer(Integer salesbyclientid) {
        this.salesbyclientid = salesbyclientid;
    }

    public Integer getSalesbyclientid() {
        return salesbyclientid;
    }

    public void setSalesbyclientid(Integer salesbyclientid) {
        this.salesbyclientid = salesbyclientid;
    }

    public Customer getClientid() {
        return clientid;
    }

    public void setClientid(Customer clientid) {
        this.clientid = clientid;
    }

    public Sales getSalesid() {
        return salesid;
    }

    public void setSalesid(Sales salesid) {
        this.salesid = salesid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (salesbyclientid != null ? salesbyclientid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SalesByCustomer)) {
            return false;
        }
        SalesByCustomer other = (SalesByCustomer) object;
        if ((this.salesbyclientid == null && other.salesbyclientid != null) || (this.salesbyclientid != null && !this.salesbyclientid.equals(other.salesbyclientid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.SalesByCustomer[ salesbyclientid=" + salesbyclientid + " ]";
    }
    
}
