/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Jean Carlo
 */
@Entity
@Table(name = "mortalities")
@NamedQueries({
    @NamedQuery(name = "Mortality.findAll", query = "SELECT m FROM Mortality m")
    , @NamedQuery(name = "Mortality.findByMortalityid", query = "SELECT m FROM Mortality m WHERE m.mortalityid = :mortalityid")
    , @NamedQuery(name = "Mortality.findByDeathsnumber", query = "SELECT m FROM Mortality m WHERE m.deathsnumber = :deathsnumber")
    , @NamedQuery(name = "Mortality.findByDescription", query = "SELECT m FROM Mortality m WHERE m.description = :description")
    , @NamedQuery(name = "Mortality.findByDeathsdate", query = "SELECT m FROM Mortality m WHERE m.deathsdate = :deathsdate")})
public class Mortality implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "mortalityid")
    private Integer mortalityid;
    @Basic(optional = false)
    @Column(name = "deathsnumber")
    private int deathsnumber;
    @Column(name = "description")
    private String description;
    @Basic(optional = false)
    @Column(name = "deathsdate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date deathsdate;
    @JoinColumn(name = "parvadaid", referencedColumnName = "parvadaid")
    @ManyToOne
    private Parvada parvadaid;

    public Mortality() {
    }

    public Mortality(Integer mortalityid) {
        this.mortalityid = mortalityid;
    }

    public Mortality(Integer mortalityid, int deathsnumber, Date deathsdate) {
        this.mortalityid = mortalityid;
        this.deathsnumber = deathsnumber;
        this.deathsdate = deathsdate;
    }

    public Integer getMortalityid() {
        return mortalityid;
    }

    public void setMortalityid(Integer mortalityid) {
        this.mortalityid = mortalityid;
    }

    public int getDeathsnumber() {
        return deathsnumber;
    }

    public void setDeathsnumber(int deathsnumber) {
        this.deathsnumber = deathsnumber;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDeathsdate() {
        return deathsdate;
    }

    public void setDeathsdate(Date deathsdate) {
        this.deathsdate = deathsdate;
    }

    public Parvada getParvadaid() {
        return parvadaid;
    }

    public void setParvadaid(Parvada parvadaid) {
        this.parvadaid = parvadaid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (mortalityid != null ? mortalityid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Mortality)) {
            return false;
        }
        Mortality other = (Mortality) object;
        if ((this.mortalityid == null && other.mortalityid != null) || (this.mortalityid != null && !this.mortalityid.equals(other.mortalityid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Mortality[ mortalityid=" + mortalityid + " ]";
    }
    
}
