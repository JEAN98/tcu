/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Jean Carlo
 */
@Entity
@Table(name = "productionrates")
@NamedQueries({
    @NamedQuery(name = "ProductionRate.findAll", query = "SELECT p FROM ProductionRate p")
    , @NamedQuery(name = "ProductionRate.findByProductionrateid", query = "SELECT p FROM ProductionRate p WHERE p.productionrateid = :productionrateid")
    , @NamedQuery(name = "ProductionRate.findByQuantitychickens", query = "SELECT p FROM ProductionRate p WHERE p.quantitychickens = :quantitychickens")
    , @NamedQuery(name = "ProductionRate.findByEggscollected", query = "SELECT p FROM ProductionRate p WHERE p.eggscollected = :eggscollected")
    , @NamedQuery(name = "ProductionRate.findByEggsbroken", query = "SELECT p FROM ProductionRate p WHERE p.eggsbroken = :eggsbroken")
    , @NamedQuery(name = "ProductionRate.findByGoodeggs", query = "SELECT p FROM ProductionRate p WHERE p.goodeggs = :goodeggs")
    , @NamedQuery(name = "ProductionRate.findByPosture", query = "SELECT p FROM ProductionRate p WHERE p.posture = :posture")
    , @NamedQuery(name = "ProductionRate.findByCollectiondate", query = "SELECT p FROM ProductionRate p WHERE p.collectiondate = :collectiondate")})
public class ProductionRate implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "productionrateid")
    private Integer productionrateid;
    @Basic(optional = false)
    @Column(name = "quantitychickens")
    private int quantitychickens;
    @Basic(optional = false)
    @Column(name = "eggscollected")
    private int eggscollected;
    @Basic(optional = false)
    @Column(name = "eggsbroken")
    private int eggsbroken;
    @Basic(optional = false)
    @Column(name = "goodeggs")
    private int goodeggs;
    @Basic(optional = false)
    @Column(name = "posture")
    private float posture;
    @Column(name = "collectiondate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date collectiondate;
    @JoinColumn(name = "parvadaid", referencedColumnName = "parvadaid")
    @ManyToOne(optional = false)
    private Parvada parvadaid;

    public ProductionRate() {
    }

    public ProductionRate(Integer productionrateid) {
        this.productionrateid = productionrateid;
    }

    public ProductionRate(Integer productionrateid, int quantitychickens, int eggscollected, int eggsbroken, int goodeggs, float posture) {
        this.productionrateid = productionrateid;
        this.quantitychickens = quantitychickens;
        this.eggscollected = eggscollected;
        this.eggsbroken = eggsbroken;
        this.goodeggs = goodeggs;
        this.posture = posture;
    }

    public Integer getProductionrateid() {
        return productionrateid;
    }

    public void setProductionrateid(Integer productionrateid) {
        this.productionrateid = productionrateid;
    }

    public int getQuantitychickens() {
        return quantitychickens;
    }

    public void setQuantitychickens(int quantitychickens) {
        this.quantitychickens = quantitychickens;
    }

    public int getEggscollected() {
        return eggscollected;
    }

    public void setEggscollected(int eggscollected) {
        this.eggscollected = eggscollected;
    }

    public int getEggsbroken() {
        return eggsbroken;
    }

    public void setEggsbroken(int eggsbroken) {
        this.eggsbroken = eggsbroken;
    }

    public int getGoodeggs() {
        return goodeggs;
    }

    public void setGoodeggs(int goodeggs) {
        this.goodeggs = goodeggs;
    }

    public float getPosture() {
        return posture;
    }

    public void setPosture(float posture) {
        this.posture = posture;
    }

    public Date getCollectiondate() {
        return collectiondate;
    }

    public void setCollectiondate(Date collectiondate) {
        this.collectiondate = collectiondate;
    }

    public Parvada getParvadaid() {
        return parvadaid;
    }

    public void setParvadaid(Parvada parvadaid) {
        this.parvadaid = parvadaid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (productionrateid != null ? productionrateid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProductionRate)) {
            return false;
        }
        ProductionRate other = (ProductionRate) object;
        if ((this.productionrateid == null && other.productionrateid != null) || (this.productionrateid != null && !this.productionrateid.equals(other.productionrateid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.ProductionRate[ productionrateid=" + productionrateid + " ]";
    }
    
}
