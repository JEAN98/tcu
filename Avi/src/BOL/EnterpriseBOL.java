/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BOL;

import controllers.EnterpriseJpaController;
import controllers.exceptions.IllegalOrphanException;
import controllers.exceptions.NonexistentEntityException;
import entities.Enterprise;
import java.util.List;

/**
 *
 * @author Jean Carlo
 */
public class EnterpriseBOL {
      private EnterpriseJpaController enterpriseController;

    public EnterpriseBOL() {
        this.enterpriseController = new EnterpriseJpaController();
    }
    
    public void insert(Enterprise newEnterprise){
        this.enterpriseController.create(newEnterprise);
    }
    public void update(Enterprise enterprise) throws NonexistentEntityException, Exception{
        this.enterpriseController.edit(enterprise);
    }
    public void remove(int id) throws IllegalOrphanException, NonexistentEntityException{
        this.enterpriseController.destroy(id);
    }
    
    public Enterprise getByID(int id){
        return this.enterpriseController.findEnterprise(id);
    }
    
    public List<Enterprise> getAll(){
        return this.enterpriseController.findEnterpriseEntities();
    }  
}
