/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BOL;

import controllers.GalponInventoryJpaController;
import controllers.exceptions.IllegalOrphanException;
import controllers.exceptions.NonexistentEntityException;
import entities.GalponInventory;
import java.util.List;

/**
 *
 * @author Jean Carlo
 */
public class GalponInventoryBOL {
    private GalponInventoryJpaController galponInventoryController;
    
    public GalponInventoryBOL() {
        this.galponInventoryController = new GalponInventoryJpaController();
    }
    public void insert(GalponInventory newGalponInventory){
        this.galponInventoryController.create(newGalponInventory);
    }
    public void update(GalponInventory galponInventory) throws NonexistentEntityException, Exception{
        this.galponInventoryController.edit(galponInventory);
    }
    public void remove(int id) throws IllegalOrphanException, NonexistentEntityException{
        this.galponInventoryController.destroy(id);
    }
    public GalponInventory getByID(int id){
        return this.galponInventoryController.findGalponInventory(id);
    }
    public List<GalponInventory> getAll(){
        return this.galponInventoryController.findGalponInventoryEntities();
    }


}
