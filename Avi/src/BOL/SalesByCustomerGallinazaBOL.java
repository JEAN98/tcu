/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BOL;

import controllers.SalesByCustomerGallinazaJpaController;
import controllers.exceptions.IllegalOrphanException;
import controllers.exceptions.NonexistentEntityException;
import entities.SalesByCustomerGallinaza;
import java.util.List;

/**
 *
 * @author Jean Carlo
 */
public class SalesByCustomerGallinazaBOL {
    private SalesByCustomerGallinazaJpaController salesByCustomerGallinazaController;

    public SalesByCustomerGallinazaBOL() {
        this.salesByCustomerGallinazaController = new SalesByCustomerGallinazaJpaController();
    }
    public void insert(SalesByCustomerGallinaza newSalesByCustomerGallinaza){
        this.salesByCustomerGallinazaController.create(newSalesByCustomerGallinaza);
    }
    public void update(SalesByCustomerGallinaza SalesByCustomerGallinaza) throws NonexistentEntityException, Exception{
        this.salesByCustomerGallinazaController.edit(SalesByCustomerGallinaza);
    }
    public void remove(int id) throws IllegalOrphanException, NonexistentEntityException{
        this.salesByCustomerGallinazaController.destroy(id);
    }
    public SalesByCustomerGallinaza getByID(int id){
        return this.salesByCustomerGallinazaController.findSalesByCustomerGallinaza(id);
    }
    public List<SalesByCustomerGallinaza> getAll(){
        return this.salesByCustomerGallinazaController.findSalesByCustomerGallinazaEntities();
    }


}
