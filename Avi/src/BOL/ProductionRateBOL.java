/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BOL;

import controllers.ProductionRateJpaController;
import controllers.exceptions.IllegalOrphanException;
import controllers.exceptions.NonexistentEntityException;
import entities.ProductionRate;
import java.util.List;

/**
 *
 * @author Jean Carlo
 */
public class ProductionRateBOL {
    private ProductionRateJpaController productionRateController;

    public ProductionRateBOL() {
        this.productionRateController = new ProductionRateJpaController();
    }
    public void insert(ProductionRate newProductionRate){
        this.productionRateController.create(newProductionRate);
    }
    public void update(ProductionRate ProductionRate) throws NonexistentEntityException, Exception{
        this.productionRateController.edit(ProductionRate);
    }
    public void remove(int id) throws IllegalOrphanException, NonexistentEntityException{
        this.productionRateController.destroy(id);
    }
    public ProductionRate getByID(int id){
        return this.productionRateController.findProductionRate(id);
    }
    public List<ProductionRate> getAll(){
        return this.productionRateController.findProductionRateEntities();
    }
}
