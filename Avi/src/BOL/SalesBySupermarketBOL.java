/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BOL;

import controllers.SalesBySupermarketJpaController;
import controllers.exceptions.IllegalOrphanException;
import controllers.exceptions.NonexistentEntityException;
import entities.SalesBySupermarket;
import java.util.List;

/**
 *
 * @author Jean Carlo
 */
public class SalesBySupermarketBOL {
    private SalesBySupermarketJpaController salesBySupermarketController;

    public SalesBySupermarketBOL() {
        this.salesBySupermarketController = new SalesBySupermarketJpaController();
    }
    public void insert(SalesBySupermarket newSalesBySupermarket){
        this.salesBySupermarketController.create(newSalesBySupermarket);
    }
    public void update(SalesBySupermarket SalesBySupermarket) throws NonexistentEntityException, Exception{
        this.salesBySupermarketController.edit(SalesBySupermarket);
    }
    public void remove(int id) throws IllegalOrphanException, NonexistentEntityException{
        this.salesBySupermarketController.destroy(id);
    }
    public SalesBySupermarket getByID(int id){
        return this.salesBySupermarketController.findSalesBySupermarket(id);
    }
    public List<SalesBySupermarket> getAll(){
        return this.salesBySupermarketController.findSalesBySupermarketEntities();
    }

}
