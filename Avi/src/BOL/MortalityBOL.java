/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BOL;

import controllers.MortalityJpaController;
import controllers.exceptions.IllegalOrphanException;
import controllers.exceptions.NonexistentEntityException;
import entities.Mortality;
import java.util.List;

/**
 *
 * @author Jean Carlo
 */
public class MortalityBOL {
    private MortalityJpaController mortalityController;

    public MortalityBOL() {
        this.mortalityController = new MortalityJpaController();
    }
    public void insert(Mortality newMortality){
        this.mortalityController.create(newMortality);
    }
    public void update(Mortality Mortality) throws NonexistentEntityException, Exception{
        this.mortalityController.edit(Mortality);
    }
    public void remove(int id) throws IllegalOrphanException, NonexistentEntityException{
        this.mortalityController.destroy(id);
    }
    public Mortality getByID(int id){
        return this.mortalityController.findMortality(id);
    }
    public List<Mortality> getAll(){
        return this.mortalityController.findMortalityEntities();
    }
}
