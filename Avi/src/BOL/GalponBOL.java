/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BOL;

import controllers.GalponJpaController;
import controllers.exceptions.IllegalOrphanException;
import controllers.exceptions.NonexistentEntityException;
import entities.Galpon;
import java.util.List;

/**
 *
 * @author Jean Carlo
 */
public class GalponBOL {
    private GalponJpaController galponController;
    
    public GalponBOL() {
        this.galponController = new GalponJpaController();
    }
    public void insert(Galpon newGalpon){
        this.galponController.create(newGalpon);
    }
    public void update(Galpon galpon) throws NonexistentEntityException, Exception{
        this.galponController.edit(galpon);
    }
    public void remove(int id) throws IllegalOrphanException, NonexistentEntityException{
        this.galponController.destroy(id);
    }
    public Galpon getByID(int id){
        return this.galponController.findGalpon(id);
    }
    public List<Galpon> getAll(){
        return this.galponController.findGalponEntities();
    }
}
