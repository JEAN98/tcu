/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BOL;

/**
 *
 * @author Jean Carlo
 */
public class Singleton {
    private static Singleton singleInstance = null; 
    private ViewHelper viewHelper;
    private Singleton(){
       this.viewHelper = new ViewHelper();
    }
    
    public static Singleton getInstance() 
    { 
        if (singleInstance == null) 
            singleInstance = new Singleton(); 
        return singleInstance; 
    } 

    public ViewHelper getViewHelper() {
        return viewHelper;
    }

    public void setViewHelper(ViewHelper viewHelper) {
        this.viewHelper = viewHelper;
    }
    
    
        
}
