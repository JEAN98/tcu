/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BOL;

import controllers.MedicineApplicationJpaController;
import controllers.exceptions.IllegalOrphanException;
import controllers.exceptions.NonexistentEntityException;
import entities.MedicineApplication;
import java.util.List;

/**
 *
 * @author Jean Carlo
 */
public class MedicineApplicationBOL {
    private MedicineApplicationJpaController medicineApplicationController;
    
    public MedicineApplicationBOL() {
       this.medicineApplicationController = new MedicineApplicationJpaController();
    }
    public void insert(MedicineApplication newMedicineApplication){
       this.medicineApplicationController.create(newMedicineApplication);
    }
    public void update(MedicineApplication MedicineApplication) throws NonexistentEntityException, Exception{
        this.medicineApplicationController.edit(MedicineApplication);
    }
    public void remove(int id) throws IllegalOrphanException, NonexistentEntityException{
        this.medicineApplicationController.destroy(id);
    }
    public MedicineApplication getByID(int id){
        return this.medicineApplicationController.findMedicineApplication(id);
    }
    public List<MedicineApplication> getAll(){
        return this.medicineApplicationController.findMedicineApplicationEntities();
    }
}
