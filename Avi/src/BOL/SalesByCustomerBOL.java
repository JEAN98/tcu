/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BOL;

import controllers.SalesByCustomerJpaController;
import controllers.exceptions.IllegalOrphanException;
import controllers.exceptions.NonexistentEntityException;
import entities.SalesByCustomer;
import java.util.List;

/**
 *
 * @author Jean Carlo
 */
public class SalesByCustomerBOL {
    private SalesByCustomerJpaController salesByCustomerController;

    public SalesByCustomerBOL() {
        this.salesByCustomerController = new SalesByCustomerJpaController();
    }
    public void insert(SalesByCustomer newSalesByCustomer){
        this.salesByCustomerController.create(newSalesByCustomer);
    }
    public void update(SalesByCustomer SalesByCustomer) throws NonexistentEntityException, Exception{
        this.salesByCustomerController.edit(SalesByCustomer);
    }
    public void remove(int id) throws IllegalOrphanException, NonexistentEntityException{
        this.salesByCustomerController.destroy(id);
    }
    public SalesByCustomer getByID(int id){
        return this.salesByCustomerController.findSalesByCustomer(id);
    }
    public List<SalesByCustomer> getAll(){
        return this.salesByCustomerController.findSalesByCustomerEntities();
    }
}
