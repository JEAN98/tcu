/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BOL;

import controllers.ParvadaFoodJpaController;
import controllers.exceptions.IllegalOrphanException;
import controllers.exceptions.NonexistentEntityException;
import entities.ParvadaFood;
import java.util.List;

/**
 *
 * @author Jean Carlo
 */
public class ParvadaFoodBOL {
    private ParvadaFoodJpaController parvadaFoodController;

   public ParvadaFoodBOL() {
        this.parvadaFoodController = new ParvadaFoodJpaController();
    }
    public void insert(ParvadaFood newParvadaFood){
        this.parvadaFoodController.create(newParvadaFood);
    }
    public void update(ParvadaFood ParvadaFood) throws NonexistentEntityException, Exception{
        this.parvadaFoodController.edit(ParvadaFood);
    }
    public void remove(int id) throws IllegalOrphanException, NonexistentEntityException{
        this.parvadaFoodController.destroy(id);
    }
    public ParvadaFood getByID(int id){
        return this.parvadaFoodController.findParvadaFood(id);
    }
    public List<ParvadaFood> getAll(){
        return this.parvadaFoodController.findParvadaFoodEntities();
    }
}
