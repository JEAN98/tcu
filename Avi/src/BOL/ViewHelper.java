/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BOL;

import java.awt.Button;
import java.awt.Component;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.text.JTextComponent;

/**
 *
 * @author Jean Carlo
 */
public class ViewHelper {
    public ViewHelper()
    {
    }
    
    public void cleanTextFields(JPanel jpanel)
    {
         Component[] components = jpanel.getComponents();
         for (Component component : components) {
             if (component instanceof JTextField || component instanceof JTextArea) {
                 JTextComponent specificObject = (JTextComponent) component;
                 specificObject.setText("");
             }
         }
    }
    
    public void doEnableButtons(ArrayList<JButton> buttonList)
    {
        for (JButton button : buttonList) {
            button.setEnabled(true);
        }
    }
    
    public void doDisableButtons(ArrayList<JButton> buttonList)
    {
        for (JButton button : buttonList) {
            button.setEnabled(false);
        }
    }
    
    public boolean verifyInputs(ArrayList<JTextField> jTextList){
        for (JTextField jTextField : jTextList) {
            if(jTextField.getText().equals("")){
                return true;
            }
        }
        return false;
    }
    
}
