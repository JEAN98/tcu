/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BOL;

import controllers.ParvadaJpaController;
import controllers.exceptions.IllegalOrphanException;
import controllers.exceptions.NonexistentEntityException;
import entities.Parvada;
import java.util.List;

/**
 *
 * @author Jean Carlo
 */
public class ParvadaBOL {

    private ParvadaJpaController parvadaController;

    public ParvadaBOL() {
        this.parvadaController = new ParvadaJpaController();
    }
    public void insert(Parvada newParvada){
        this.parvadaController.create(newParvada);
    }
    public void update(Parvada Parvada) throws NonexistentEntityException, Exception{
        this.parvadaController.edit(Parvada);
    }
    public void remove(int id) throws IllegalOrphanException, NonexistentEntityException{
        this.parvadaController.destroy(id);
    }
    public Parvada getByID(int id){
        return this.parvadaController.findParvada(id);
    }
    public List<Parvada> getAll(){
        return this.parvadaController.findParvadaEntities();
    }
}
