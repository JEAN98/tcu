/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BOL;

import controllers.TemperatureJpaController;
import controllers.exceptions.IllegalOrphanException;
import controllers.exceptions.NonexistentEntityException;
import entities.Temperature;
import java.util.List;

/**
 *
 * @author Jean Carlo
 */
public class TemperatureBOL {
    private TemperatureJpaController temperatureController;

    public TemperatureBOL() {
        this.temperatureController = new TemperatureJpaController();
    }
    public void insert(Temperature newTemperature){
        this.temperatureController.create(newTemperature);
    }
    public void update(Temperature Temperature) throws NonexistentEntityException, Exception{
        this.temperatureController.edit(Temperature);
    }
    public void remove(int id) throws IllegalOrphanException, NonexistentEntityException{
        this.temperatureController.destroy(id);
    }
    public Temperature getByID(int id){
        return this.temperatureController.findTemperature(id);
    }
    public List<Temperature> getAll(){
        return this.temperatureController.findTemperatureEntities();
    }
}
