/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BOL;

import controllers.CustomerJpaController;
import controllers.exceptions.IllegalOrphanException;
import controllers.exceptions.NonexistentEntityException;
import entities.Customer;
import java.util.List;

/**
 *
 * @author Jean Carlo
 */
public class CustomerBOL {
    
    private CustomerJpaController customerController;

    public CustomerBOL() {
        this.customerController = new CustomerJpaController();
    }
    public void insert(Customer newCustomerada){
        this.customerController.create(newCustomerada);
    }
    public void update(Customer Customer) throws NonexistentEntityException, Exception{
        this.customerController.edit(Customer);
    }
    public void remove(int id) throws IllegalOrphanException, NonexistentEntityException{
        this.customerController.destroy(id);
    }
    
    public Customer getByID(int id){
        return this.customerController.findCustomer(id);
    }
    
    public List<Customer> getAll(){
        return this.customerController.findCustomerEntities();
    }
}
