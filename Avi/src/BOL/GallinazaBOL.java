/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BOL;

import controllers.GallinazaJpaController;
import controllers.exceptions.IllegalOrphanException;
import controllers.exceptions.NonexistentEntityException;
import entities.Gallinaza;
import java.util.List;

/**
 *
 * @author Jean Carlo
 */
public class GallinazaBOL {
    private GallinazaJpaController gallinazaController;

    public GallinazaBOL() {
        this.gallinazaController = new GallinazaJpaController();
    }
    public void insert(Gallinaza newGallinaza){
        this.gallinazaController.create(newGallinaza);
    }
    public void update(Gallinaza Gallinaza) throws NonexistentEntityException, Exception{
        this.gallinazaController.edit(Gallinaza);
    }
    public void remove(int id) throws IllegalOrphanException, NonexistentEntityException{
        this.gallinazaController.destroy(id);
    }
    public Gallinaza getByID(int id){
        return this.gallinazaController.findGallinaza(id);
    }
    public List<Gallinaza> getAll(){
        return this.gallinazaController.findGallinazaEntities();
    }
}
