/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BOL;

import controllers.SalesJpaController;
import controllers.exceptions.IllegalOrphanException;
import controllers.exceptions.NonexistentEntityException;
import entities.Sales;
import java.util.List;

/**
 *
 * @author Jean Carlo
 */
public class SalesBOL {
    private SalesJpaController salesController;

    public SalesBOL() {
        this.salesController = new SalesJpaController();
    }
    public void insert(Sales newSales){
     this.salesController.create(newSales);
    }
    public void update(Sales Sales) throws NonexistentEntityException, Exception{
        this.salesController.edit(Sales);
    }
    public void remove(int id) throws IllegalOrphanException, NonexistentEntityException{
        this.salesController.destroy(id);
    }
    public Sales getByID(int id){
        return this.salesController.findSales(id);
    }
    public List<Sales> getAll(){
        return this.salesController.findSalesEntities();
    }

   
}
