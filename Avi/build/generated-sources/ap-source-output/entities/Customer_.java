package entities;

import entities.Enterprise;
import entities.SalesByCustomer;
import entities.SalesByCustomerGallinaza;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-02-14T23:12:11")
@StaticMetamodel(Customer.class)
public class Customer_ { 

    public static volatile SingularAttribute<Customer, Integer> clientid;
    public static volatile SingularAttribute<Customer, String> telephonenumber;
    public static volatile SingularAttribute<Customer, String> firstlastname;
    public static volatile SingularAttribute<Customer, Integer> eggscartonsnormallybuy;
    public static volatile SingularAttribute<Customer, String> clientname;
    public static volatile SingularAttribute<Customer, String> cellphonenumber;
    public static volatile ListAttribute<Customer, SalesByCustomerGallinaza> salesByCustomerGallinazaList;
    public static volatile ListAttribute<Customer, SalesByCustomer> salesByCustomerList;
    public static volatile SingularAttribute<Customer, Boolean> customerstatus;
    public static volatile SingularAttribute<Customer, Enterprise> enterpriseid;
    public static volatile SingularAttribute<Customer, String> secondlastname;

}