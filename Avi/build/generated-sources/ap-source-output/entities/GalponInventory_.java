package entities;

import entities.Galpon;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-02-14T23:12:11")
@StaticMetamodel(GalponInventory.class)
public class GalponInventory_ { 

    public static volatile SingularAttribute<GalponInventory, Integer> galponinventoryid;
    public static volatile SingularAttribute<GalponInventory, Integer> feedersquantitydesactives;
    public static volatile SingularAttribute<GalponInventory, Integer> lightbulbsactives;
    public static volatile SingularAttribute<GalponInventory, Integer> nestingboxesdesactives;
    public static volatile SingularAttribute<GalponInventory, Integer> lightbulbsdesactives;
    public static volatile SingularAttribute<GalponInventory, Galpon> galponid;
    public static volatile SingularAttribute<GalponInventory, Integer> nestingboxesactive;
    public static volatile SingularAttribute<GalponInventory, Integer> drinkingfountainsquantitydesactives;
    public static volatile SingularAttribute<GalponInventory, Integer> drinkingfountainsquantityactives;
    public static volatile SingularAttribute<GalponInventory, Integer> feedersquantityactives;

}