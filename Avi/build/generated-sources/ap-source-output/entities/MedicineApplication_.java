package entities;

import entities.Parvada;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-02-14T23:12:11")
@StaticMetamodel(MedicineApplication.class)
public class MedicineApplication_ { 

    public static volatile SingularAttribute<MedicineApplication, Integer> medicineapplicationid;
    public static volatile SingularAttribute<MedicineApplication, Date> datetimeapplication;
    public static volatile SingularAttribute<MedicineApplication, String> description;
    public static volatile SingularAttribute<MedicineApplication, Parvada> parvadaid;
    public static volatile SingularAttribute<MedicineApplication, String> medicinename;

}