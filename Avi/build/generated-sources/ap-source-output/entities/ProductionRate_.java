package entities;

import entities.Parvada;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-02-14T23:12:11")
@StaticMetamodel(ProductionRate.class)
public class ProductionRate_ { 

    public static volatile SingularAttribute<ProductionRate, Integer> quantitychickens;
    public static volatile SingularAttribute<ProductionRate, Float> posture;
    public static volatile SingularAttribute<ProductionRate, Integer> productionrateid;
    public static volatile SingularAttribute<ProductionRate, Integer> eggscollected;
    public static volatile SingularAttribute<ProductionRate, Integer> goodeggs;
    public static volatile SingularAttribute<ProductionRate, Date> collectiondate;
    public static volatile SingularAttribute<ProductionRate, Integer> eggsbroken;
    public static volatile SingularAttribute<ProductionRate, Parvada> parvadaid;

}