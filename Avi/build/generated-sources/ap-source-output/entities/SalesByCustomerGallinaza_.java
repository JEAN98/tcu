package entities;

import entities.Customer;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-02-14T23:12:11")
@StaticMetamodel(SalesByCustomerGallinaza.class)
public class SalesByCustomerGallinaza_ { 

    public static volatile SingularAttribute<SalesByCustomerGallinaza, Date> trasctiondate;
    public static volatile SingularAttribute<SalesByCustomerGallinaza, Customer> clientid;
    public static volatile SingularAttribute<SalesByCustomerGallinaza, Integer> salesbyclientgallinazaid;
    public static volatile SingularAttribute<SalesByCustomerGallinaza, Integer> bagsquantity;
    public static volatile SingularAttribute<SalesByCustomerGallinaza, Float> totalsold;
    public static volatile SingularAttribute<SalesByCustomerGallinaza, Float> pricebybag;
    public static volatile SingularAttribute<SalesByCustomerGallinaza, String> paymenttype;

}