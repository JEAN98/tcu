package entities;

import entities.Enterprise;
import entities.SalesBySupermarket;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-02-14T23:12:11")
@StaticMetamodel(Supermarket.class)
public class Supermarket_ { 

    public static volatile SingularAttribute<Supermarket, String> supermarketname;
    public static volatile ListAttribute<Supermarket, SalesBySupermarket> salesBySupermarketList;
    public static volatile SingularAttribute<Supermarket, String> administratorname;
    public static volatile SingularAttribute<Supermarket, Integer> supermarketid;
    public static volatile SingularAttribute<Supermarket, String> telephonenumber;
    public static volatile SingularAttribute<Supermarket, String> firstlastname;
    public static volatile SingularAttribute<Supermarket, Integer> eggscartonsnormallybuy;
    public static volatile SingularAttribute<Supermarket, String> supermarketlocation;
    public static volatile SingularAttribute<Supermarket, Boolean> supermarketstatus;
    public static volatile SingularAttribute<Supermarket, Enterprise> enterpriseid;
    public static volatile SingularAttribute<Supermarket, String> secondlastname;

}