package entities;

import entities.Galpon;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-02-14T23:12:11")
@StaticMetamodel(Gallinaza.class)
public class Gallinaza_ { 

    public static volatile SingularAttribute<Gallinaza, Galpon> galponid;
    public static volatile SingularAttribute<Gallinaza, Integer> gallinazaid;
    public static volatile SingularAttribute<Gallinaza, Date> collectiondate;
    public static volatile SingularAttribute<Gallinaza, Integer> bagsquantity;
    public static volatile SingularAttribute<Gallinaza, Float> pricebybag;

}