package entities;

import entities.Enterprise;
import entities.Gallinaza;
import entities.GalponInventory;
import entities.Parvada;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-02-14T23:12:11")
@StaticMetamodel(Galpon.class)
public class Galpon_ { 

    public static volatile SingularAttribute<Galpon, String> galponcode;
    public static volatile ListAttribute<Galpon, Gallinaza> gallinazaList;
    public static volatile ListAttribute<Galpon, GalponInventory> galponInventoryList;
    public static volatile ListAttribute<Galpon, Parvada> parvadaList;
    public static volatile SingularAttribute<Galpon, Integer> galponid;
    public static volatile SingularAttribute<Galpon, Float> meterslarge;
    public static volatile SingularAttribute<Galpon, Float> meterswidth;
    public static volatile SingularAttribute<Galpon, Enterprise> enterpriseid;
    public static volatile SingularAttribute<Galpon, Integer> maximumchickens;
    public static volatile SingularAttribute<Galpon, Date> constructiondate;

}