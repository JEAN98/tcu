package entities;

import entities.Customer;
import entities.Galpon;
import entities.Supermarket;
import entities.User;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-02-14T23:12:11")
@StaticMetamodel(Enterprise.class)
public class Enterprise_ { 

    public static volatile SingularAttribute<Enterprise, String> telfonnumber;
    public static volatile SingularAttribute<Enterprise, Date> creationdatetime;
    public static volatile ListAttribute<Enterprise, User> userList;
    public static volatile ListAttribute<Enterprise, Galpon> galponList;
    public static volatile ListAttribute<Enterprise, Supermarket> supermarketList;
    public static volatile ListAttribute<Enterprise, Customer> customerList;
    public static volatile SingularAttribute<Enterprise, Integer> enterpriseid;
    public static volatile SingularAttribute<Enterprise, String> enterpriseurl;
    public static volatile SingularAttribute<Enterprise, String> enterprisename;
    public static volatile SingularAttribute<Enterprise, String> email;

}