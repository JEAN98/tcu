package entities;

import entities.Sales;
import entities.Supermarket;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-02-14T23:12:11")
@StaticMetamodel(SalesBySupermarket.class)
public class SalesBySupermarket_ { 

    public static volatile SingularAttribute<SalesBySupermarket, Sales> salesid;
    public static volatile SingularAttribute<SalesBySupermarket, Supermarket> supermarketid;
    public static volatile SingularAttribute<SalesBySupermarket, Integer> salesbysupermarketid;

}