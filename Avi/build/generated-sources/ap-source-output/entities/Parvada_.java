package entities;

import entities.Galpon;
import entities.MedicineApplication;
import entities.Mortality;
import entities.ParvadaFood;
import entities.ProductionRate;
import entities.Temperature;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-02-14T23:12:11")
@StaticMetamodel(Parvada.class)
public class Parvada_ { 

    public static volatile SingularAttribute<Parvada, Date> beginningposture;
    public static volatile SingularAttribute<Parvada, String> race;
    public static volatile SingularAttribute<Parvada, Galpon> galponid;
    public static volatile ListAttribute<Parvada, MedicineApplication> medicineApplicationList;
    public static volatile SingularAttribute<Parvada, Integer> parvadaid;
    public static volatile SingularAttribute<Parvada, Integer> startquantitychickens;
    public static volatile SingularAttribute<Parvada, String> postureage;
    public static volatile SingularAttribute<Parvada, Date> admissiondate;
    public static volatile ListAttribute<Parvada, ProductionRate> productionRateList;
    public static volatile ListAttribute<Parvada, Mortality> mortalityList;
    public static volatile SingularAttribute<Parvada, String> entryage;
    public static volatile SingularAttribute<Parvada, Boolean> parvadastatus;
    public static volatile ListAttribute<Parvada, ParvadaFood> parvadaFoodList;
    public static volatile ListAttribute<Parvada, Temperature> temperatureList;

}