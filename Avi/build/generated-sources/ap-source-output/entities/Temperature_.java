package entities;

import entities.Parvada;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-02-14T23:12:11")
@StaticMetamodel(Temperature.class)
public class Temperature_ { 

    public static volatile SingularAttribute<Temperature, String> temperatureobtained;
    public static volatile SingularAttribute<Temperature, String> measurementhour;
    public static volatile SingularAttribute<Temperature, Integer> temperatureid;
    public static volatile SingularAttribute<Temperature, Parvada> parvadaid;
    public static volatile SingularAttribute<Temperature, Date> measurementdate;

}