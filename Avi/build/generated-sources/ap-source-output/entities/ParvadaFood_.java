package entities;

import entities.Parvada;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-02-14T23:12:11")
@StaticMetamodel(ParvadaFood.class)
public class ParvadaFood_ { 

    public static volatile SingularAttribute<ParvadaFood, Integer> parvadasfoodid;
    public static volatile SingularAttribute<ParvadaFood, String> suppliedconcentratename;
    public static volatile SingularAttribute<ParvadaFood, Float> suppliedquantity;
    public static volatile SingularAttribute<ParvadaFood, Date> supplieddatetime;
    public static volatile SingularAttribute<ParvadaFood, Parvada> parvadaid;

}