package entities;

import entities.SalesByCustomer;
import entities.SalesBySupermarket;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-02-14T23:12:11")
@StaticMetamodel(Sales.class)
public class Sales_ { 

    public static volatile ListAttribute<Sales, SalesBySupermarket> salesBySupermarketList;
    public static volatile SingularAttribute<Sales, Date> trasctiondate;
    public static volatile SingularAttribute<Sales, Integer> salesid;
    public static volatile SingularAttribute<Sales, Float> kilogramstotal;
    public static volatile ListAttribute<Sales, SalesByCustomer> salesByCustomerList;
    public static volatile SingularAttribute<Sales, Float> totalsold;
    public static volatile SingularAttribute<Sales, String> paymenttype;
    public static volatile SingularAttribute<Sales, Float> pricebykilograms;

}