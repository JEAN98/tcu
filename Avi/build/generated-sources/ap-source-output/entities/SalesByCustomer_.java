package entities;

import entities.Customer;
import entities.Sales;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-02-14T23:12:11")
@StaticMetamodel(SalesByCustomer.class)
public class SalesByCustomer_ { 

    public static volatile SingularAttribute<SalesByCustomer, Customer> clientid;
    public static volatile SingularAttribute<SalesByCustomer, Sales> salesid;
    public static volatile SingularAttribute<SalesByCustomer, Integer> salesbyclientid;

}