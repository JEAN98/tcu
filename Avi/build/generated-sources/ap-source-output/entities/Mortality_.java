package entities;

import entities.Parvada;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-02-14T23:12:11")
@StaticMetamodel(Mortality.class)
public class Mortality_ { 

    public static volatile SingularAttribute<Mortality, Integer> mortalityid;
    public static volatile SingularAttribute<Mortality, String> description;
    public static volatile SingularAttribute<Mortality, Integer> deathsnumber;
    public static volatile SingularAttribute<Mortality, Date> deathsdate;
    public static volatile SingularAttribute<Mortality, Parvada> parvadaid;

}